from __future__ import print_function
import os
from os.path import join, isdir, basename, dirname
from collections import defaultdict, OrderedDict
from tempfile import mktemp

from doit.tools import check_timestamp_unchanged

from dodo_common import mkdir_p, filesin, DOIT_CONFIG, get_deps

# conf globals {{{3
closedexp = os.environ.get('EXP_DIR', 'closed_experiments/')
# }}}3


def exp_wanted(expdir):
    if os.getenv('GREP_EXP', ''):
        if os.getenv('GREP_EXP') not in basename(expdir):
            return False
    if os.getenv('GREPV_EXP', ''):
        if os.getenv('GREPV_EXP') in basename(expdir):
            return False
    return True

experiments = OrderedDict(((basename(expdir), expdir) for expdir in
    filesin(closedexp) if isdir(expdir) and exp_wanted(expdir)
        ))

TTA = {e: {} for e in experiments}
NOISE = {e: {} for e in experiments}
GOOGLEISMULTICLASS = defaultdict(dict)
ENHANCED_SESSION = {e: {} for e in experiments}
for e in experiments:
    TTA[e] = {sessiondir: join(experiments[e], 'metrics',
        basename(sessiondir), 'tta')
        for sessiondir in filesin(join(experiments[e], 'data'))}
    NOISE[e] = [join(experiments[e], 'metrics', basename(sessiondir), 'noise')
            for sessiondir in filesin(join(experiments[e], 'data'))]
    ENHANCED_SESSION[e] = {sessiondir: join(experiments[e], 'metrics',
        basename(sessiondir), 'enhanced.json')
            for sessiondir in filesin(join(experiments[e], 'data'))}
    if e.startswith('0'):
# google-multiclass is only visible when there is no preprofile
        GOOGLEISMULTICLASS[e] = {sessiondir: join(experiments[e],
            'metrics',
            basename(sessiondir),
            'googleismulticlass.json')
            for sessiondir in filesin(join(experiments[e], 'data'))}
BADURLS = {e: join(edir, 'results', 'badurls.json')
        for e, edir in experiments.items()}
METRICSBYTARGET = {e: join(edir, 'results', 'bytarget.json')
        for e, edir in experiments.items()
        if e.split('-')[1].split('_')[0] == '1'
        }
METRICSBY2TARGET = {e: join(edir, 'results', 'by2target.json')
        for e, edir in experiments.items()
        if e.split('-')[1].split('_')[0] == '1'
        }
CUMULATIVEBYTARGET = {e: join(edir, 'results', 'arrivalbytarget.json')
        for e, edir in experiments.items()
        }
TOPCUMULATIVEBYTARGET = {e: join(edir, 'results', 'toparrivalbytarget.json')
        for e, edir in experiments.items()
        }
CUMULATIVE = {e: join(edir, 'results', 'tta.cumulative')
        for e, edir in experiments.items()}
TOPCUMULATIVE = {e: join(edir, 'results', 'toptta.cumulative')
        for e, edir in experiments.items()}
NOISE_AVG = {e: join(edir, 'results', 'noise.avg')
        for e, edir in experiments.items()}
TOPNOISE_AVG = {e: join(edir, 'results', 'topnoise.avg')
        for e, edir in experiments.items()}
ACCURACY_AVG = {e: join(edir, 'results', 'accuracy.avg')
        for e, edir in experiments.items()}
TOPACCURACY_AVG = {e: join(edir, 'results', 'topaccuracy.avg')
        for e, edir in experiments.items()}
GOODNOISE = {e: join(edir, 'results', 'goodnoiseratio.avg')
        for e, edir in experiments.items()}
FREQCAT = {e: join(edir, 'results', 'frequent.json')
        for e, edir in experiments.items()}
NOISYCATS = {e: join(edir, 'results', 'noisy.json')
        for e, edir in experiments.items()}
METRICATARRIVAL = {e: join(edir, 'results', '{metric}@{moment}.txt')
        for e, edir in experiments.items()}
ARRIVALMETRICS = ('topnoise', 'accuracy', 'topaccuracy')
ARRIVALMOMENTS = ('arrivalorlast', 'toparrival', 'toparrivalorlast')

GRAPHGROUPS = {}  # nicename: [list, of, expnames]
GRAPHGROUPS['all'] = [expname for expname in experiments.keys()]
GRAPHGROUPS['pre'] = [expname for expname in experiments.keys() if
        not expname.startswith('0_')]
GRAPHGROUPS['nopre'] = [expname for expname in experiments.keys() if
        expname.startswith('0_')]
#GRAPHGROUPS['soon'] = [expname for expname in experiments.keys() if
#        expname.endswith('soon-0click')]
GRAPHGROUPS['reach'] = [expname for expname in experiments.keys() if
        expname.endswith('reach-0click')]
GRAPHGROUPS['stability'] = [expname for expname in experiments.keys() if
        expname.endswith('reach-0click') and
        ('-3_' in expname or '-5_' in expname)]
for key in GRAPHGROUPS.keys():
    if not GRAPHGROUPS[key]:
        del GRAPHGROUPS[key]


def get_label(metric):
    if 'accuracy' in metric:
        return 'Profile similarity'
    elif 'noise' in metric:
        return 'Number of non-target categories'
    elif 'arrival' in metric:
        return 'Number of visits'
    return metric.capitalize()


def task_zipsession():
    '''zip every sessiondir in a session.zip file'''
    for expname, expdir in experiments.items():
        for session in filesin(join(expdir, 'data')):
            tmp = mktemp(suffix='.zip', prefix='zipsession')
            res = join(session, 'session.zip')
            session_files = filesin(session)
            if not session_files:
                continue
            yield {
                    'name': session,
                    'targets': [res],
                    'uptodate': [check_timestamp_unchanged(session)],
                    'actions': [
                        ['zip', '-q', '-j',
                            tmp
                        ] + session_files,
                        ['mv', tmp, res]
                        ]
                    }


def task_enhancedsession():
    '''enhanced session file; all in one, also'''
    bin = './evaluation/enhance_session.py'
    for expname, expdir in experiments.items():
        for rundir, res in ENHANCED_SESSION[expname].items():
            yield {
                'name': rundir,
                'targets': [res],
                'file_dep': get_deps(bin) + [join(rundir, 'session.zip')],
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    [bin, '--outfile', res,
                        '--skip-small-nonarrived', '50',
                        rundir]
                    ],
                }


def task_badurls():
    bin = 'evaluation/badurls.py'
    for expname, expdir in experiments.items():
        res = BADURLS[expname]
        sessions = ENHANCED_SESSION[expname].values()
        yield {
                'name': expname,
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    ['python2', bin,
                        '--min-diff', '5', '--previous', '1',
                        '--outfile', res
                        ] +
                    sessions
                    ],
                'file_dep': get_deps(bin) + sessions,
                'targets': [res],
                }


def task_metricsbytarget():
    bin = 'evaluation/bytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        res = METRICSBYTARGET[expname]
        sessions = ENHANCED_SESSION[expname].values()
        yield {
                'name': expname,
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    [bin, '--outfile', res] + sessions
                    ],
                'file_dep': [bin] + sessions,
                'targets': [res]
                }


def task_metricsby2target():
    bin = 'evaluation/bytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBY2TARGET:
            continue
        res = METRICSBY2TARGET[expname]
        sessions = ENHANCED_SESSION[expname].values()
        yield {
                'name': expname,
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    [bin,
                        '--level', '2',
                        '--outfile', res] + sessions
                    ],
                'file_dep': [bin] + sessions,
                'targets': [res]
                }


def task_topcumulativebytarget():
    bin = 'evaluation/cumulative_bytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        dep = METRICSBYTARGET[expname]
        res = TOPCUMULATIVEBYTARGET[expname]
        yield {
                'name': expname,
                'actions': [
                    [bin,
                        '--infinity', '--relative',
                        '--outfile', res,
                        '--arrivalkind', 'toparrival',
                        dep
                        ]
                    ],
                'file_dep': [bin, dep],
                'targets': [res]
                }


def task_cumulativebytarget():
    bin = 'evaluation/cumulative_bytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        dep = METRICSBYTARGET[expname]
        res = CUMULATIVEBYTARGET[expname]
        yield {
                'name': expname,
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    [bin,
                        '--infinity', '--relative',
                        '--outfile', res,
                        '--arrivalkind', 'arrival',
                        dep
                        ]
                    ],
                'file_dep': [bin, dep],
                'targets': [res]
                }


def task_cumulativetta():
    bin = 'evaluation/cumulative2.py'
    for expname, expdir in experiments.items():
        enhs = ENHANCED_SESSION[expname].values()
        res = CUMULATIVE[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + enhs,
            'actions': [
                (mkdir_p, (dirname(res),)),
                [bin, '--infinity', '--relative', '--outfile', res] + enhs
                ]
            }
        res = TOPCUMULATIVE[expname]
        yield {
            'name': expname + '-top',
            'targets': [res],
            'file_dep': [bin] + enhs,
            'actions': [
                (mkdir_p, (dirname(res),)),
                [bin, '--infinity', '--relative',
                    '--outfile', res,
                    '--arrivalkind', 'toparrival'] + enhs
                ]
            }


def task_arrivalavg():
    bin = 'evaluation/arrivalavg.py'
    for expname, expdir in experiments.items():
        for moment in ARRIVALMOMENTS:
            enhs = ENHANCED_SESSION[expname].values()
            res = join(expdir, 'results', 'avg_{}.txt'.format(moment))
            yield {
                'name': '{}-{}'.format(moment, expname),
                'targets': [res],
                'file_dep': [bin] + enhs,
                'actions': [
                    [bin, '--moment', moment, '--outfile', res] + enhs
                    ]
                }


def task_plottta():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'tta_%s.pdf' % group)
        cumulatives = [CUMULATIVE[name] for name in expnames]
        yield {
                'name': group,
                'targets': [out],
                'task_dep': ['cumulativetta'],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xlabel', 'number of visits',
                        '--ylabel', 'ratio of appeared categorizations',
                        '--output', out
                    ] + cumulatives,
                    ],
                'file_dep': [bin] + cumulatives
                }


def task_plotgoodnoise():
    binary = 'evaluation/histogram.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'goodnoise_%s.pdf' % (group))
        data = [GOODNOISE[e] for e in expnames]
        yield {
                'name': group,
                'targets': [out],
                'actions': [
                    ['./classifier/venv/bin/python2', binary,
                        '--ylabel', 'Good noise ratio',
                        '--xlabel', 'Number of target categories',
                        '--outfile', out,
                    ] + data,
                    ],
                'file_dep': get_deps(binary) + data
                }


def task_plotatarrival():
    binary = 'evaluation/histogram.py'
    for group, expnames in GRAPHGROUPS.items():
        for metric in ARRIVALMETRICS:
            for moment in ARRIVALMOMENTS:
                out = join(closedexp, '%s@%s_%s.pdf' % (metric, moment, group))
                data = [METRICATARRIVAL[name].format(metric=metric,
                    moment=moment)
                        for name in expnames]
                otheropts = []
                if 'accuracy' in metric:
                    otheropts += ['--ymin', '0', '--ymax', '1']
                yield {
                        'name': '{}@{}-{}'.format(metric, moment, group),
                        'targets': [out],
                        'task_dep': ['cumulativetta'],
                        'actions': [
                            ['./classifier/venv/bin/python2', binary,
                                '--ylabel', get_label(metric),
                                '--xlabel', 'Number of target categories',
                                '--outfile', out,
                            ] + otheropts + data,
                            ],
                        'file_dep': get_deps(binary) +
                        data
                        }


def task_plottoptta():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'toptta_%s.pdf' % group)
        cumulatives = [TOPCUMULATIVE[name] for name in expnames]
        yield {
                'name': group,
                'task_dep': ['cumulativetta'],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xlabel', 'number of visits',
                        '--ylabel', 'ratio of appeared categorizations',
                        '--output', out
                        ] + cumulatives,
                    ],
                'targets': [out],
                'file_dep': [bin, 'evaluation/plot_common.py'] + cumulatives
                }


def task_noiseavg():
    bin = 'evaluation/noise.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        data = ENHANCED_SESSION[expname].values()
        res = NOISE_AVG[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                    (mkdir_p, (dirname(res),)),
                ['python2', bin, 'average',
                    '--metric', 'noise',
                    '--header', '--outfile', res]
                + data
                ]
            }


def task_accuracyavg():
    bin = 'evaluation/noise.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        data = ENHANCED_SESSION[expname].values()
        res = ACCURACY_AVG[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                    (mkdir_p, (dirname(res),)),
                ['python2', bin, 'average',
                    '--metric', 'accuracy',
                    '--header', '--outfile', res]
                + data
                ]
            }


def task_topaccuracyavg():
    bin = 'evaluation/noise.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        data = ENHANCED_SESSION[expname].values()
        res = TOPACCURACY_AVG[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                    (mkdir_p, (dirname(res),)),
                ['python2', bin, 'average',
                    '--metric', 'topaccuracy',
                    '--header', '--outfile', res]
                + data
                ]
            }


def task_topnoiseavg():
    bin = 'evaluation/noise.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        data = ENHANCED_SESSION[expname].values()
        res = TOPNOISE_AVG[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                    (mkdir_p, (dirname(res),)),
                ['python2', bin, 'average',
                    '--metric', 'topnoise',
                    '--header', '--outfile', res]
                + data
                ]
            }


def task_frequentcats():
    bin = 'evaluation/bias_toofrequent.py'
    step = '200'  # at which step to check for frequent items
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        res = FREQCAT[expname]
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin],
            'actions': [
                    (mkdir_p, (dirname(res),)),
                ['python2', bin,
                    '--outfile', res,
                    join(expdir, 'data'), step]
                ]
            }


def task_noisycats():
    bin = 'evaluation/noisycats.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        res = NOISYCATS[expname]
        data = ENHANCED_SESSION[expname].values()
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                [bin,
                    '--arrivalmoment', 'toparrival',
                    '--outfile', res
                    ] + data
                ]
            }


def task_goodnoise():
    bin = 'evaluation/goodnoise.py'
    for expname, expdir in experiments.items():
# this is slightly wrong, since it won't consider the /tta that still
# does not exist, but will be created by task_tta
        res = GOODNOISE[expname]
        data = ENHANCED_SESSION[expname].values()
        yield {
            'name': expname,
            'targets': [res],
            'file_dep': [bin] + data,
            'actions': [
                [bin,
                    '--level', '1',
                    '--outfile', res
                    ] + data
                ]
            }


def task_atarrival():
    binary = 'evaluation/noiseatarrival.py'
    for expname, expdir in experiments.items():
        for metric in ARRIVALMETRICS:
            for moment in ARRIVALMOMENTS:
                sessions = ENHANCED_SESSION[expname].values()
                res = METRICATARRIVAL[expname].format(metric=metric,
                        moment=moment)
                yield {
                        'name': '{}@accuracy{}-{}'.format(metric, moment,
                            expname),
                        'actions': [
                            (mkdir_p, (dirname(res),)),
                            [binary,
                                '--metric', metric,
                                '--arrivalmoment', moment,
                                '--outfile', res] + sessions
                            ],
                        'file_dep': [binary] + sessions,
                        'targets': [res]
                        }


def task_googleismulticlass():
    bin = 'evaluation/find_googleismulticlass.py'
    for expname, expdir in experiments.items():
        for rundir, res in GOOGLEISMULTICLASS[expname].items():
            yield {
                'name': '{}_{}'.format(expname, rundir),
                'targets': [res],
                'file_dep': [bin],
                'actions': [
                    (mkdir_p, (dirname(res),)),
                    ['python2', bin, '--outfile', res, rundir]
                    ]
                }


def task_plotnoise():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'noise_%s.pdf' % group)
        cumulatives = [NOISE_AVG[name] for name in expnames]
        yield {
                'name': group,
                'targets': [out],
                'task_dep': ['noiseavg'],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xlabel', 'number of visits',
                        '--ylabel', get_label('noise'),
                        '--output', out
                    ] + cumulatives,
                    ],
                'file_dep': [bin, 'evaluation/plot_common.py'] + cumulatives
                }


def task_plottopnoise():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'topnoise_%s.pdf' % group)
        cumulatives = [TOPNOISE_AVG[name] for name in expnames]
        yield {
                'name': group,
                'targets': [out],
                'task_dep': ['noiseavg'],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xmax', '500',
                        '--xlabel', 'number of visits',
                        '--ylabel', get_label('topnoise'),
                        '--alternate-linewidth', '0.8',
                        '--linewidth', '1.2',
                        '--legend-title', 'Target profile size',
                        '--output', out
                    ] + cumulatives,
                    ],
                'file_dep': [bin, 'evaluation/plot_common.py'] + cumulatives
                }


def task_plotaccuracy():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'accuracy_%s.pdf' % group)
        data = [ACCURACY_AVG[name] for name in expnames]
        yield {
                'name': group,
                'targets': [out],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xmax', '500',
                        '--xlabel', 'number of visits',
                        '--ylabel', get_label('accuracy'),
                        '--output', out
                    ] + data,
                    ],
                'file_dep': [bin] + data
                }


def task_plottopaccuracy():
    bin = 'evaluation/plot.py'
    for group, expnames in GRAPHGROUPS.items():
        out = join(closedexp, 'topaccuracy_%s.pdf' % group)
        data = [TOPACCURACY_AVG[name] for name in expnames]
        yield {
                'name': group,
                'targets': [out],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--xscale', 'linear',
                        '--xlabel', 'number of visits',
                        '--xmax', '500',
                        '--alternate-linewidth', '0.8',
                        '--linewidth', '1.2',
                        '--ylabel', get_label('topaccuracy'),
                        '--legend-title', 'Target profile size',
                        '--output', out
                    ] + data,
                    ],
                'file_dep': [bin, 'evaluation/plot_common.py'] + data
                }


def task_plotbytarget():
    bin = 'evaluation/metricbytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        metricsfile = METRICSBYTARGET[expname]
        for metric in ('NO_toparrivaltime', 'avg_toparrivaltime',
            'avg_toparrivalnoise', 'avg_topaccuracy'):
            out = join(expdir, 'results', '{}.pdf'.format(metric))
            yield {
                    'name': '{}-{}'.format(expname, metric),
                    'targets': [out],
                    'actions': [
                        ['./classifier/venv/bin/python2', bin,
                            '--metric', metric,
                            '--ylabel', get_label(metric),
                            '--bar-width', '0.8',
                            '--outfile', out,
                            metricsfile
                            ],
                        ],
                    'task_dep': ['metricsbytarget'],
                    'file_dep': get_deps(bin) + [metricsfile]
                    }


def task_plotby2target():
    bin = 'evaluation/metricbytarget.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBY2TARGET:
            continue
        metricsfile = METRICSBY2TARGET[expname]
        for metric in ('NO_toparrivaltime', 'avg_toparrivaltime'):
            out = join(expdir, 'results', '{}_by2target.pdf'.format(metric))
            yield {
                    'name': '{}-{}'.format(expname, metric),
                    'targets': [out],
                    'actions': [
                        ['./classifier/venv/bin/python2', bin,
                            '--metric', metric,
                            '--ylabel', get_label(metric),
                            '--bar-width', '0.8',
                            '--outfile', out,
                            metricsfile
                            ],
                        ],
                    'task_dep': ['metricsbytarget'],
                    'file_dep': [bin, metricsfile]
                    }


def task_plottoparrivalbytarget():
    bin = 'evaluation/plot.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        metricsfile = TOPCUMULATIVEBYTARGET[expname]
        metric = 'toparrival'
        out = join(expdir, 'results', '{}_bytarget.pdf'.format(metric))
        yield {
                'name': '{}-{}'.format(expname, metric),
                'targets': [out],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--json',
                        '--xscale', 'log',
                        '--ylabel', get_label(metric),
                        '--output', out,
                        metricsfile
                        ],
                    ],
                'file_dep': [bin, 'evaluation/plot_common.py', metricsfile]
                }


def task_plotarrivalbytarget():
    bin = 'evaluation/plot.py'
    for expname, expdir in experiments.items():
        if expname not in METRICSBYTARGET:
            continue
        metricsfile = CUMULATIVEBYTARGET[expname]
        metric = 'arrival'
        out = join(expdir, 'results', '{}_bytarget.pdf'.format(metric))
        yield {
                'name': '{}-{}'.format(expname, metric),
                'targets': [out],
                'actions': [
                    ['./classifier/venv/bin/python2', bin,
                        '--json',
                        '--xscale', 'log',
                        '--xlabel', 'number of visits',
                        '--ylabel', get_label(metric),
                        '--output', out,
                        metricsfile
                        ],
                    ],
                'file_dep': [bin, 'evaluation/plot_common.py', metricsfile]
                }

# vim: set ft=python fdm=marker fdl=2:
