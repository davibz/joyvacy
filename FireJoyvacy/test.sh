#!/usr/bin/env bash
set -u
set -e
set -o pipefail

ADDONID='mantra@fub.it'

usage() {
	echo "$0 INTEREST_CSV HOWMANY PROFILEDIR"
	echo "Options:"
	echo "  -t   TIMEOUT   Quit after TIMEOUT seconds. Will return 10"
	echo "  -E             Do NOT enable evaluation-mode"
	echo "  -n   HOWMANY   How many target categories"
	echo "  -c   VISITS    How many url to visit per run (=number of clicks + 1)"
	echo "  -s   RUNS      How many run to do before stopping"
	echo "  -p   RUNS      How many 'preload' visits; default is 0"
	echo "  -P   HOWMANY   How many 'preload' categories; default is 1"
	echo "  -e             exit as soon as an interest appear in google profile"
	echo "  -r             exit as soon as the target is R_eached"
}
timeout=${CFX_TIMEOUT:-3600}
evalmode=1
howmany=1
stopafter=100
preload=0
preload_howmany=1
clicks=
exit_on_interests=0
exit_on_reached=0
while getopts 'eEs:n:t:p:P:c:r' opt; do
	case $opt in
	E)
		evalmode=0
		;;
	e)
		exit_on_interests=1
		;;
	r)
		exit_on_reached=1
		;;
	t)
		timeout="$OPTARG"
		;;
	n)
		howmany="$OPTARG"
		;;
	p)
		preload="$OPTARG"
		;;
	P)
		preload_howmany="$OPTARG"
		;;
	c)
		clicks="$OPTARG"
		;;
	s)
		stopafter="$OPTARG"
		;;
	\?)
		echo "invalid option: -$OPTARG" >&2
		exit 2
		;;
	esac
done
shift $((OPTIND-1))
if [[ $# -ne 2 ]]; then
	usage >&2
	exit 1
fi
interest_file=$1
profdir=$2
shift 2
if ! [[ $howmany -ge 1 ]]; then
	echo "Invalid value for HOWMANY ($howmany)" >&2
	exit 1
fi

function setting() {
    cset "extensions.$ADDONID.$1" "$2"
}
function cset() { # custom setting
    echo "user_pref(\"$1\", $2);"
}

random_cat() {
	# require one argument: number of categories
	[[ $# -eq 1 ]]
	target=$(find todo/$1 -type f -name '*.target' | sort | head -n1)
	if [ -n "$target" -a -f "$target" ]; then
		echo "Reading cat from $target" >&2
		cat $target
		mv $target ${target}.picked
	else
		cut -sf 1-2 "$interest_file"|sort -u| \
			shuf -n "$1"| \
			tr '\t' '+' | \
			tr '\n' ';'
	fi
}
function cats() {
    setting target "\"$(random_cat ${howmany})\""
}

function enabled() {
    setting evaluation_enabled true
    setting stopafter $stopafter
}

function evaldir() {
    evaldir=$(readlink -f "${profdir}/evaluation_dir")
    mkdir -p "${evaldir}"
    setting logdir "\"${evaldir}\""
}

function socksproxy() {
    host=$1
    shift 1
    port=$1
    shift 1
    cset network.proxy.socks "\"$host\""
    cset network.proxy.socks_port $port
    cset network.proxy.type 1
}

function lang() {
    cset intl.accept_languages '"en-US"'
    cset general.useragent.locale '"en-US"'
}

function timeout() {
	setting timeout_quit "$1"
}
function preload() {
	setting start_phase '"preprofile"'
	setting preprofile_stopafter "$1"
	setting preprofile_target "\"$(random_cat ${preload_howmany})\""
}

function base() {
    lang
    socksproxy localhost 8081
}

function clicks() {
	setting visit_clicks "$1"
}
function all() {
	if [[ $evalmode -eq 1 ]]; then
		enabled
	fi
	if [[ $preload -gt 0 ]]; then
		preload $preload
	else
		setting start_phase '"log"'
	fi
	cats
	evaldir
	base
	timeout "$timeout"
	if [[ -n "$clicks" ]]; then
		clicks "$clicks"
	fi
	if [[ "$exit_on_interests" -eq 1 ]]; then
		setting exit_on_google_profile_nonempty true
	fi
	if [[ "$exit_on_reached" -eq 1 ]]; then
		setting exit_on_target_reached true
	fi
}

all | tee "${profdir}/prefs.js"
exit $?
