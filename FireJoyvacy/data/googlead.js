function enable_profiling() {
	$('div[role=link]:contains("Enable")').trigger("click");
}

function get_interests() {
	var interests = [];
	$('tr[tabindex=0] > td:first-child').each(function(i, el) {
		descr = $(el).parent().attr('aria-describedby');
		attached = $(document.getElementById(descr));
		fullpath = attached.text().split('-').map(String.trim);
		interests.unshift({ name: $(el).text(),
				fullpath: fullpath });
	});
	return interests;
}

function send_interests() {
	get_interests().map(function(interest) {
		self.port.emit('interest', interest);
	});
	self.port.emit('endinterests');
}

jQuery(function($) {
	enable_profiling();
	setTimeout(send_interests, 1000);
});
