var constants = {
    colors: {
        'level1': 'rgba(0, 255, 0, 0.4)',
        'level2': 'rgba(12, 219, 242, 0.4)'
    },
    threshold: 0.01,
    visualizations: {
        flat: false,
        sub: false,
        first: true,
    }
};

function get_gradient(col, perc) {
        return 'linear-gradient(to right, ' + col + ' ' + perc + '%, #ffffff ' + perc + '%)';
}

function get_interest_bg_style(li, col) {
        var weight = parseFloat($(li).data('weight'));
        var rel_weight = weight / parseFloat($(li).parent().closest(':data(weight)').data('weight'));
        console.log(li.find('> a').text(), 'W', weight, 'RW', rel_weight);
        var perc = Math.floor(rel_weight * 100);
        if(col === undefined) {
            if(li.parent().hasClass('level2')) {
                col = constants.colors.level2;
            } else {
                col = constants.colors.level1;
            }
        }
        return 'linear-gradient(to right, ' + col + ' ' + perc + '%, #ffffff ' + perc + '%)';
}

self.port.on("classification-data-avail", function() {
    self.port.emit("classification-data-get");
});

function populate_flat(elem, interests) {
    function addcell(name, weight, par) {
        basename = name.split('/')[1];
        parentname = name.split('/')[0];
        var new_item = $('<li/>').addClass('category').data('weight', weight);
        new_item.append($('<span/>').attr('title',
                    parseInt(weight * 100) + '%').text(basename));
        $(par).append(new_item);
        return new_item;
    }
    var ol = $('ol', elem);
    $('li', ol).fadeOut(500, function() { $(this).remove(); });
    interests.map(function(node) {
        if(node.weight < constants.threshold) return;
        addcell(node.name, node.weight, ol);
    });

}

jQuery(function($) {
    $('#panels').tabs();
    $('#know-advanced').accordion({
        collapsible: true
    }).accordion('option', 'active', false);
    $("input[type=submit], button" ).button();
    $(document).tooltip();
    $('.category').initialize(function() {
        $(this).css('background', get_gradient(constants.colors.level1,
                                               $(this).data('weight') * 100));
    }); //.category initialize

    self.port.emit("classification-meta-get");
    self.port.emit("classification-data-get");
    self.port.emit("treemodifier-get");
    self.port.emit("monitor-get");

    $('#export-profile').click(function(evt) {
        //all this saving in data-tree is now useless, but could be
        //useful to _show_ data to the user
        //by now, we're just copy-pasting, so the port.emit
        //would be enough
        var tree = $('#classification').data('tree');
        self.port.emit('clipboard-classification-data');
    });
    $('#ask-reclassify').click(function(evt) {
        $('#classification-progress').progressbar({ value: false });
        $('#classification tr').animate({
            backgroundColor: "#ccc",
        }, 1000 );
        self.port.emit('reclassify-asked');
    }); //on #ask-reclassify
    $('input[name=treemodifier]').change(function() {
        self.port.emit("treemodifier-set", $(this).val());
    });

    self.port.on("classification-data-raw", function(interests) {
        $('#classification').data('tree', interests);
    });
    if(constants.visualizations.flat) {
        self.port.on("classification-data-2", function(interests) {
            populate_flat($('#classification-flat'), interests);
        });
    } else {
        $('#classification-flat').hide();
    }
    if(!constants.visualizations.sub) {
        $('#classification-sub').hide();
    }
    self.port.on("classification-data", function(interests) {
        if(interests === undefined || interests === null) { //no data avail
            return;
        }
        $('#classification-progress').progressbar({ value: 100 }).hide("scale");
        var table = $('#classification');
        $('li', table).fadeOut(500, function() { $(this).remove(); });
        function addcell(name, weight, par) {
            var new_item = $('<li/>').addClass('category').data('weight', weight);
            new_item.append($('<a/>').attr('href', '#').attr('title',
                    parseFloat(weight * 100).toFixed(2) + '%').text(name));
            $(par).append(new_item);
            return new_item;
        }
        //TODO: set $('#classification').data('weight') to the total weight
        $('#classification').data('weight',
            _.reduce(
                _.pluck(_.filter(interests,
                        function(el) { return ! el.name.includes('/'); }
                        ), 'weight'
                    ),
                function(memo, num) { return memo + parseFloat(num); },
                0
                )
            );
            var grouped = _.mapObject(
                _.pick( //remove parents that do not contain themselves
                       _.groupBy(interests, function(el) { return el.name.split('/')[0]; }),
                       function(value, key, obj) {
                           return _.findIndex(value, function(val) {
                               return _.contains(_.map(val, function(v) { return v.name; }),
                                                 key);
                           });
                       }),
                       function(values) {
                           return _.indexBy(values, 'name');
                       }
            );
            var total = _.reduce(_.keys(grouped).map(function(key) {
                return grouped[key][key].weight;
            }), function(memo, num) { return memo + num; }, 0);
        _.keys(grouped).map(function(key) {
            if((grouped[key][key].weight / total) < constants.threshold )
                return;
            var toprow = addcell(grouped[key][key].name, grouped[key][key].weight / total, table);
            $(toprow).addClass('level1');
            var sublist = $('<ol/>').addClass('level2');
            $(toprow).append(sublist);
            sublist.hide();
            _.keys(grouped[key]).map(function(subkey) {
                if(subkey === key) { return; }
                var name = grouped[key][subkey].name;
                addcell(name.substr(name.indexOf("/") + 1), grouped[key][subkey].weight, sublist);
            });
        });
        $(table).show();
    }); //on classification-data
    $(document).on('click', '.level1', function(evt) {
        var li = $(this);
        var ul = $('.level2', li);
        var nameclicked = $(li).clone().find('li').remove().end().text();
        $('#classification-sub ol').hide('normal', function() {
            $(this).remove();
            $('#classification-sub h4').text('Details for ' + nameclicked);
            $(ul).clone().appendTo('#classification-sub').show('normal');
        });
        li.closest('ol').find('li.level1').each(function() {
            $(this).fadeTo('fast', 0.6);
            $(this).css('background', get_interest_bg_style($(this),
                    constants.colors.level1));
        });
        li.fadeTo(50, 1);
        li.css('background', get_interest_bg_style(li, constants.colors.level2));
    }); //on click .level1
    self.port.on('classification-meta', function(meta) {
        var deltahours;
        if(meta === null) {
            deltahours = Infinity;
        } else {
            var deltasecs = (Date.now() - meta.time) / 1000;
            deltahours = deltasecs / 3600;
        }
        if(deltahours > 72) {
            console.info('Too much time passed; reclassifying');
            $('#ask-reclassify').trigger('click');
        }
    }); //on classification-meta

    self.port.on("treemodifier-get", function(val) {
        $('input[name=treemodifier]').val([val]);
    });
    self.port.on("monitor-get", function(stat) {
        if(stat.running === true) {
            $('#monitor-status').text('Manipulator is running');
            $('#monitor-nvisits').text('' + stat.nvisits);
        } else {
            $('#monitor-status').text('Manipulator is not running');
        }
    });
});
// vim: set ts=4 sw=4 et:
