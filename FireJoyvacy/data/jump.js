/* wait a random time, then select a random link and pass it to addon */

function exp_random(rate) {
        //rate is lambda
        //remember that it is the INVERSE of the mean
        var rand = Math.random();
        var out = - Math.log(1-rand) / rate;
        return out;
}

MIN_MS = 1000;
MEAN_MS = 4000;

jQuery(function($) {
  sleeptime = MIN_MS + exp_random(1/MEAN_MS);
  setTimeout(
          function() {
                  var links = _.pluck($('a'), 'href')
          .filter(_.isString) // this will discard non-links and other bad things
          .filter(function(url) {
                  return String.toLowerCase(url).startsWith('http');
          }).filter(function(url) {
                  var low = url.toLowerCase();
                  var exts = ['pdf', 'doc', 'mp3', 'ogg', 'avi', 'jpg'];
                  for(var i=0; i < exts.length; i++) {
                          if(low.endsWith('.' + exts[i])) {
                                  return false;
                          }
                  }
                  return true;
          });
  if(links.length === 0) {
          console.log("No valid links found");
          return;
  }
  var url = _.sample(links);
  self.port.emit('randomurl', url);
          },
        sleeptime);
});
