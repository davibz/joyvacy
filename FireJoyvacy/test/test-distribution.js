var utils = require('../lib/utils');
var random_dist = utils.random_dist;
var _ = require('../data/underscore');

function many(howmany, dist) {
    var out = [];
    for(var i=0; i < howmany; i++) {
        out.push(random_dist(dist));
    }
    return _.countBy(out, _.identity);
}

exports["test complete fail"] = function(assert) {
    assert.throws(function() {
        random_dist(4);
    }, /array/, "refuse numbers");
    assert.throws(function() {
        random_dist({});
    }, /array/, "refuse objects");
}

exports["test refuse invalid items"] = function(assert) {
    assert.throws(function() {
        random_dist([4]);
    }, /object/, "refuse numbers items");
    assert.throws(function() {
        random_dist([{}]);
    }, /must have/, "refuse object items");
    assert.throws(function() {
        random_dist([{ val: 'ciao'}]);
    }, /density/, "refuse object items with missing fields");
}

exports["test refuse non distributions"] = function(assert) {
    assert.throws(function() {
        random_dist([{ val: 'ciao', density: 0.3}]);
    }, /proper distribution/, "refuse when sum != 1");
}


exports["test flip coin"] = function(assert) {
    var dist = many(300, [{val: 'a', density: 0.5},{val: 'b', density: 0.5}]);
    console.log(dist);
    //comparisons are really "non-tight", because of too much variance
    assert.ok(dist.a > 100 && dist.b > 100, "not less than 1/3");
}

exports["test approx"] = function(assert) {
    var dist = many(100, [
            {val: 'a', density: 1/3},
            {val: 'b', density: 1/3},
            {val: 'c', density: 1/3}
            ]);
    //comparisons are really "non-tight", because of too much variance
    assert.ok(dist.a > 20 && dist.b > 20 && dist.c > 20, "not less than 1/5");
}


require("sdk/test").run(exports);
