var synthesis = require('../lib/synthesis');
var SP = synthesis.SimplePicker;
var _ = require('../data/underscore');

exports["test init"] = function(assert) {
    var t = new SP({a: [1,2,3], b: [3,4,5]});
    assert.pass("init successful");
    var cl = t.pick_class();
    assert.ok(cl === 'a' || cl === 'b', 'class chosen is meaningful');
}


exports["test refuse non-obj"] = function(assert) {
    assert.throws(function() {
        var t = new SP([1,2,3]);
    }, Error, 'list is not a valid corpora');
}

exports["test refuse non-array as corpus"] = function(assert) {
    assert.throws(function() {
        var t = new SP({a: 'ciao', b: ['foo']});
    }, Error, 'a corpus is an array');
}

exports["test pick url given class"] = function(assert) {
    var t = new SP({a: [1], b: [3]});
    var url = t.pick_url('a');
    assert.equal(url.url, 1, 'picked url belong to given class');
}

exports["test class chooser"] = function(assert) {
    var t = new SP({a: [1], b: [3]});
    t.class_chooser = _.constant('c');
    assert.equal(t.pick_class(), 'c', 'our external class chooser');
}

exports["test class chooser"] = function(assert) {
    var t = new SP({a: [1], b: [3]});
    t.class_chooser = _.constant('a');
    assert.equal(t.pick_url().url, 1, 'choose url according to chosen category');
}

exports["test random_dist as class chooser"] = function(assert) {
    var t = new SP({a: [1], b: [3]});
    var dist = [{val: 'a', density: 1}];
    t.class_chooser = _.partial(require('../lib/utils').random_dist, dist);
    assert.equal(t.pick_url().url, 1, 'choose url according to chosen category');
}

require("sdk/test").run(exports);
