var classifier = require('../lib/classifier');
var fakeclassifier = require('./classifier-utils').fakeclassifier;
var he_generator = require('./classifier-utils').he_generator;

var example_he = {url: 'http://bayes.com/',
	title: 'Probabilistic classification methods'
};

exports["test classify_hentries with many entries"] = function(assert, done) {
    classifier.classify_hentries(fakeclassifier, function(itree) {
	    assert.pass("callback reached");
	    var tops = itree.getSortedInterestsAtDepth(1);
	    var children = itree.getSortedInterestsAtDepth(2);
	    assert.equal(tops.length, 1);
	    assert.equal(children.length, 1);
	    assert.equal(tops[0].id, 'Top');
	    assert.equal(children[0].id, 'Top/Child');
	    done();
    }, Array.apply(null, Array(50000)).map(he_generator));
}

require("sdk/test").run(exports);
