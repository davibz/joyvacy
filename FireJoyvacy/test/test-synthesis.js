"use strict";
var synthesis = require('../lib/synthesis');
var TG = synthesis.TrafficGenerator;
let { before, after } = require('sdk/test/utils');
var _ = require('../data/underscore');


exports["test start only once"] = function(assert) {
    var t = new TG(_.constant({url: 'asd', interest: 'a'}));
    assert.strictEqual(t.started, false, "not started");
    assert.ok(t.start(), "start the first time");
    assert.strictEqual(t.started, true, "it is started");
    assert.notStrictEqual(t.timer, null, "there is a timer");
    assert.ok(!t.start(), "don't start the second time");
    assert.ok(!t.start(), "don't start the third time");
}

exports["test visit does at least callback"] = function(assert, done) {
    var t = new TG(_.constant({url: 'asd', interest: 'a'}));
    t.waitseconds = 3;
    t.nvisits = 0; //immediately callback
    t.onVisitComplete = function(data) {
        assert.pass("callback called");
        assert.equal(data.interest, 'a', 'interest is the one we set');
        done();
    }
    t.start();
}

exports["test visit does visit then callback"] = function(assert, done) {
    var t = new TG(_.constant({url: 'asd', interest: 'a'}));
    t.waitseconds = 3;
    t.nvisits = 1; //immediately callback
    t.onVisitComplete = function(data) {
        assert.pass("callback called");
        done();
    }
    t.start();
}

exports["test is singleton"] = function(assert) {
    var t1 = new TG(_.constant({url: 'asd', interest: 'a'}));
    var t2 = new TG(_.constant({url: 'asd', interest: 'a'}));
    assert.strictEqual(t1, t2, 'same instance');
}


before(exports, function(name, assert) {
    delete TG.prototype._singletonInstance;
});


require("sdk/test").run(exports);

// vim: set ts=4 sw=4 et:
