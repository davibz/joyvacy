var bundled = require('../lib/bundled');

exports['test get_path types'] = function(assert, done) {
    bundled.get_path('foo', function(path) {
        assert.equal(typeof path, "string", "path is a complete path");
        done();
    });
}

exports['test get_path READ'] = function(assert, done) {
    bundled.get_path('data/dialog.js', function(path) {
        require('../lib/utils').read_file(path, function(content) {
            done();
        });
    });
}

require("sdk/test").run(exports);
