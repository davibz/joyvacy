var tree = require('../lib/tree');
var _ = require('../data/underscore');

exports["test create"] = function(assert) {
    var i = new tree.InterestTree();
    assert.equal(i.tree.toArray().length, 1);
}

exports["test split depth"] = function(assert) {
    var i = new tree.InterestTree();
    var t = i.tree;
    i.addInterest('top+ic', 3);
    console.log(t.toSubtree());
    assert.equal(t.children.length, 1)
    assert.equal(t.children[0].children.length, 1)
    assert.equal(t.children[0].data.weight, 3);
    assert.equal(t.children[0].children[0].data.weight, 3);
}

exports["test parent contains sum of children"] = function(assert) {
    var i = new tree.InterestTree();
    var t = i.tree;
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    console.log(t.toSubtree());
    assert.equal(t.children.length, 1)
    assert.equal(t.children[0].children.length, 2)
    assert.equal(t.children[0].data.weight, 5);
}

exports["test path separated with +"] = function(assert) {
    var i = new tree.InterestTree();
    var path = i.getNode('ciao+asd');
    assert.equal(path.length, 2);
    assert.equal(path[0], 'ciao');
    assert.equal(path[1], 'asd');
}

exports["test get sorted interests"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    var sorted;
    sorted = i.getSortedInterests();
    assert.equal(sorted.length, 3);
    assert.equal(sorted[0].id, 'top');
    assert.equal(sorted[1].id, 'top/ic');
    assert.equal(sorted[2].id, 'top/ology');
    assert.equal(sorted[0].data.weight, 5);

    i.addInterest('top+ology', 2);
    console.log(i.tree.toSubtree());
    sorted = i.getSortedInterests();
    assert.equal(sorted.length, 3);
    assert.equal(sorted[0].id, 'top');
    assert.equal(sorted[2].id, 'top/ic');
    assert.equal(sorted[1].id, 'top/ology');
    assert.equal(sorted[1].data.weight, 4);
    assert.equal(sorted[0].data.weight, 7);
}

exports["test get sorted at depth one"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var sorted = i.getSortedInterestsAtDepth(1);
    assert.equal(sorted.length, 2, '2 top-level categories');
    assert.equal(sorted[0].id, 'top', 'top is the most important');
}

exports["test get sorted at depth two"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var sorted = i.getSortedInterestsAtDepth(2);
    assert.equal(sorted.length, 3, '3 level-2 categories');
    assert.equal(sorted[0].id, 'foo/bar', 'bar is the most important');
}

exports["test get sorted relative to parent"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var sorted = i.getSortedInterestsRelative();
    assert.equal(sorted.length, 5, "all nodes are in");
    assert.ok(_.every(sorted, function(n) {
        return _.has(n.data, 'rel_weight');
    }), "rel_weight attribute present");
    assert.equal(sorted[0].id, 'foo/bar', "foo/bar wins (100%)");
}

exports["test weigth invocation"] = function(assert) {
    var data = { visitCount: 1, time: Date.now() };
    var confidence = 0.5;
    assert.equal(
            Math.round(tree.weight_url(data, confidence) * 10) / 10.0,
            0.5, "now, one visit");
}

exports["test tree serializable"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var cleared = i.serializable();
    JSON.stringify(cleared);
    assert.pass("serialization does not raise exceptions");
}

exports["test tree serialization sort-correct"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var cleared = i.serializable();
    var dump = JSON.stringify(cleared);
    var back = JSON.parse(dump);
    var i2 = new tree.InterestTree();
    i2.load_serializable(back);
    var interests_tostr = function(n) { return n.id + " " + n.data.weight };
    var int1 = i.getSortedInterests().map(interests_tostr);
    var int2 = i2.getSortedInterests().map(interests_tostr);

    assert.deepEqual(int1, int2, "same top interests");
}
exports["test distribution from tree at depth"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var sorted = i.getDistribution(2);
    assert.equal(sorted.length, 3, '3 level-2 categories');
    assert.ok(_.every(sorted, function(n) {
        return _.has(n, 'density');
    }), "density attribute present");
    assert.ok(_.every(sorted, function(n) {
        return _.has(n, 'val');
    }), "val attribute present");
    assert.equal(sorted[0].val, 'foo/bar', 'bar is the most important');
    assert.equal(Math.round(_.reduce(sorted, function(old, n) {
        return old + n.density;
    }, 0)* 10), 10, "is a distribution");
}

exports["test distribution fits into utils.random_dist"] = function(assert) {
    var i = new tree.InterestTree();
    i.addInterest('top+ic', 3);
    i.addInterest('top+ology', 2);
    i.addInterest('foo+bar', 4);
    var sorted = i.getDistribution(2);
    var el = require('../lib/utils').random_dist(sorted);
    assert.pass('no errors');
}

require("sdk/test").run(exports);
