var classifier = require('../lib/classifier');
var InterestTree = require('../lib/tree').InterestTree;
var ClassifiableWebsite = require('urlclassifier').website.ClassifiableWebsite;
var fakeclassifier = require('./classifier-utils').fakeclassifier;
var he_generator = require('./classifier-utils').he_generator;

var example_website = new ClassifiableWebsite('http://google.com', 'Search');
exports["test classify_history cb gets called"] = function(assert, done) {
    classifier.classify_history(done);
}

exports["test classify_history cb gets InterestTree"] = function(assert, done) {
    classifier.classify_history(function(it) {
        assert.notStrictEqual(it.getSortedInterests, undefined,
            "has method getSortedInterests");
        assert.ok(it instanceof InterestTree, 'is exactly of InterestTree');
        done();
    });
}

exports["test history callback evaluated"] = function(assert, done) {
    classifier.classify_history(function(cl) {
        assert.pass("callback is evaluated");
        done();
    });
}

exports['test get_classifier can categorize'] = function(assert, done) {
    classifier.get_classifier(function(c) {
        assert.notStrictEqual(c.categorize, undefined, 'has a member named categorize');
        c.categorize(example_website);
        done();
    });
}

exports['test classify ClassifiableWebsite gets itself plus label'] = function(assert, done) {
    var oldw = example_website;
    oldw.extra = 'foobar';
    assert.notEqual(classifier.classify_website, undefined, 'classify_website exists');
    classifier.get_classifier(function(cl) {
        classifier.classify_website(cl, oldw, function(w) {
            console.info('got website');
            assert.ok(w instanceof ClassifiableWebsite, 'result is a ClassifiableWebsite')
            assert.equal(w.extra, 'foobar', 'extra data is preserved');
            assert.notEqual(w.label, undefined, 'some label is present');
            assert.ok(w.label.indexOf('+') > 0, 'label is hierarchical');
            done();
        });
    });
}


exports['test classify hentry'] = function(assert, done) {
    classifier.classify_hentry(fakeclassifier, he_generator(12),
                               function(ws) {
                                   assert.equal(ws.label, 'Top+Child');
                                   done();
                               });
}

exports["with empty history"] = function(assert, done) {
    classifier.classify_hentries(fakeclassifier, function(itree) {
	    assert.pass("callback reached");
	    done();
    }, []);
}

exports["really small history"] = function(assert, done) {
    classifier.classify_hentries(fakeclassifier, function(itree) {
	    assert.pass("callback reached");
	    done();
    }, [example_he]);
}

require("sdk/test").run(exports);
// vim: set ts=4 sw=4 et:
