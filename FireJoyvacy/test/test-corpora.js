var corpora = require('../lib/corpora');
//var data = require('sdk/self').data;
var bundled = require('../lib/bundled');
var _ = require('../data/underscore');

exports["test corpora init"] = function(assert, done) {
    bundled.get_path('data/corpus', function(corpusdir) {
        console.log('corpusdir', corpusdir.path);
        var cd = new corpora.CorporaDir(corpusdir.path);
        assert.pass("and it doesnt explode!");
        done();
    });
}

exports["test corpora map"] = function(assert, done) {
    bundled.get_path('data/corpus', function(corpusdir) {
        var cd = new corpora.CorporaDir(corpusdir.path);
        assert.ok(_.keys(cd.corpora).length > 0, "at least one corpus");
        assert.ok(_.every(_.keys(cd.corpora), function(corpus) {
            return ! corpus.endsWith('.csv');
        }), "file ext stripped out");
        assert.ok(_.every(_.values(cd.corpora), function(corpus) {
            return corpus instanceof corpora.CorpusCsv;
        }), "CorporaDir contains many CorpusCsv");
        done();
    });
}

exports["test corpus pick"] = function(assert, done) {
    bundled.get_path('data/corpus', function(corpusdir) {
        var cd = new corpora.CorporaDir(corpusdir.path);
        var cat = _.sample(_.keys(cd.corpora));
        var url = cd.corpora[cat].pick();
        assert.ok(_.isString(url), "pick strings");
        assert.notEqual(url, cd.corpora[cat].pick(), "is random!");
        done();
    });
}

require("sdk/test").run(exports);
