var log = require('../lib/trafficlog');
var _ = require('../data/underscore');
let { before, after } = require('sdk/test/utils');

exports["test create"] = function(assert, done) {
    var t = new log.TrafficLog(String(Math.random() * 10000000));
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function() {
        assert.pass("ok, create with no exceptions");
        done();
    });
}

exports["test empty"] = function(assert, done) {
    var t = new log.TrafficLog('testempty');
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function(t) {
        assert.pass('ok, connected');
        t.getItems(function(items) {
            assert.ok(_.isArray(items), "receive array");
            assert.equal(items.length, 0, "the array is empty");
            done();
        });
    });
}

exports["test one"] = function(assert, done) {
    var t = new log.TrafficLog(String(Math.random() * 10000000));
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function(t) {
        var req = t.logVisit({url: 'about:blank', interest: 'foo'})
        req.onsuccess = function() {
            t.getItems(function(items) {
                assert.ok(_.isArray(items), "receive array");
                assert.equal(items.length, 1, "with one item");
                done();
            });
        }
    });
}


exports["test two"] = function(assert, done) {
    var t = new log.TrafficLog(String(Math.random() * 10000000));
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function(t) {
        var cb = _.after(2, function(evt) {
            require('sdk/timers').setTimeout(function() {
                t.getItems(function(items) {
                    assert.ok(items.length >= 2, "with two items");
                    var interests = _.pluck(items, 'interest');
                    assert.ok(_.intersection(interests, ['foo', 'baz']).length >= 2,
                        "everything inside the log");
                    done();
                });
            }, 1000);
        });
        var req1 = t.logVisit({url: 'about:blank', interest: 'foo'})
        req1.onsuccess = cb;
        var req2 = t.logVisit({url: 'google.asd', interest: 'baz'})
        req2.onsuccess = cb;
    });
}

exports["test getItemsNumber empty"] = function(assert, done) {
    var t = new log.TrafficLog(String(Math.random() * 10000000));
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function(t) {
        t.getItemsNumber(function(nvisits) {
            assert.ok(_.isNumber(nvisits), "nvisits is a number");
            assert.equal(nvisits, 0, "empty");
            done();
        });
    });
}

exports["test getItemsNumber two"] = function(assert, done) {
    var t = new log.TrafficLog(String(Math.random() * 10000000));
    t.onError = function(e) {
        assert.ok(false, "DB error");
    }
    t.connect(function(t) {
        var cb = _.after(2, function(evt) {
            require('sdk/timers').setTimeout(function() {
                t.getItemsNumber(function(nvisits) {
                    assert.ok(_.isNumber(nvisits), "nvisits is a number");
                    assert.ok(nvisits >= 2, "two items");
                    done();
                });
            }, 1000);
        });
        var req1 = t.logVisit({url: 'about:blank', interest: 'foo'})
        req1.onsuccess = cb;
        var req2 = t.logVisit({url: 'google.asd', interest: 'baz'})
        req2.onsuccess = cb;
    });
}

require("sdk/test").run(exports);
