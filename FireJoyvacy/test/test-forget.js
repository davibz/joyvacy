var forget = require('../lib/forget');

exports["test immediate means perfect memory"] = function(assert) {
    var R = forget.forgetting(0, 10);
    assert.equal(R, 1, "perfect memory on t=0");
}

exports["test always greater than 0"] = function(assert) {
    var strength = 10;
    var many_times = 5;
    var time;
    while(many_times-- > 0) {
        time = Math.random()*30;
        assert.ok(forget.forgetting(time, strength) > 0, "greater than 0");
    }
}

exports["test always lesser equal than 1"] = function(assert) {
    var strength = 20;
    var many_times = 5;
    var time;
    var retention;
    while(many_times-- > 0) {
        time = Math.random()*30;
        retention = forget.forgetting(time, strength);
        assert.ok(retention <= 1, "lesser equal than 1");
        if(retention === 1) {
            assert.equal(time, 0, "if retention is 1, time is 0");
        }
    }
}

exports["test strength is the days it take to be 1/e"] = function(assert) {
    var strength = 1;
    var retention;
    while(strength++ < 40) {
        retention = forget.forgetting(strength, strength);
        assert.ok(retention < 0.37); // 1/e ~= 0.3678
    }
}

exports["test memory forget table"] = function(assert) {
    var N = 30;
    var hardcoded = { '20': [1.0, 0.951229424500714, 0.9048374180359595,
        0.8607079764250578, 0.8187307530779818, 0.7788007830714049,
        0.7408182206817179, 0.7046880897187134, 0.6703200460356393,
        0.6376281516217733, 0.6065306597126334, 0.5769498103804866,
        0.5488116360940264, 0.522045776761016, 0.4965853037914095,
        0.4723665527410147, 0.44932896411722156, 0.4274149319487267,
        0.4065696597405991, 0.38674102345450123, 0.36787944117144233,
        0.3499377491111553, 0.33287108369807955, 0.3166367693790533,
        0.30119421191220214, 0.2865047968601901, 0.2725317930340126,
        0.2592402606458915, 0.2465969639416065, 0.23457028809379765],
        '10': [1.0, 0.9048374180359595, 0.8187307530779818, 0.7408182206817179,
        0.6703200460356393, 0.6065306597126334, 0.5488116360940264,
        0.4965853037914095, 0.44932896411722156, 0.4065696597405991,
        0.36787944117144233, 0.33287108369807955, 0.30119421191220214,
        0.2725317930340126, 0.2465969639416065, 0.22313016014842982,
        0.20189651799465538, 0.18268352405273466, 0.16529888822158653,
        0.14956861922263506, 0.1353352832366127, 0.1224564282529819,
        0.11080315836233387, 0.10025884372280375, 0.09071795328941251,
        0.0820849986238988, 0.07427357821433388, 0.06720551273974976,
        0.06081006262521797, 0.05502322005640723]
    }
    for(var strength in hardcoded) {
        var table = Array.apply(null, {length: N})
                    .map(Number.call, Number).map(
                            function(t) {
                                return forget.forgetting(t, parseInt(strength, 10));
                            });
        assert.deepEqual(hardcoded[strength], table, "hardcoded table for " + strength);
    }
}

require("sdk/test").run(exports);
