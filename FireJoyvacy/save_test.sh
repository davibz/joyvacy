#!/usr/bin/env bash
set -e
set -o pipefail

ADDONID='mantra@fub.it'

usage() {
	echo "$0 PROFILEDIR RESULTS" >&2
}
if [[ $# -ne 2 ]]; then
	usage
	exit 1
fi
profdir=$1
testresults=$2
shift 2
resdir="${testresults}/$(date '+%Y%m%d%H%M%S')"
mkdir -p "$resdir"
cp ${profdir}/evaluation_dir/*.json "$resdir"
fgrep $ADDONID ${profdir}/prefs.js > "${resdir}/prefs.js"
ls "$resdir"
