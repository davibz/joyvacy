var prefs = require('sdk/simple-prefs').prefs;
var data = require('sdk/self').data;
var _ = require('../data/underscore');

function is_enabled() {
    return prefs['evaluation_enabled'] === true;
}

function get_target(phase) {
    var prefs = require('sdk/simple-prefs').prefs;
    if(prefs['evaluation_enabled'] === true) {
	var prefname = 'target';
	if(!_.isString(phase)) {
		console.warn("no phase detected?!");
		console.warn(phase);
	}
	if(phase === "preprofile") {
		prefname = 'preprofile_target';
	}
        if(_.isString(prefs[prefname])) {
            return _.filter(prefs[prefname].split(';'));
        }
    }
    return null;
}

function get_tree(interests) {
	//interests is a list of strings
	var tree = require('./tree');
	var i = new tree.InterestTree();
	interests.map(function(label) {
		i.addInterest(label, 1); //same weight to every node
	});
	return i;
}

function Interests() {
    this.interests = [];
    this.logdir = prefs['logdir'];
    if(!_.isString(this.logdir)) {
        this.logdir = null;
    }
    this.extra_data = {};
}

Interests.prototype.get_interests = function() {
	if(!is_enabled() || this.logdir === null) {
		return;
	}
	var sUrl = 'https://www.google.com/settings/ads?hl=en';
	var pageWorkers = require("sdk/page-worker");
	var page = pageWorkers.Page({
		contentURL: sUrl,
	    contentScriptFile:
		[data.url('jquery2.js'), data.url("googlead.js")],
	    contentScriptWhen: 'end', //default
	});
	var me = this;
	page.port.on("interest", function(msg) {
		path = msg.fullpath;
		if(_.isArray(path)) {
			nodedesc = path.slice(0, 2).join('+');
		} else {
			nodedesc = msg.name;
		}
		me.interests.push(nodedesc);
	});
	page.port.on("endinterests", function() {
		console.info("interests collected:", me.interests);
		me.interests.sort();
		me.dump_interests();
		page.destroy();
		if(me.interests.length > 0 && prefs['exit_on_google_profile_nonempty']) {
			console.info("Found something in google profile, exit soon!");
			require('sdk/timers').setInterval(function() {
				console.log('exit (caused by google profile nonempty)');
				require('sdk/system').exit();
			}, 20 * 1000);
		}
		if(prefs['exit_on_target_reached']) {
			var target = get_target(' ').slice();
			target.sort();
			var missing = _.difference(target, me.interests);
			console.info("Missing from target: " +  missing.length + "  " + missing);
			if(missing.length === 0) {
				console.info("Target reached, exit soon!");
				require('sdk/timers').setInterval(function() {
					console.log('exit (caused by target reached)');
					require('sdk/system').exit();
				}, 20 * 1000);
			}
		}
	});
}

Interests.prototype.dump_interests = function() {
    if(!is_enabled() || this.logdir === null) {
        return;
    }
    var msg = this.extra_data;
    msg['google_interests'] = _.uniq(_.sortBy(this.interests, _.identity), true);

    var io_file = require('sdk/io/file');
    io_file.mkpath(this.logdir);
    var path = io_file.join(this.logdir, '' + Date.now() + '.json');
    if(io_file.exists(path)) {
        console.error("URGH! path already exists", path);
    }
    var buf = io_file.open(path, 'w');
    buf.write(JSON.stringify(msg));
    buf.write("\n");
    buf.close();

}


exports.Interests = Interests;
exports.get_target = get_target;
exports.get_tree = get_tree;
