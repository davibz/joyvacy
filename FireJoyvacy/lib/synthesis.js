var _ = require('../data/underscore');

function SimplePicker(corpora) {
	if(! _.isObject(corpora) || _.isArray(corpora)) {
		console.info('non obj');
		throw new Error('invalid corpora received');
	}
	if(! _.every(_.values(corpora), _.isArray)) {
		console.info('non array');
		throw new Error('at least one corpus is not valid');
	}
	this.corpora = corpora; //an object whose key is the name of the class, and the value is a list of urls
	this.class_chooser = null;
}

SimplePicker.prototype.pick_class = function() {
	if(this.class_chooser === null) {
		var values = Object.keys(this.corpora);
		return _.sample(values);
	}
	return this.class_chooser();
}

SimplePicker.prototype.pick_url = function(corpora_class) {
	if(corpora_class === undefined) {
		corpora_class = this.pick_class();
	}
	return {
		interest: corpora_class,
		url: _.sample(this.corpora[corpora_class])
	};
}

function TrafficGenerator(picker) {
	if ( TrafficGenerator.prototype._singletonInstance ) {
		return TrafficGenerator.prototype._singletonInstance;
	}
	TrafficGenerator.prototype._singletonInstance = this;


	if(!_.isFunction(picker)) {
		throw new Error("picker must be a function");
	}
	this.pick_url = picker;
	this.started = false;
	this.timer = null;
	this.waitseconds = 60;
	this.nvisits = 5;
}

TrafficGenerator.prototype.start = function() {
	/* This will make TrafficGenerator run in background; you should call it
	 * only once */
	if(this.started === true) {
		return false;
	}
	this.started = true;
	var timers = require('sdk/timers');
	this.timer = timers.setInterval(function(tg) {
		tg.run();
	}, this.waitseconds*1000, this);

	return true;
}

TrafficGenerator.prototype.stop = function() {
	if(this.started === false) {
		return false;
	}
	this.started = false;
	var tid = this.timer;
	this.timer = null;
	require('sdk/timers').clearInterval(tid);
}

TrafficGenerator.prototype.run = function() {
	/* This will do a "single run"; this will NOT make the TrafficGenerator
	 * run in background */
	var obj = this.pick_url();
	if(!_.isObject(obj) || !_.has(obj, 'url') || !_.has(obj, 'interest')) {
		console.error('picker should return an object with url and interest', obj);
		return;
	}
	var url = obj.url;
	var interest = obj.interest;

	var tg = this;
	this.visit(url, this.nvisits, function() {
		console.log('visit complete for', url);
		if(_.isFunction(tg.onVisitComplete)) {
			tg.onVisitComplete({url: url, interest: interest});
		} else {
			console.log('no onVisitComplete set');
		}
	});
}

TrafficGenerator.prototype.visit = function(url, njumps, onEnd) {
	/* Visit url, sleep random time, then click on a random link and starts
	 * again for njumps times
	 * */
	if(njumps <= 0) {
		console.log('not really visiting ' + url + 'because it is last one');
		if(_.isFunction(onEnd)) {
			onEnd();
		} else {
			console.log('finished with no end callback');
		}
		return;
	}
	var pageWorkers = require("sdk/page-worker");
	var data = require('sdk/self').data;
	console.log('visiting', url, njumps);
	var page = pageWorkers.Page({
		contentURL: url,
	    contentScriptFile:
		[data.url('jquery2.js'),
	    data.url('underscore.js'),
	    data.url("jump.js")],
	    contentScriptWhen: 'start', //default
	});
	var tg = this;
	page.port.on('randomurl', function(newurl) {
		console.log('received randomurl', newurl);
		tg.visit(newurl, njumps-1, onEnd);
		page.destroy();
	});
	page.port.on('message', function(msg) {
		console.log('message from page', url, msg);
	});
	page.port.on('error', function(error) {
		console.info('error in page', url, error);
		console.error(error.message);
	});
}

exports.TrafficGenerator = TrafficGenerator;
exports.SimplePicker = SimplePicker;
