var ui_dialog = require('./ui-dialog');
var utils = require('./utils');
var _ = require('../data/underscore');
var corpora = require('./corpora')
var prefs = require('sdk/simple-prefs').prefs;

//FIXME: this is starting to become too big; need to setup in cleaner code
//that glues the different components
var corporadir;
require('./bundled').get_path('data/corpus', function(corpusdir) {
	corporadir = new corpora.CorporaDir(corpusdir,
		_.partial(String.replace, _, '+', '/'));
});
function picker() {
	var i;
	if(current_phase === 'preprofile') {
		i = require('./profiles').preprofileTree;
	} else {
		i = require('./profiles').getTargetTree();
	}
	if(i === null) {
		console.log('no tree, so no url picked');
		return null;
	}
	console.log('picking from tree', i.tree.serializable());
	var dist = i.getDistribution(2);
	if(dist.length == 0) {
		console.log('empty tree, so no url picked');
		return null;
	}
	var picked = corpora.object_picker(
			_.partial(utils.random_dist, dist),
			corporadir.corpora);
	return picked;
}
// Evaluation {{{
var current_phase = "";
if(prefs['evaluation_enabled'] === true) {
	current_phase = "preprofile";
	if(_.isString(prefs['start_phase'])) {
		current_phase = prefs['start_phase'];
	}
}
if(_.isNumber(prefs['timeout_quit'])) {
	require('sdk/timers').setInterval(function() {
		console.log('timeout reached');
		require('sdk/system').exit(10);
	}, prefs['timeout_quit'] * 1000);
}
//}}}

// Traffic {{{
var trafficlog = require('./trafficlog');
var tlog = new trafficlog.TrafficLog('traffic');
var synthesis = require('./synthesis');
var traffic = new synthesis.TrafficGenerator(picker);

traffic.waitseconds = prefs['visit_wait'];
traffic.nvisits = prefs['visit_clicks'];
traffic.onVisitComplete = function logvisit(info) {
	//after each visit we must log
	if(!_.isNumber(logvisit.counter)) {
		logvisit.counter = 0;
	}
	logvisit.counter++;
	console.info("Visit " + logvisit.counter + " in phase " + current_phase);
	if(prefs['evaluation_enabled'] === true && current_phase === "preprofile") { // we are in "preprofile" phase
		if(logvisit.counter >= prefs['preprofile_stopafter']) { //stop being in "preprofile" phase
			logvisit.counter = 0;
			//set "log" phase
			current_phase = "log";
		} else {
			return;
		}
	}
	var loginfo = info;
	loginfo.nvisits = traffic.nvisits;
	loginfo.waitseconds = traffic.waitseconds;
	loginfo.nthrun = logvisit.counter;
	tlog.logVisit(loginfo);
	tlog.getItems(function(items) {
		console.debug('we have ' + items.length + 'lines in trafficlog');
	});

	var evaluation = require('./evaluation');
	var interests = new evaluation.Interests();
	loginfo.target = evaluation.get_target(current_phase);
	interests.extra_data = loginfo;
	interests.get_interests();
	if(prefs['evaluation_enabled'] === true) {
		if(_.isNumber(prefs['stopafter']) && prefs['stopafter'] <= loginfo.nthrun) {
			console.info('QUITTING');
			require('sdk/system').exit();
			console.info('ma quindi non sono quittato?');
		}
	}
}
tlog.connect(function(t) {
	if (prefs['do_traffic_generation'] === true) {
		traffic.start();
	} //if prefs['do_traffic_generation'] === true
});
require('sdk/simple-prefs').on('do_traffic_generation', function() {
	if (prefs['do_traffic_generation'] === false) {
		console.info('STOPPING traffic generation')
		traffic.stop()
	} else {
		console.info('STARTING traffic generation')
		traffic.start()
	}
});
// }}}



/* vim: set noet ts=4 sw=4 fdm=marker fdl=2: */
