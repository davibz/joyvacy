/* ***************************
   This module is devoted to forgetting curves
   https://en.wikipedia.org/wiki/Forgetting_curve
   *************************** */

function forgetting(time, strength) {
    if(time < 0) {
        throw new Error("time cannot be negative");
    }
    if(strength < 0) {
        throw new Error("strength cannot be negative");
    }
    return Math.exp(-time / strength);
}

exports.forgetting = forgetting;
