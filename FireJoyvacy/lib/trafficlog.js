var { indexedDB, IDBKeyRange } = require('sdk/indexed-db');
var _ = require('../data/underscore');

function TrafficLog(name) {
    if(name === undefined) {
        name = "trafficlog";
    }
    this.db = null;
    this.onError = null;
}

TrafficLog.prototype.connect = function(callback) {
    var name = this.name;
    var request = indexedDB.open(name, "1");
    request.onupgradeneeded = function(e) {
        var db = e.target.result;

        if(db.objectStoreNames.contains("visits")) {
            db.deleteObjectStore("visits");
        }

        var store = db.createObjectStore("visits",
                {keyPath: "time"});
    };

    var t = this;
    request.onsuccess = function(e) {
        t.db = e.target.result;
        callback(t);
    };
    if(_.isFunction(t.onError)) {
        request.onerror = t.onError;
    }
}

TrafficLog.prototype.logVisit = function(info) {
    if(!_.isObject(info)) {
        throw new Error("info is not an object, can't log it");
    }
    var trans = this.db.transaction(["visits"], "readwrite");
    var store = trans.objectStore("visits");
    info.time = Date.now();
    var req = store.put(info); //onerror should be set, but...
    if(_.isFunction(this.onError)) {
        req.onerror = this.onError;
    }
    return req;
}

TrafficLog.prototype.getItems = function(cb) {
    var trans = this.db.transaction(["visits"], "readonly");
    var store = trans.objectStore("visits");
    var items = new Array();

    trans.oncomplete = function() {
        cb(items);
    }

    var keyRange = IDBKeyRange.lowerBound(0);
    var cursorRequest = store.openCursor(keyRange);
    cursorRequest.onsuccess = function(e) {
        var result = e.target.result;
        if(!!result == false)
            return;

        items.push(result.value);
        result.continue();
    };
}

TrafficLog.prototype.getItemsNumber = function(cb) {
    this.getItems(function(items) {
        cb(items.length);
    });
}

exports.TrafficLog = TrafficLog;
