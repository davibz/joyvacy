// Bundled-utils

var self = require('sdk/self');
const {Cu,Ci,Cc} = require("chrome");

function get_path_bundled(relpath, callback) {
	/* Get a file object contained inside the extension
	 * Params:
	 * @relpath   path relative to extension directory (as in source code)
	 * @callback  invoked as callback(file) where file is a nsIFile object
	 */
	Cu.import("resource://gre/modules/AddonManager.jsm");
	AddonManager.getAddonByID(self.id, function(addon) {
		var uri = addon.getResourceURI(relpath); // it's a nsIURI
		var path = uri.QueryInterface(Ci.nsIFileURL).file.path; //it's a path
		callback(path);
	});
}

exports.get_path = get_path_bundled

/* vim: set noet ts=4 sw=4 fdm=marker fdl=2: */
