var data = require("sdk/self").data;
var _ = require('../data/underscore');
// Construct a panel, loading its content from the "text-entry.html"
// file in the "data" directory, and loading the "get-text.js" script
// into it.

// Create a button
require("sdk/ui/button/action").ActionButton({
    id: "show-joyvacy-prefs",
    label: "Show Mantra Preferences",
    icon: {
        "16": "./icon-16.png",
    "32": "./icon-32.png",
    "64": "./icon-64.png"
    },
    onClick: handleClick
});

var {Hotkey} = require("sdk/hotkeys");
Hotkey({
	combo: "control-j",
	onPress: handleClick
});

function handleClick() {
    require('sdk/tabs').open(data.url('dialog.html'));
}

require("sdk/page-mod").PageMod({
	include: 'resource://mantra-at-fub-dot-it/*',
	contentStyleFile: data.url('jquery-ui/jquery-ui.min.css'),
	contentScriptFile: [data.url('jquery2.js'),
	data.url('jquery-ui/jquery-ui.min.js'),
	data.url('underscore.js'),
	data.url('jquery.initialize.js'),
	data.url('dialog.js')],
	onAttach: function(worker) {
		worker.port.on("reclassify-asked", function() {
			console.log('handling [reclassify-asked]');
			var classifier = require('./classifier');
			classifier.classify_history(function(cl) {
				var ss = require("sdk/simple-storage").storage;
				console.log('QUOTA before', ss.quotaUsage);
				ss.classification = cl.serializable();
				ss.classification_meta = {
					'time': Date.now()
				};
				console.log('QUOTA', ss.quotaUsage);
				require('./profiles').createTrees();
				worker.port.emit("classification-data-avail");
			});
		});
		worker.port.on("clipboard-classification-data", function() {
			var profiles = require('./profiles');
			var i = profiles.readInterestTree();
			if(i === null) {
				console.log('clipboard-classification-data asked, but data not available');
			}
			var data = require("sdk/base64").encode(JSON.stringify(i.serializable()));
			require('sdk/clipboard').set(data);
			require('sdk/notifications').notify({
				title: "ManTra",
				text: "Profile copied to clipboard. Paste it!",
				iconURL: require('sdk/self').data.url('icon-64.png')
			});
		});
		worker.port.on("classification-data-get", function() {
			var profiles = require('./profiles');
			var i = profiles.readInterestTree();
			/* Passing an object is not viable, as functions cannot be serialized;
			   also passing raw data is difficult, as libraries are hard to be
			   shared between addon and content scripts.  Therefore we will
			   pass a simpler structure */
			if(i !== null) {
				worker.port.emit("classification-data-raw", i.serializable());
				var emittingdata = i.getSortedInterests().map(function(node) {
						return { name: node.id, weight: node.data.weight };
					});
				worker.port.emit("classification-data", emittingdata);
				var flat2_emittingdata = i.getSortedInterestsAtDepth(2).map(function(node) {
						return { name: node.id, weight: node.data.weight };
					});
				worker.port.emit("classification-data-2", flat2_emittingdata);
			}
		});
		worker.port.on("classification-meta-get", function() {
			var ss = require("sdk/simple-storage").storage;
			var data = null;
			if(_.has(ss, 'classification_meta')) {
				data = ss.classification_meta;
			}
			worker.port.emit('classification-meta', data);
		});
		worker.port.on("treemodifier-get", function(val) {
			var prefs = require("sdk/simple-prefs").prefs;
			worker.port.emit("treemodifier-get", prefs.treemodifier);
		});
		worker.port.on("monitor-get", function(val) {
			var synthesis = require('./synthesis');
			var traffic = new synthesis.TrafficGenerator();
			var trafficlog = require('./trafficlog');
			var TL = new trafficlog.TrafficLog('traffic');
			TL.connect(
				function(tlog) {
					tlog.getItemsNumber(function(nvisits) {
						worker.port.emit("monitor-get", {
							running: traffic.started,
							nvisits: nvisits
						});
					});
				});
		});
		worker.port.on("treemodifier-set", function(val) {
			var prefs = require("sdk/simple-prefs").prefs;
			prefs.treemodifier = val;
		});
	}
});

/* vim: set noet ts=4 sw=4 fdm=marker fdl=2: */
