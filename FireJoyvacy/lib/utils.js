// file utils
const {Cu,Ci,Cc,Cr} = require("chrome");
var _ = require('../data/underscore');

function mktemp(base) {
	/* Returns: a nsIFile object for a unique, temporary file */
	var ds = Cc["@mozilla.org/file/directory_service;1"].getService();
	var dsprops = ds.QueryInterface(Ci.nsIProperties);
	var tmpFile = dsprops.get("TmpD", Ci.nsIFile);
	if(base === undefined) {
		base = "classifier";
	}
	tmpFile.append("mantra." + base + ".tmp");
	tmpFile.createUnique(tmpFile.NORMAL_FILE_TYPE, parseInt('0600', 8));
	return tmpFile
}

function read_file(file, callback) {
	/* Read a file asynchronously
	 * Params: a nsIFile or nsIURI and a function(aData) which will get file contents as
	 * one string
	 * Returns: nothing
	 */
	if(file.exists !== undefined && !file.exists()) {
		console.error('Error reading: file does not exist', file.path);
	}
	Cu.import("resource://gre/modules/NetUtil.jsm");

	NetUtil.asyncFetch(file, function(inputStream, status) {
		if (Cr.NS_OK !== status) {
			console.error("Error fetching", file.path, status);
			return;
		}
		// The file data is contained within inputStream.
		// You can read it into a string with
		var data = "";
		var avail = 0;
		var buf;
		while(true) {
			try{
				avail = inputStream.available();
			} catch(err) {
				break;
			}
			if(!avail) {
				break;
			}
			buf = NetUtil.readInputStreamToString(inputStream, avail);
			data += buf;
		}
		inputStream.close();
		callback(data);
	});
}

/* Transforms sth like a=ciao,b=3 in d = { a: 'ciao', b: '3' }
   NOTES:
   - no implicit type conversion is done. Everything still is a string
   - there's no escaping mechanism. Yes, this means that '=' and ',' are
     forbidden in both keys and values */
function from_keyvalue(encoded) {
	return encoded.split(',').map(function(keyvalue) {
		return keyvalue.split('=');
	}).reduce(function(prev, cur) {
		prev[cur[0]] = cur[1];
		return prev;
	}, {});
}

function pick_random(list) {
	var item = list[Math.floor(Math.random()*list.length)];
	return item;
}

function random_dist(dist) {
	if(!_.isArray(dist)) {
		throw new Error('dist must be an array');
	}
	if(!_.every(dist, _.isObject)) {
		throw new Error('array elements must be object');
	}
	if(!_.every(dist, _.partial(_.has, _, 'val'))) {
		throw new Error('array elements must have "val"');
	}
	if(!_.every(dist, _.partial(_.has, _, 'density'))) {
		throw new Error('array elements must have "density"');
	}
	var sum = _.reduce(_.pluck(dist, 'density'), function(old, x) {
		return x+old;
	}, 0);
	if(Math.abs(sum-1.0) > 0.01) {
		console.error(sum, dist);
		throw new Error('a proper distribution sums to 1');
	}
	var sorted = _.sortBy(dist, _.partial(_.property, _, 'density'));

	var {Cu} = require("chrome");
	Cu.import("resource://gre/modules/Services.jsm");
	var window = Services.appShell.hiddenDOMWindow;
	var value = window.crypto.getRandomValues(new Uint8Array(1))[0] / (Math.pow(2, 8));
	var partial_sum = 0;
	for(var i=0; i < sorted.length; i++) {
		partial_sum += sorted[i].density;
		if(value <= partial_sum) {
			return sorted[i].val;
		}
	}
	throw Error('how has it happened?');
}

exports.mktemp = mktemp;
exports.read_file = read_file;
exports.pick_random = pick_random;
exports.from_keyvalue = from_keyvalue;
exports.random_dist = random_dist;

/* vim: set noet ts=4 sw=4 fdm=marker fdl=2: */
