var _ = require('../data/underscore');

function CorpusCsv(fname) {
    this.fname = fname;
    var io_file = require('sdk/io/file');
    if(!io_file.exists(fname)) {
        throw new Error('file does not exist: ' + fname);
    }
}

CorpusCsv.prototype.pick = function() {
    var io_file = require('sdk/io/file');
    var lines = io_file.read(this.fname).split('\n');
    var sample = _.sample(lines).split('\t');
    if(sample.length <= 2) {
        console.error("How strange, sampled '" + sample +
                "' from " + this.fname + " (" + lines.length + " lines)")
        return null;
    }
    return sample[1].trim();
}

function CorporaDir(dir, renamer) {
    this.dir = dir;
    this.renamer = renamer;
    if(!_.isFunction(renamer)) {
        this.renamer = _.identity;
    }
    this.corpora = {};
    this.read_dir(dir);
}

CorporaDir.prototype.read_dir = function(dir) {
    var io_file = require('sdk/io/file');
    var files = io_file.list(dir);
    var cd = this;
    var renamer = this.renamer
    _.map(files, function(fname) {
        var base = fname.substr(0, fname.indexOf('.'));
        var fpath = io_file.join(dir, fname);
        var corpus = new CorpusCsv(fpath); 
        cd.corpora[renamer(base)] = corpus;
    });
}

function object_picker(picker, obj) {
    /* Get obj[picker()].pick() in a sane way, and return
     * {
     *   interest: 'an interest returned by picker()',
     *   url: 'a url belonging to $interest'
     * }
     */
    var ctg = picker().trim();
    if(!_.has(obj, ctg)) {
        console.error('ctg not in obj', ctg);
        return null;
    }
    var el = obj[ctg];
    var picked = el.pick();
    if(picked === undefined) {
        console.warn('not a corpus', el);
        picked = el;
    }
    return { url: picked, interest: ctg };
}

exports.CorporaDir = CorporaDir;
exports.CorpusCsv = CorpusCsv;
exports.object_picker = object_picker;
