/* jshint esversion: 6 */
let { search } = require("sdk/places/history");
var utils = require('./utils.js');
var async = require('./ext/async-custom');
var bundled = require('./bundled.js');
var tree = require('./tree.js');
var urlclassifier = require('urlclassifier');
var timers = require('sdk/timers');

function classify_hentries(cl, callback, hEntries) {
	async.mapSeries(
		hEntries,
		function(he, cb) { //map
			timers.setImmediate(function() {
				classify_hentry(cl, he, function(classified) {
					cb(null, classified);
				});
			});
		},  // end map
		function(err, results) { // reduce classified_websites
			console.timeEnd('history-classification');
			if(err) {
				console.warn('errors', err);
				throw err;
			}
			console.time('history-classification-totree');
			var it = new tree.InterestTree();
			async.each(results,
					   function(classified_website, cb) {
						   it.addInterest(classified_website.label,
										  tree.weight_url(classified_website.extra, 
														  classified_website.label_confidence
														 ));
														 timers.setImmediate(function() {
															 cb(null);
														 });
					   },
					   function(err) {
						   console.timeEnd('history-classification-totree');
						   if(err) throw err;
						   callback(it);
					   });
		}); //end reduce classified_websites
}

function classify_history(callback) {
	console.info('history called');
	get_classifier(function(cl) {
		console.time('history-classification');
		search({}, {}) // get all history
		.on("end", function(hEntries) {
			console.log('classifying', hEntries.length, 'hEntries');
			require('sdk/places/bookmarks').search({}, {})
			.on("end", function(bEntries) {
				return classify_hentries(cl, callback,
										 hEntries.concat(bEntries));
			});
		}); //on end (all history fetched)
	}); //get_classifier
} //classify_history

function classify_hentry(cl, hEntry, cb) {
	var title = hEntry.title;
	if(title == null) { title = ""; }
	var website = new urlclassifier.website.ClassifiableWebsite(hEntry.url, title.trim());
	website.extra = hEntry;
	classify_website(cl, website, cb);
}

function get_classifier(callback) {
	var data = require('sdk/self').data;
	var classifier = data.url('classifier.json');
	utils.read_file(classifier, function(content) {
		console.time("classifier-load");
		var cl = urlclassifier.HierClassifier.fromJson(content);
		console.timeEnd("classifier-load");
		callback(cl);
	});
}

function classify_website(classifier, website, callback) {
	website.label = classifier.categorize(website);
	//with the new classifier we must assume
	//confidence == 1 :(
	website.label_confidence = 1;
	callback(website);
}

exports.classify_history = classify_history;
exports.classify_website = classify_website;
exports.classify_hentry = classify_hentry;
exports.classify_hentries = classify_hentries;
exports.get_classifier = get_classifier;

// vim: set ts=4 sw=4 noet:
