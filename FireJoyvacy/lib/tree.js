/* ***************************
   This module is devoted to tree weighting, manipulation, etc
   *************************** */

var arboreal = require('./arboreal.js');
var forget = require('./forget');
var Arboreal = arboreal.Arboreal;
var _ = require('../data/underscore');

function weight_url(historydata, confidence) {
	var memory_strength = 43;
	var factor = 1;
	if(historydata.visitCount !== undefined) {
		factor *= Math.sqrt(parseInt(historydata.visitCount, 10));
	}
	if(historydata.time !== undefined) {
		var delta = (Date.now() / 1000) - (historydata.time / 1000);
		var deltadays = delta / (3600*24);
		factor *= forget.forgetting(deltadays, memory_strength);
	}
	if(historydata.visitCount === undefined &&
	   historydata.time === undefined) { //it's a bookmark!
		factor *= 5;
	}
	var score = factor * confidence;
	return score;
}

/* TODO: subclass Arboreal, adding specific function for weight sum */

function InterestTree() {
    this.tree = new Arboreal(null, {}, 'root');
    this.pathsep = '+';
}

InterestTree.prototype.addInterest = function(nodeDesc, weight) {
    /* nodeDesc is a description that can be accepted by getNode
       weight is a float
       returns nothing
     */
    var path = this.getNode(nodeDesc);
    var label;
    var node;
    var i=1;
    var parent_node = this.tree;
    while(i <= path.length) {
        label = path.slice(0, i).join('/');
        node = this.tree.find(label);
        if(node === null) {
            parent_node.appendChild({'weight': 0, 'numentries': 0}, label);
            node = this.tree.find(label);
        }
        node.data.weight += weight;
        node.data.numentries += 1;
        parent_node = node;
        i += 1;
    }
    return this.tree.find(path.join('/'));
};

InterestTree.prototype.getNode = function(nodeDesc) {
    /* converts nodeDesc into an array that represents the path from root to
     * the node or fail hard */
    if(Array.isArray(nodeDesc)) {
        return nodeDesc;
    }
    var node = nodeDesc.split(this.pathsep);
    return node;
};

InterestTree.prototype.getSortedInterests = function() {
    var interests = this.tree.toArray();
    var idx = interests.findIndex(function(n) { return n.id == 'root'; });
    if(idx > -1) {
        interests.splice(idx, 1);
    }
    interests.sort(function(m, n) { return m.data.weight < n.data.weight; });
    return interests;
};

InterestTree.prototype.getSortedInterestsAtDepth = function(depth) {
    var sorted = this.getSortedInterests();
    sorted = sorted.filter(function(n) { return n.depth === depth; });
	var total = _.reduce(
			sorted.map(function(n) { return n.data.weight; }),
			function(x,y) { return x+y; },
			0);
	return sorted.map(function(n) {
		n.data.weight = n.data.weight / total;
		return n;
	});
};

InterestTree.prototype.getDistribution = function(depth) {
	var sorted = this.getSortedInterestsAtDepth(depth);
	var sum = _.reduce(sorted, function(old, n) {
		return old + n.data.weight;
	}, 0);
	return sorted.map(function(n) {
		return {
			val: n.id,
			density: n.data.weight / sum
		};
	});
};

InterestTree.prototype.getSortedInterestsRelative = function() {
    var interests = this.tree.toArray();
    var idx = interests.findIndex(function(n) { return n.id == 'root'; });
    if(idx > -1) {
        interests.splice(idx, 1);
    }
    var rel_weight = function(x) {
        if(x.parent !== null && x.parent.data.weight && x.data.weight) { //it's a child
            var ratio = x.data.weight / x.parent.data.weight;
			if(_.isNaN(ratio) || !_.isNumber(ratio) || ratio > 1 || ratio < 0) {
				console.warning('Invalid ratio: ', ratio);
			}
			return ratio;
        }
		//it's a parent
        return 0;
    };
    interests = _.sortBy(_.map(interests, function(n) {
		n.data.rel_weight = rel_weight(n);
		return n;
	}), function(n) {
        return -n.data.rel_weight;
    });
    return interests;
};

InterestTree.prototype.difference = function(othertree) {
	throw new Error("still not implemented");
};

InterestTree.prototype.serializable = function() {
	return { tree: this.tree.serializable(),
		pathsep: this.pathsep };
};
InterestTree.prototype.load_serializable = function(obj) {
	if(obj.pathsep !== undefined) {
		this.pathsep = obj.pathsep;
	}
	if(obj.tree !== undefined) {
		this.tree.load_serializable(obj.tree);
	}
};

// Tree manipulation methods {{{2

function treeManipulatePrivacy(itree) {
	//We should not only "average" all the weights, but also add the
	//interests that we haven't  still classified
	throw new Error("still not implemented");
}
// Tree manipulation methods }}}


exports.weight_url = weight_url;
exports.InterestTree = InterestTree;

// vim: set noet ts=4 sw=4 fdm=marker:
