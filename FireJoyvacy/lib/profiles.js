/* This module is dedicated to some high-level functions that
   depend on multiple other modules and are difficult to isolate
   */
var _ = require('../data/underscore');

var userInterestsTree; // an interesttree
var targetTree;
var preprofileTree;

function readInterestTree() {
	var ss = require("sdk/simple-storage");
	if(ss.storage === undefined) {
        ss.storage = {};
	}
	var serial = ss.storage.classification;
	if(serial === undefined) {
		console.error('cant get serial');
		return null;
	}
	var tree = require('./tree');
	var i = new tree.InterestTree();
	i.load_serializable(serial);
	return i;
}

function getPreprofileTree() {
	var evaluation = require('./evaluation');
	var targets = evaluation.get_target('preprofile');
	if(targets !== null) {
		return evaluation.get_tree(targets);
	} 
	return null;
}

function getTargetTree() {
	var evaluation = require('./evaluation');
	var targets = evaluation.get_target('log');
	if(targets !== null) {
		return evaluation.get_tree(targets);
	} 
	var input = readInterestTree(); //TODO: optimize? cache? whatever
	//TODO: obey to storage to decide on targeting/privacy
	return input;
}

function createTrees() {
    userInterestsTree = readInterestTree();
    targetTree = getTargetTree();
    preprofileTree = getPreprofileTree();
}

createTrees();

exports.userInterestsTree = userInterestsTree;
exports.targetTree = targetTree;
exports.getTargetTree = getTargetTree;
exports.preprofileTree = preprofileTree;
exports.createTrees = createTrees;
exports.readInterestTree = readInterestTree;
