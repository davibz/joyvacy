import os


def task_corpuscsv():
    '''creates csv files'''
#TODO: this breaks the linearity of dodo slightly: it creates the .csv.gz,
    #which are assumed to be sources
    exe = 'trueprofile/create_index'''
    for dirname in os.listdir('trueprofile/google_directory/'):
        dirpath = os.path.join('trueprofile/google_directory/', dirname)
        if not os.path.isdir(dirpath):
            continue
        assert dirname.endswith('_dir')
        dirname = dirname[:-4]
        target = os.path.join('trueprofile/', '%s.2.csv.gz' % dirname)
        yield {
                'name': dirname,
                'actions': [[
                    exe,
                    '--cat-depth', '1',
                    dirpath,
                    target
                    ]],
                'targets': [target]
                }
