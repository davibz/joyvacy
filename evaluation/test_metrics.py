from metrics import weighted_set_difference
from nose.tools import eq_

def test_same():
    eq_(weighted_set_difference(range(4), range(4)), 0)

def test_notreached():
    eq_(weighted_set_difference(range(7), range(4)), 3, "count not reached")
    eq_(weighted_set_difference(range(7), range(4), notreached_weight=3),
            9, "count not reached and weights it")

def test_notwanted():
    eq_(weighted_set_difference(range(4), range(7)), 3, "count not wanted")
    eq_(weighted_set_difference(range(4), range(7), notwanted_weight=3),
            9, "count not wanted and weights it")

def test_sumup():
    eq_(weighted_set_difference(range(4), range(2, 6)), 4)
    eq_(weighted_set_difference(range(4), range(2, 6), notwanted_weight=3, notreached_weight=2),
            2*3+2*2, "count not wanted and weights it")
