#!/usr/bin/env bash
usage () {
	echo "Usage: $0 ROOT TOP_TARGET"
	echo "outputs a list of dirs that match the required top_target"
}

find_dirs() {
	find -L "$root" -type f -name prefs.js -exec grep -l "extensions\.firejoyvacy@fub\.it\.target.*${top_target}+" {} \; | xargs --no-run-if-empty -L1 dirname
}

if [[ $# -ne 2 ]]; then
	usage >&2
	exit 2
fi
root=$1
shift
top_target=$1
shift

find_dirs
