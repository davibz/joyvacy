import sys
import functools
import argparse
from itertools import count
import logging

import parsers
import metrics
import utils


def series_to_diffs(data, metric):
    # TODO: better handle the case of missing runs
    last = 0
    for now in data:
        this = now['time']
        val =  metric(now['target'], now['state'])
        for run in xrange(last+1, this):
            logging.debug('Missing run %d' % run)
            yield val
        yield val
        last = this

@utils.coroutine
def series_to_gnuplot_data(metric, pipe):
    while True:
        now = (yield)
        diff = metric(now['target'], now['state'])
        pipe.send(diff)


@utils.coroutine
def gnuplot_formatter(pipe, header=False):
    if header:
        pipe.send('# TIME\tDIFF')
    for i in count(1):
        diff = (yield)
        pipe.send('%d\t%.2f' % (i, diff))


@utils.coroutine
def output(theFile):
    while True:
        line = (yield)
        theFile.write(line + '\n')


def get_parser():
    parser = argparse.ArgumentParser(
        description='Get gnuplot data from evaluation directory')
    parser.add_argument('--not-wanted-weight', metavar='WEIGHT', type=float,
                        default=1)
    parser.add_argument('--not-reached-weight', metavar='WEIGHT', type=float,
                        default=1)
    parser.add_argument('directory', metavar='DIRNAME', type=str, nargs=1,
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--header', default=False, action='store_true',
                        help='Display gnuplot header')
    parser.add_argument('--out', metavar='FILENAME', type=str, default='-',
                        help='Write (pickle) classifier to FILENAME')
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    metric = functools.partial(metrics.weighted_set_difference,
            notreached_weight=args.not_reached_weight,
            notwanted_weight=args.not_wanted_weight)
    process = series_to_gnuplot_data(metric,
            gnuplot_formatter(
                output(utils.auto_buf(args.out, 'w')),
                header=args.header))
    for val in parsers.get_series(args.directory[0]):
        process.send(val)

# vim: set cc=79 ts=4 sw=4 et:
