#!/usr/bin/env python2.7
'''
this will help you find succession of supersets

the implementation is fundamentally broken: it should find successions of
sessions, one contained in the other
at the moment, it just find _pairs_.
'''
import logging
from itertools import chain

from mantra.top import top_set


def read_target_file(buf, args):
    l = []
    expected_len = None
    for line in buf:
        session, targets = line.split('\t')
        targets = [t.strip() for t in targets.split(',')]
        if expected_len is None:
            expected_len = len(targets)
        else:
            if len(targets) != expected_len:
                logging.warning('len is {}, expecting {} ({})'.format(
                    len(targets), expected_len, buf.name))
        to_set = top_set if args.top else set
        l.append((session, to_set(targets)))
    return l


def subset_succession(target, other_experiments):
    if not other_experiments:
        return []
    for session_name, other_target in other_experiments[0]:
        if not target.issuperset(other_target):
            continue
        res = subset_succession(other_target, other_experiments[1:])
        if res is not False:
            return [session_name] + res
    return False


def successions(experiments):
    longest_exp = experiments[0]
    other_exp = experiments[1:]
    for name, target in longest_exp:
        res = subset_succession(target, other_exp)
        if res is not False:
            yield [name] + res
            # and now we remove results
            assert len(res) == len(other_exp)
            for exp_i in xrange(len(res)):
                prev_len = len(other_exp[exp_i])
                other_exp[exp_i] = [s for s in other_exp[exp_i]
                        if s[0] != res[exp_i]]
                assert len(other_exp[exp_i]) == prev_len - 1


def main(args):
    experiments = {}
    for buf in args.targetfile:
        experiments[buf.name] = read_target_file(buf, args)
    lengths = [(name, len(experiments[name][0][1]))
            for name in experiments.keys()]
    lengths = [(n, l) for n, l in lengths if args.max_len >= l >= args.min_len]
    lengths.sort(key=lambda x: x[1], reverse=True)

    new_exp = [experiments[l[0]] for l in lengths]
    for succession in successions(new_exp):
        for sessionname in succession:
            args.outfile.write('{}\n'.format(sessionname))
        if args.target_details:
            print(','.join(succession))


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Helper for superset runs')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('targetfile', type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--max-len', type=int, default=float('inf'))
    parser.add_argument('--min-len', type=int, default=2)
    parser.add_argument('--top', default=False, action='store_true')
    parser.add_argument('--target-details', default=False, action='store_true')
    parser.add_argument('--outfile', default='-', type=argparse.FileType('w'))

    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
