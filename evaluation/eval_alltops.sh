#!/usr/bin/env bash

script_dir=$(dirname $0)

usage () {
	echo "Usage: $0 INTERESTFILE ROOT"
	echo "outputs a tab-delimited csv file with conditioned metric"
}

if [[ $# -ne 2 ]]; then
	usage >&2
	exit  2
fi
interests=$1
root=$2
shift 2

average_args=${average_args:-}
tmpdir=$(mktemp -d alltops.XXXXXXX)
function cleanup {
	rm -rf "$tmpdir"
}
trap cleanup EXIT

cut -f1 "$interests" | sort -u | while read top; do
	(echo "$top"; "$script_dir/subset_top.sh" "$root" "$top" |
		tee >(echo -n "$top: " >&2; wc -l >&2) |	\
		xargs python "${script_dir}/average.py" \
		$average_args) > "$tmpdir/$top.csv"
done

find "$tmpdir" -name '*.csv' -print0 | xargs -0 paste
