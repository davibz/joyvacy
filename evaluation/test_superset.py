from nose.tools import eq_

from supersets import subset_succession, successions


def test_two():
    corpus = [[
        ('n2', {1, 2, 3}),
        ], [
            ('n1', {1, 2}),
            ]]
    eq_(subset_succession({1, 2, 3, 4}, corpus), ['n2', 'n1'])


def test_two_no():
    corpus = [[
        ('n2', {1, 2, 3}),
        ], [
            ('n1', {1, 2}),
            ]]
    eq_(subset_succession({2, 3, 4}, corpus), False)


def test_more_corpus():
    corpus = [[
        ('n2', {1, 2, 3}),
        ('n2no', {1, 2, 9}),
        ], [
            ('n1no', {1, 9}),
            ('n1', {1, 2}),
            ]]
    eq_(subset_succession({1, 2, 3, 4}, corpus), ['n2', 'n1'])


def succ(*args):
    return list(successions(*args))


def test_successions_unambiguous():
    corpus = [[
        ('n2', {1, 2, 3}),
        ('n2b', {1, 2, 8}),
        ], [
            ('n1no', {1, 9}),
            ('n1', {1, 2}),
            ]]
    eq_(succ(corpus), [['n2', 'n1'], ['n2b', 'n1']])


def test_successions_precedence():
    '''precedence is in the order of the corpus'''
    corpus = [[
        ('n2', {1, 2, 3}),
        ('n2no', {1, 2, 9}),
        ], [
            ('n1no', {1, 9}),
            ('n1', {1, 2}),
            ]]
    eq_(succ(corpus), [['n2', 'n1'], ['n2no', 'n1no']])
