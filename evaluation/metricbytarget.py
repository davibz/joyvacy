#!/usr/bin/env python2.7
from __future__ import division
import logging
import json
from collections import defaultdict

from plot_common import plot_histogram


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate Noise',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument(dest='bytarget', metavar='JSON',
            type=argparse.FileType('r'))
    parser.add_argument('--outfile', metavar='OUTFILE')
    parser.add_argument('--metric', default='avg_toparrivaltime',
            help='the metric to plot')

    graphic = parser.add_argument_group('graphic options')
    graphic.add_argument('--bar-width', type=float, default=0.30,
            help='width of a single bar')
    graphic.add_argument('--bar-color', default='#dddddd',
            help='width of a single bar')
    graphic.add_argument('--font-size', type=int, default=8,
            help='font size')
    graphic.add_argument('--ylabel', metavar='LABEL')
    graphic.add_argument('--title')
    graphic.add_argument('--label-space', type=float, default=0.25,
            help='room to reserve for xlabels')

    return parser


def main(args):
    obj = json.load(args.bytarget)
    args.bytarget.close()
    averages = get_averages(obj, args)
    plot_histogram(averages, args)


def get_averages(obj, args):
    values = defaultdict(list)
    for key in obj:
        values[key].append(obj[key]['aggregate'][args.metric] or 0)
    averages = {key: sum(values[key]) / float(len(values[key]))
            for key in values}
    return averages

if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
