from StringIO import StringIO
from copy import deepcopy

from nose.tools import eq_

from parsers import read_single_file, get_series, fill_timeserie

def fill(*args, **kwargs):
    return list(fill_timeserie(*args, **kwargs))

def test_single():
    content = StringIO('''
    {"url":"http://asd","interest":"Autos & Vehicles/Commercial Vehicles","nvisits":2,"waitseconds":15,"nthrun":35,"time":1414422197759,"target":["Sports+Sport Scores & Statistics","Autos & Vehicles+Commercial Vehicles","Beauty & Fitness+Spas & Beauty Services"],"google_interests":["Beauty & Fitness","Beauty & Fitness+Spas & Beauty Services","Business & Industrial","Finance+Insurance","Jobs & Education+Education","Jobs & Education+Jobs","News+Sports News","Sports","Sports+Team Sports","Travel+Hotels & Accommodations"]}''')
    obj = read_single_file(content)
    eq_(obj['time'], 35)

def test_single_string():
    '''even if nthrun is a string, an integer is expected'''
    content = StringIO('''
    {"url":"http://asd","interest":"Autos & Vehicles/Commercial Vehicles","nvisits":2,"waitseconds":15,"nthrun":"35","time":1414422197759,"target":["Sports+Sport Scores & Statistics","Autos & Vehicles+Commercial Vehicles","Beauty & Fitness+Spas & Beauty Services"],"google_interests":["Beauty & Fitness","Beauty & Fitness+Spas & Beauty Services","Business & Industrial","Finance+Insurance","Jobs & Education+Education","Jobs & Education+Jobs","News+Sports News","Sports","Sports+Team Sports","Travel+Hotels & Accommodations"]}''')
    obj = read_single_file(content)
    eq_(obj['time'], 35)


def test_series():
    '''a serie is sorted by nthrun'''
    c1 = StringIO('''{"nthrun":"2","target":["Sports+Sport Scores & Statistics","Autos & Vehicles+Commercial Vehicles","Beauty & Fitness+Spas & Beauty Services"],"google_interests":["Beauty & Fitness","Beauty & Fitness+Spas & Beauty Services","Business & Industrial","Finance+Insurance","Jobs & Education+Education","Jobs & Education+Jobs","News+Sports News","Sports","Sports+Team Sports","Travel+Hotels & Accommodations"]}''')
    c2 = StringIO('''{"nthrun":"10","target":["Sports+Sport Scores & Statistics","Autos & Vehicles+Commercial Vehicles","Beauty & Fitness+Spas & Beauty Services"],"google_interests":["Beauty & Fitness","Beauty & Fitness+Spas & Beauty Services","Business & Industrial","Finance+Insurance","Jobs & Education+Education","Jobs & Education+Jobs","News+Sports News","Sports","Sports+Team Sports","Travel+Hotels & Accommodations"]}''')

    s = get_series(iterable=map(read_single_file, deepcopy((c1, c2))))
    eq_(s[0]['time'], 2)
    eq_(s[1]['time'], 10)
    s = get_series(iterable=map(read_single_file, deepcopy((c2, c1))))
    eq_(s[0]['time'], 2)
    eq_(s[1]['time'], 10)


def test_fill():
    serie = [
            {'time': 1, 'val': 'foo'},
            {'time': 2, 'val': 'asd'}
            ]
    eq_(serie, fill(serie))


def test_fill_start():
    serie = [
            {'time': 2, 'val': 'asd'}
            ]
    eq_(fill(serie), [
        {'time': 1},
        {'time': 2, 'val': 'asd'}
        ])
    eq_(fill(serie, {'val': 'foo'}), [
        {'time': 1, 'val': 'foo'},
        {'time': 2, 'val': 'asd'}
        ])


def test_fill_middle():
    serie = [
            {'time': 1, 'val': 'a'},
            {'time': 2, 'val': 'as'},
            {'time': 5, 'val': 'zap'},
            ]
    expect = [
            {'time': 1, 'val': 'a'},
            {'time': 2, 'val': 'as'},
            {'time': 3, 'val': 'as'},
            {'time': 4, 'val': 'as'},
            {'time': 5, 'val': 'zap'},
            ]
    eq_(fill(serie), expect)


# vim: set nowrap ts=4 sw=4 et:
