#!/usr/bin/env python2.7
'''
This script will make plots out of his file arguments
'''
import argparse
import logging
from itertools import izip, cycle, repeat
import functools

import matplotlib.pyplot as plt

from plot_common import label_map
import mantra.argutils


def get_data(fname, header=False):
    '''read data from fname. If header is True, skip first line'''
    def sub_reader(iterable):
        for line in iterable:
            yield map(float, line.split('\t'))
    with open(fname) as buf:
        if header:
            next(buf)
        return zip(*sub_reader(buf))


def plot(all_data, fname, args):
    '''
    all_data is a sequence of couples of arrays.

    Each couple contains the two arrays with the data
    '''
    has_arg = functools.partial(mantra.argutils.is_set_arg, args)
    if args.alternate_linewidth is None:
        lw_gen = repeat(args.linewidth)
    else:
        lw_gen = cycle((args.linewidth, args.alternate_linewidth))
    for info, marker, dashes, lw in izip(all_data,
            cycle('oxvsD*'), cycle(((20, 0), (5, 5))), lw_gen):
        data = info['data']
        if not data:  # empty
            logging.info("{} is empty, skipping".format(info['label']))
            continue
        style = dict(dashes=dashes) if dashes[1] != 0 else dict(ls='-')
        p, = plt.plot(*data, lw=lw,
                marker=marker, fillstyle='none', markevery=0.16,
                markeredgewidth=1.5,
                markersize=args.ms, aa=True,
                label=info['label'], **style)
    plt.grid(True)
    legend = plt.legend(loc=0,  # upper right
            title=args.legend_title if has_arg('legend_title') else None,
            prop={'size': 20}
            )
    plt.setp(legend.get_title(), fontsize='large')
    for lab in legend.get_texts():
        plt.setp(lab, fontsize='large')

    if args.title is not None:
        plt.title(args.title)

    plt.xscale(args.xscale, nonposx='clip')
    xmin, xmax = plt.xlim()
    if has_arg('xmin'):
        xmin = args.xmin
    if has_arg('xmax'):
        xmax = args.xmax
    plt.xlim((xmin, xmax))
    ymin, ymax = plt.ylim()
    if has_arg('ymin'):
        ymin = args.ymin
    if has_arg('ymax'):
        ymax = args.ymax
    plt.ylim((ymin, ymax))

    plt.xlabel(args.xlabel)
    plt.ylabel(args.ylabel)
    plt.savefig(fname)


def get_parser():
    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            description='Calculate Time To Arrival for a directory')
    display = parser.add_argument_group('display')
    display.add_argument('--title')
    display.add_argument('--xscale', choices=['log', 'linear'], default='log')
    display.add_argument('--ymax', type=float)
    display.add_argument('--ymin', type=float)
    display.add_argument('--xmax', type=float)
    display.add_argument('--xmin', type=float)
    display.add_argument('--xlabel', metavar='LABEL', default='runs')
    display.add_argument('--ylabel', metavar='LABEL')
    display.add_argument('--legend-title', metavar='TITLE')
    display.add_argument('--ms', '--markersize', default=9, type=float)
    display.add_argument('--linewidth', default=0.5, type=float)
    display.add_argument('--alternate-linewidth', type=float,
            help='If specified, there are two alternating linewidths')
    parser.add_argument('datafile', type=str, nargs='+',
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--json', action='store_true', default=False,
            help='will use the JSON file format for datafile')
    parser.add_argument('--output', metavar='IMAGEFILE', required=True)
    parser.add_argument('--loglevel', metavar='LEVEL', type=str,
                        default='INFO')
    return parser


def main():
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)

    if args.json:
        import json
        if len(args.datafile) != 1:
            raise Exception("json => exactly 1 datafile")
        with open(args.datafile[0]) as buf:
            alldata = json.load(buf)
        data = [dict(data=zip(*val),
            label=label_map(key))
            for key, val in alldata.items()]
    else:
        data = [dict(data=get_data(f, header=True), label=label_map(f))
                for f in args.datafile]
    plot(data, args.output, args)

if __name__ == '__main__':
    main()

# vim: set ft=python et ts=4 sw=4:
