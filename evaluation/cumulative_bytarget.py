#!/usr/bin/env python2.7
import logging
import json
from collections import defaultdict

from cumulative import counter_list, cumulative


def get_enh_alltargets(bytarget, args):
    ret = {}
    for category in bytarget:
        ret[category] = bytarget[category]['details'][args.arrivalkind
                + 'time']
    return ret


def all_bytargets(args):
    merged = defaultdict(list)
    obj = json.load(args.bytarget)
    ret = get_enh_alltargets(obj, args)
    for category, arrival in ret.items():
        merged[category] = [x if x is not None else 'INFINITY'
                for x in arrival]

    for category in merged:
        merged[category].sort(reverse=True)
    return merged


def cumulate_bytarget(merged):
    d = {}
    for category in merged:
        d[category] = cumulative(merged[category])
    return d


def cumulated_out(cumulated, args):
    d = {}
    for category in cumulated:
        d[category] = list(
                counter_list(cumulated[category],
                    inf=args.infinity,
                    rel=args.relative))
    return d


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate cumulative TTA for all targets',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument(dest='bytarget', metavar='JSON',
            type=argparse.FileType('r'))
    parser.add_argument('--outfile', default='-',
            metavar='JSON', help='Will dump a JSON structured output',
            type=argparse.FileType('w'))
    parser.add_argument('--arrivalkind', default='toparrival',
            help='the metric to use')
    parser.add_argument('--infinity', action='store_true',
            help='Print INIFINITY in output')
    parser.add_argument('--relative', action='store_true',
            help='Print relative values')

    return parser


def main(args):
    logging.basicConfig(level=args.loglevel)
    cumulated = cumulate_bytarget(all_bytargets(args))
    json.dump(cumulated_out(cumulated, args), args.outfile)

if __name__ == '__main__':
    main(get_parser().parse_args())
