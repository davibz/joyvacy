'''
This module will calculate the noise for a session

Parameters are a directory and a "target". Noise is the number of categories
that are not in the target but are in the collected interests.
'''
from __future__ import division

import logging
import argparse
import json
import os.path

from parsers import fill_timeserie
from average import average

VALID_METRICS = ['noise', 'topnoise', 'accuracy', 'topaccuracy']


def read_dirnoise(fpath):
    with open(fpath) as buf:
        logging.debug('reading ' + fpath)
        base = {'abs': 0, 'rel': 0}
        content = json.load(buf)
    runs = content['runs'] if content else []
    return fill_timeserie(runs,
            prev_time=-1,
            default_obj={metric: base for metric in VALID_METRICS})


def noise_average(noisefiles, metric='noise'):
    assert metric in VALID_METRICS
    for noisepath in noisefiles:
        if not os.path.exists(noisepath) or not os.path.isfile(noisepath):
            raise ValueError('Error reading file')

    noises = map(read_dirnoise, noisefiles)
    while True:
        now = []
        for n in noises:
            try:
                now.append(next(n))
            except StopIteration:
                continue
        if not now:
            break
        assert len(set([n['time'] for n in now])) == 1, now
        values = [n[metric]['abs'] for n in now]
        ok = [type(v) is int for v in values]
        assert all(ok), str(ok.index(False))

        average_now = average(values)
        yield now[0]['time'], average_now


def main_average(args):
    if args.header:
        args.outfile.write('STEP\tNoise\n')
    for time, avg in noise_average(args.noisefiles, metric=args.metric):
        args.outfile.write('%d\t%.2f\n' % (time, avg))
    logging.info('finished')
    args.outfile.close()
    logging.info('exit')


def get_parser():
    parser = argparse.ArgumentParser(
        description='Calculate Noise')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    sub = parser.add_subparsers(dest='subcommand')

    average = sub.add_parser('average',
            description='Aggregate lot of dirnoise')
    average.set_defaults(func=main_average)
    average.add_argument('noisefiles', metavar='NOISEFILE', type=str,
            nargs='+', help='noise info file, as produced by "dir" subcommand')
    average.add_argument('--header', action='store_true', default=False)
    average.add_argument('--metric', choices=VALID_METRICS,
            default=VALID_METRICS[0])
    average.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))

    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    args.func(args)
