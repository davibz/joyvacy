import sys
import functools
import logging
import argparse
import os
import os.path

import parsers
import metrics
from single_plot import series_to_diffs


def average(iterable):
    length = 0
    s = 0
    for val in iterable:
        s += val
        length += 1
    if length is 0:
        return None
    return float(s) / length


def get_parser():
    parser = argparse.ArgumentParser(
        description='Make metric directory over a bunch of runs')
    parser.add_argument('directory', metavar='DIRNAME', type=str, nargs='+',
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--not-wanted-weight', metavar='WEIGHT', type=float,
                        default=1)
    parser.add_argument('--not-reached-weight', metavar='WEIGHT', type=float,
                        default=1)
    parser.add_argument('--loglevel', metavar='LEVEL', type=str,
                        default='INFO')
    parser.add_argument('--out', metavar='FILENAME', type=str, default='-',
                        help='Write output to FILENAME')
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    metric = functools.partial(metrics.weighted_set_difference,
            notreached_weight=args.not_reached_weight,
            notwanted_weight=args.not_wanted_weight)
    # process = series_to_gnuplot_data(metric, to_pipe)
    # aggregate(sys.argv[1:], process)
    paths = args.directory
    logging.debug('Running on %s' % ':'.join(paths))

    for path in paths:
        if not os.path.exists(path):
            logging.error("Path '%s' does not exist" % path)
            sys.exit(1)
        if not os.path.isdir(path):
            logging.error("Path '%s' is not a directory" % path)
            sys.exit(1)

    diffs = {}
    for path in paths:
        diffs[path] = series_to_diffs(parsers.get_series(path), metric)

    logging.debug(str(diffs))

    while True:
        try:
            line = [next(diffs[path]) for path in diffs]
        except StopIteration:
            break
        print average(line)

# vim: set ts=4 sw=4 et autoindent smartindent:
