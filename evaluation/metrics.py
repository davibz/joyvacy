def weighted_set_difference(target_el, actual_el, notreached_weight=1, notwanted_weight=1):
    target = set(target_el)
    actual = set(actual_el)
    return len(target - actual) * notreached_weight + \
            len(actual - target) * notwanted_weight

