#!/usr/bin/env python
'''
This code attempt to show that google is a multiclass classifier.

It does so by looking at experiments where at some point the number of visited
pages is inferior to the number of attributed interests
'''
import argparse
import logging
import json
from itertools import islice

from parsers import read_session


def check_now(run_info):
    '''
    check that a proof of google being a multiclass is available at that point
    '''
    return len(run_info['state']) > run_info['visits_done']


def check_every(directory, stopafter):
    for run in islice(read_session(directory,
        skiperrors=True), stopafter):
        if check_now(run):
            yield run['time']


def get_parser():
    parser = argparse.ArgumentParser(
        description='Calculate Noise')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')
    parser.add_argument('directory', metavar='DIRNAME', type=str,
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--stopafter', metavar='N_RUNS', default=10, type=int,
            help='after a certain number of visit, '
            'it is pointless to continue')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))

    return parser


def main(args):
    evidences = list(check_every(args.directory, args.stopafter))
    json.dump({'evidences': evidences}, args.outfile)


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
