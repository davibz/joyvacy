#!/usr/bin/env python2
'''
If google ads parsers is broken, we have a session where the 'state' of each run
is always empty.

Those are BAD experiments, and we need to find them
'''
from __future__ import print_function, division
import json
import logging

from mantra.top import top_set
from mantra.paths import get_sessions
from average import average


def goodbad(diff, interest, target):
    assert interest.issubset(target), \
            '{} should be contained in {}'.format(interest, target)
    url_noise = diff - interest

    good = url_noise.intersection(target)
    assert len(good) <= len(url_noise)
    return len(good), len(url_noise)


def goodbad_noise(session, args):
    toset = set if args.level == 2 else top_set
    good_noise = 0
    bad_noise = 0
    for run in session.get('runs', []):
        if not run['google_diff']:
            continue
        g, b = goodbad(toset(run['google_diff']['new']),
                toset({run['url_interest'].replace('/', '+')}),
                toset(session['info']['target']))
        good_noise += g
        bad_noise += b
# ok, sth changed. is there noise?
    return good_noise, bad_noise


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Check if a session has never any state')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('experiments', metavar='ENHANCED_SESSION',
            type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))

    return parser


def get_ratios(sessions, args):
    for en in sessions:
        g, b = goodbad_noise(json.load(en), args)
        if b == 0:
            continue
        yield g / b


def main(args):
    for exp in args.experiments:
        en = json.load(exp)
        if not en:
            continue
        if not any((bool(run['state']) for run in en['runs'])):
            args.outfile.write('%s\n' % exp.name)

if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
