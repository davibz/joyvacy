#!/usr/bin/env python2
'''
During a session, noise is created.
It could be the case that noise relatively to the visit is not noise relatively
to the target

we define:
    bad noise = the appeared interests that are not url_interest
    good noise = the appeared interests that are not url_interests but are in
        target
'''
from __future__ import print_function, division
import json
import logging

from mantra.top import top_set
from mantra.paths import get_sessions
from average import average


def goodbad(diff, interest, target):
    assert interest.issubset(target), \
            '{} should be contained in {}'.format(interest, target)
    url_noise = diff - interest

    good = url_noise.intersection(target)
    assert len(good) <= len(url_noise)
    return len(good), len(url_noise)


def goodbad_noise(session, args):
    toset = set if args.level == 2 else top_set
    good_noise = 0
    bad_noise = 0
    for run in session.get('runs', []):
        if not run['google_diff']:
            continue
        g, b = goodbad(toset(run['google_diff']['new']),
                toset({run['url_interest'].replace('/', '+')}),
                toset(session['info']['target']))
        good_noise += g
        bad_noise += b
# ok, sth changed. is there noise?
    return good_noise, bad_noise


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Calculate Noise')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('experiments', metavar='ENHANCED_SESSION',
            type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    parser.add_argument('--level', metavar='N', default=1,
            choices=[1, 2], type=int, help='top level or second level?')

    return parser


def get_ratios(sessions, args):
    for en in sessions:
        g, b = goodbad_noise(json.load(en), args)
        if b == 0:
            continue
        yield g / b


def main(args):
    args.outfile.write('%f' % average(ratio for ratio in
        get_ratios(args.experiments, args)))

if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
