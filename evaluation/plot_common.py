import re
import os.path
import functools

import numpy as np
import matplotlib.pyplot as plt
import mantra.argutils


def plot_histogram(data, args):
    '''data must be a dict of kind label: value'''
    has_arg = functools.partial(mantra.argutils.is_set_arg, args)
    labels = sorted(data.keys())
    values = [data[l] for l in labels]
    fig = plt.figure().add_subplot(111)
    barargs = {}
    if has_arg('bar_color'):
        barargs['color'] = args.bar_color

    fig.bar(np.arange(len(values)), values, args.bar_width, hatch='/',
            **barargs)
    if args.title is not None:
        fig.set_title(args.title)
    if 'xlabel' in args and args.xlabel is not None:
        plt.xlabel(args.xlabel)
    if 'ylabel' in args and args.ylabel is not None:
        plt.ylabel(args.ylabel)
    fig.set_xticks(np.arange(len(values)) + (args.bar_width / 2.0))
    xtickNames = fig.set_xticklabels([l[:20] for l in labels])
    if has_arg('font_size'):
        plt.setp(xtickNames, rotation=90, fontsize=args.font_size)
    if has_arg('label_space'):
        plt.gcf().subplots_adjust(bottom=args.label_space)

    ymin, ymax = plt.ylim()
    if has_arg('ymin'):
        ymin = args.ymin
    if has_arg('ymax'):
        ymax = args.ymax
    plt.ylim((ymin, ymax))
    plt.xlim([0, len(values)])
    if args.outfile is not None:
        plt.savefig(args.outfile)
    else:
        print("What's the point of plotting without saving?")


def label_map(orig):
    m = re.search('([0-9]*)_reach', orig)
    if m is not None:
        return m.group(1)
    if os.path.exists(orig):
        fileparts = orig.split(os.path.sep)[:-1]
        numparts = [part for part in fileparts
                if re.search(r'[0-9]', part) is not None]
        if numparts:
            return numparts[-1]
    return orig
