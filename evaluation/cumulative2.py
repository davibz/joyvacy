#!/usr/bin/env python2.7
import logging
import json

from cumulative import format_gnuplot, cumulative


def get_enh_all(args):
    for buf in args.enhanced_files:
        enh = json.load(buf)
        if not enh:
            # invalid
            continue
        if args.arrivalkind not in enh['moments']:
            yield 'INFINITY'
            continue
        arrival = enh['moments'][args.arrivalkind]['run']['time']
        if arrival is None:
            yield 'INFINITY'
            continue
        yield arrival


def get_enh_results(args):
    return sorted(get_enh_all(args), reverse=True)


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate Noise',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument(dest='enhanced_files', metavar='JSON',
            type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--outfile', default='-', type=argparse.FileType('w'))
    parser.add_argument('--arrivalkind', default='arrival',
            help='the metric to use')
    parser.add_argument('--infinity', action='store_true',
            help='Print INIFINITY in output')
    parser.add_argument('--relative', action='store_true',
            help='Print relative values')

    return parser


def main(args):
    logging.basicConfig(level=args.loglevel)
    for line in format_gnuplot(cumulative(get_enh_results(args)),
            inf=args.infinity, rel=args.relative):
        args.outfile.write(line.strip())
        args.outfile.write('\n')

if __name__ == '__main__':
    main(get_parser().parse_args())
