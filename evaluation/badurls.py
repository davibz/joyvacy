'''
find (maybe) bad URLs based on how many categories appears after it
'''
import json
import logging

from utils import iter_with_previous


def is_bad(run, min_diff):
    # TODO: ignore target in diff!
    badness = len(run['google_diff']['new']) - min_diff
    if badness > 0:
        return badness
    return False


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate Time To Arrival for a directory')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('enhsession', metavar='ENHANCED_SESSION',
            type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--previous', metavar='PREV_URLS',
            type=int, default=0)
    parser.add_argument('--min-diff', metavar='PREV_URLS',
            type=int, default=1)
    parser.add_argument('--outfile', metavar='TARGET',
            type=argparse.FileType('wb'), default='-')
    return parser


def bad_urls(enhanced, args):
    for context in iter_with_previous(enhanced['runs'], args.previous + 1):
        badness = is_bad(context[-1], args.min_diff)
        if badness is not False:
            yield dict(badness=badness,
                    urls=[run['url'] for run in context if run is not None],
                    url_target=run['url_interest'])


def main(args):
    urls = []
    for session in args.enhsession:
        enhanced = json.load(session)
        session.close()
        if not enhanced:
            continue
        urls += bad_urls(enhanced, args)
    json.dump(dict(bad=urls), args.outfile)


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
