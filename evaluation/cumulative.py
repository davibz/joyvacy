#!/usr/bin/env python2.7
'''
This script will take every file in input and read them as numerical.
If the file is empty, it is assumed that the value is "infinite"
'''
from collections import Counter
import argparse
import logging


def get_content(fname):
    with open(fname) as buf:
        val = buf.read()
    if val.strip() == 'invalid':
        return None
    if val:
        return int(val)
    return 'INFINITY'


def get_results(files):
    contents = (get_content(fname) for fname in files)
    return sorted((val for val in contents if val is not None),
            reverse=True)


def cumulative(elements):
    '''assume that elements is sorted'''
    count = Counter()
    for n in elements:
        if n not in count:
            count[n] = 0
        if n == 'INFINITY':
            count[n] += 1
        else:
            for key in count:
                if key != 'INFINITY':
                    count[key] += 1
    return count


def counter_list(counter, inf=False, rel=False):
    total = 0
    for key in counter:
        if key != 'INFINITY':
            if counter[key] > total:
                total = counter[key]
    total += counter['INFINITY']

    def out(n):
        return float(n) / total if rel else n

    for key in sorted(counter.keys()):
        if key != 'INFINITY':
            yield key, out(counter[key])
    if inf:
        yield 'INFINITY', out(counter['INFINITY'])


def format_gnuplot(counter, inf=False, rel=False):
    yield 'STEP\t{}\n'.format('Ratio' if rel else 'N')
    for index, value in counter_list(counter, inf, rel):
        yield '{}\t{}\n'.format(index, value)


def get_parser():
    parser = argparse.ArgumentParser(
        description='Calculate Time To Arrival for a directory')
    parser.add_argument('file', type=str, nargs='+',
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--infinity', action='store_true',
            help='Print INFINITY in output')
    parser.add_argument('--relative', action='store_true',
            help='Print relative values')
    parser.add_argument('--loglevel', metavar='LEVEL', type=str,
                        default='INFO')
    return parser


def main():
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    for line in format_gnuplot(cumulative(get_results(args.file)),
            inf=args.infinity, rel=args.relative):
        print line.strip()

if __name__ == '__main__':
    main()

# vim: set ft=python et ts=4 sw=4:
