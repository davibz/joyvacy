'''
This test aims to discover if some categories are too often assigned by google
'''
import logging
log = logging.getLogger(__name__)
import os.path
import json
from collections import Counter, OrderedDict

from parsers import read_session


def pick_run(dirpath, nth):
    for run in read_session(dirpath, skiperrors=True):
        if run['time'] == nth:
            return run
    return None


def find_runs(args):
    for exp in os.listdir(args.experiment_dir):
        exp_path = os.path.join(args.experiment_dir, exp)
        if not os.path.isdir(exp_path):
            log.info('discarding {}'.format(exp_path))
            continue
        if not exp.isdigit():  # convention over directory naming
            continue

        run = pick_run(exp_path, args.run_time)
        if run is None:
            log.warning('Cannot found valid run for {}'.format(exp))
            continue
        yield run


def count_categories(args):
    category_counter = Counter()
    top_counter = Counter()
    toprepeated = Counter()
    for run in find_runs(args):
        category_counter.update(run['state'])
        tops = [ctg.split('+')[0] for ctg in run['state']]
# note that if a run contains A+B, A+C, A+D, only one is added for A
        top_counter.update(set(tops))
# in toprepeated, instead, 3 are added
        toprepeated.update(tops)

    return {
        'cats': category_counter,
        'topuniq': top_counter,
        'toprepeated': toprepeated
        }


def main(args):
    counters = count_categories(args)
    formatted_counters = OrderedDict()
    for name, counter in counters.items():
        fmt_counter = OrderedDict()
        for el, num in counters[name].most_common():
            fmt_counter[el] = num
        formatted_counters[name] = fmt_counter
    json.dump(formatted_counters,
        args.outfile, indent=2
        )


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Understand if some categories are too frequent')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('experiment_dir',
            help='dir callecting one dir for each experiment')
    parser.add_argument('run_time', type=int,
            help='Check only at this time')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
