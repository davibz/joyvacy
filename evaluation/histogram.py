#!/usr/bin/env python2.7
import logging

from plot_common import plot_histogram, label_map


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Generic histogram plot',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('valuefile', metavar='TXT',
            type=str, nargs='+')
    parser.add_argument('--label-in-filename', default=False,
            action='store_true',
            help='If set, filenames should be label:filename')
    parser.add_argument('--outfile', metavar='OUTFILE')

    graphic = parser.add_argument_group('graphic options')
    graphic.add_argument('--bar-width', type=float, default=0.30,
            help='width of a single bar')
    graphic.add_argument('--bar-color', default='#dddddd',
            help='width of a single bar')
    graphic.add_argument('--font-size', type=int,
            help='font size')
    graphic.add_argument('--title')
    graphic.add_argument('--ymin', type=float, help='Axis range')
    graphic.add_argument('--ymax', type=float, help='Axis range')
    graphic.add_argument('--xlabel', metavar='LABEL')
    graphic.add_argument('--ylabel', metavar='LABEL')
    graphic.add_argument('--label-space', type=float, default=0.25,
            help='room to reserve for xlabels')

    return parser


def get_data(args):
    for valuefile in args.valuefile:
        if args.label_in_filename:
            label, fname = valuefile.split(':', 1)
        else:
            fname = valuefile
            label = label_map(fname)
        with open(fname) as buf:
            yield label, float(buf.read())


def main(args):
    plot_histogram(dict(get_data(args)), args)


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
