from nose.tools import eq_

from goodnoise import goodbad


def test_empty():
    eq_(goodbad(set(), {1}, {1, 2}), (0, 0))


def test_only_bad():
    eq_(goodbad({10}, {1}, {1, 2}), (0, 1))
    eq_(goodbad({10, 11}, {1}, {1, 2}), (0, 2))
    eq_(goodbad({10, 11, 20}, {1}, {1, 2}), (0, 3))


def test_all_good():
    '''here, good noise = bad noise'''
    eq_(goodbad({1}, {1}, {1, 2}), (0, 0))
    eq_(goodbad({1, 2}, {1}, {1, 2}), (1, 1))


def test_mixed():
    eq_(goodbad({1, 2, 3}, {1}, {1, 2}), (1, 2))
