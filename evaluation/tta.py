#!/usr/bin/env python2.7
'''
This module will calculate the TTA (Time To Appear) for a directory.

Parameters are a directory and a "target". TTA is the smallest i such that
${target} in ${results[i]}, where files is a sorted array of files
'''
import json
import logging
import argparse


def get_parser():
    parser = argparse.ArgumentParser(
        description='Calculate Time To Arrival for a directory')
    parser.add_argument('--loglevel', metavar='LEVEL', type=str,
                        default='INFO')
    parser.add_argument('enhsession', metavar='ENHANCED_SESSION',
            type=argparse.FileType('r'),
                        help='input directory (created by make ffrun_eval)')
    parser.add_argument('--target', metavar='TARGET', type=str, nargs='*',
            default=None)
    parser.add_argument('--outfile', default='-',
            type=argparse.FileType('w'))
    return parser


def main(args):
    obj = json.load(args.enhsession)
    if not obj:
        args.outfile.write('invalid')
        return
    tta = obj['metrics']['arrival']
    args.enhsession.close()
    if tta is not None:
        args.outfile.write(str(tta))

if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)

# vim: set ft=python et ts=4 sw=4:
