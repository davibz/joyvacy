#!/usr/bin/env python2.7
import logging
import json

from average import average

from mantra.constants import METRICLIST, MOMENTSLIST


def get_values(args):
    for session in args.enhanced_sessions:
        with open(session) as buf:
            obj = json.load(buf)
        if not obj:
            continue
        moment = obj['moments'].get(args.arrivalmoment, None)
        if moment is None:
            continue
        yield moment['run'][args.metric][args.absoluteness]


def main(args):
    avg = average(get_values(args))
    if avg is None:
        args.outfile.write('\n')
    else:
        args.outfile.write('%f\n' % avg)


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate average noise at arrival. '
        'Non-arrived sessions are skipped',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    metrics = parser.add_argument_group('metric options')
    metrics.add_argument('--rel', dest='absoluteness',
        action='store_const', const='rel',
            default='abs', help='get metric relative to the number of targets')
    metrics.add_argument('--metric', default='noise',
            choices=METRICLIST,
            help='Which metric to use')

    parser.add_argument('enhanced_sessions',
            help='dir containing one json file per run', nargs='+')
    metrics.add_argument('--arrivalmoment', default='toparrival',
            choices=MOMENTSLIST,
            help='Which moment should be chosen')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
