#!/usr/bin/env python
from __future__ import division

import logging
from collections import defaultdict
import json

from mantra.top import top_set


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate Noise')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('enhsession', metavar='ENHANCED_SESSION',
            type=argparse.FileType('r'), nargs='+')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    parser.add_argument('--level', metavar='N', default=1,
            choices=[1, 2], type=int, help='top level or second level?')

    return parser


def main(args):
    results = defaultdict(lambda: {
        'details': defaultdict(list),
        'aggregate': {}
        })
    for session_buf in args.enhsession:
        session = json.load(session_buf)
        if not session:
            continue
        target = session['info']['target']
        if args.level == 1:
            target = top_set(target)
        if len(target) > 1:
            raise Exception("bytarget does not support multiple targets")
        target = target.pop()

        try:
            val = session['moments']['arrival']['run']['topnoise']['abs']
        except KeyError:
            val = None
        results[target]['details']['toparrivalnoise'].append(val)

        try:
            val = session['moments']['toparrival']['run']['time']
        except KeyError:
            val = None
        results[target]['details']['toparrivaltime'].append(val)

        try:
            val = session['moments']['arrival']['run']['time']
        except KeyError:
            val = None
        results[target]['details']['arrivaltime'].append(val)

        try:
            val = session['moments']['arrival']['run']['topaccuracy']['abs']
        except KeyError:
            val = None
        results[target]['details']['topaccuracy'].append(val)

    for cat in results:
        aggr = results[cat]['aggregate']
        for metric in results[cat]['details']:
            all_data = results[cat]['details'][metric]
            if not all_data:
                continue
            aggr['NO_' + metric] = all_data.count(None) / len(all_data)

            arrived_data = [x for x in all_data if x is not None]
            if not arrived_data:
                aggr['avg0_' + metric] = None
                aggr['avg_' + metric] = None
            else:
                sum_data = sum(arrived_data)
                aggr['avg0_' + metric] = sum_data / len(all_data)
                aggr['avg_' + metric] = sum_data / len(arrived_data)
    json.dump(results, args.outfile)


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
