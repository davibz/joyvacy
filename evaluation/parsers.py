from __future__ import print_function

import os
import os.path
import json
from logging import getLogger
log = getLogger('parsers')
from copy import deepcopy
from warnings import warn
from zipfile import is_zipfile, ZipFile

from mantra.clean import cat_is_ok


def read_single_file(buf):
    obj = json.load(buf)
    if any((not cat_is_ok(c.split('+')) for c in obj['target'])):
        log.warning('Invalid target for {}'.format(buf.name))
        return None
    return {
            'time': int(obj['nthrun']),
# obj[nvisits] is the number of clicks + 1 (yes, odd)
# 1 should be added to it (mainpage) so obj[nvisits] + 1 - 1 = obj[nvisits]
            'visits_done': int(obj['nthrun']) * (int(obj['nvisits'])),
            'target': frozenset(obj['target']),
            'par_targetsize': len(obj['target']),
            'state': frozenset(cat for cat in obj['google_interests']
                    if cat_is_ok(cat.split('+'))
                    ),
            'url': obj['url'],
            'url_interest': obj['interest']
            }


def read_directory(*args, **kwargs):
    warn("use read_session instead", DeprecationWarning)
    for run in read_session(*args, **kwargs):
        yield run


def read_session(dirpath, **kwargs):
    if is_zipfile(os.path.join(dirpath, 'session.zip')):
        iterable = read_session_zip(os.path.join(dirpath, 'session.zip'),
                **kwargs)
    elif is_zipfile(dirpath):
        iterable = read_session_zip(dirpath, **kwargs)
    else:
        iterable = read_session_dir(dirpath, **kwargs)
    first = True
    for run in iterable:
        if first:
            first = False
            if run is None:
                break

        yield run


def read_session_zip(zippath, skiperrors=False):
    z = ZipFile(zippath)
    for fname in sorted(z.namelist()):
        if not fname.endswith('.json'):
            continue
        try:
            yield read_single_file(z.open(fname))
        except ValueError as exc:
            if not skiperrors:
                raise exc


def read_session_dir(dirpath, skiperrors=False):
    for basename in sorted(os.listdir(dirpath)):
        if not basename.endswith('.json'):
            continue
        fpath = os.path.join(dirpath, basename)
        log.debug('Reading {}'.format(fpath))
        try:
            yield read_single_file(open(fpath))
        except ValueError as exc:
            if not skiperrors:
                raise exc


def fill_timeserie(serie, default_obj={}, prev_time=0):
    prev_obj = default_obj
    sorted_serie = sorted(serie, key=lambda i: i['time'])
    for item in sorted_serie:
        if item['time'] <= prev_time:
            continue
        for time in xrange(prev_time + 1, item['time']):
            new_item = deepcopy(prev_obj)
            new_item['time'] = time
            log.debug('inserted {}'.format(new_item))
            yield new_item
        log.debug('std {}'.format(item))
        yield item
        prev_time = item['time']
        prev_obj = item


def get_series(directory=None, iterable=None):
    if directory is None:
        assert iterable is not None
    else:
        iterable = read_directory(directory)
    return sorted(iterable, key=lambda r: r['time'])


# vim: set ts=4 sw=4 et:
