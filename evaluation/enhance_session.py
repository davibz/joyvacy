#!/usr/bin/env python2
'''
given a single session, will transform  the whole bunch of json files into a
single one. Also, it will enhance data with google "diff"
'''
import json
import logging
log = logging.getLogger(__name__)

from parsers import read_session
from mantra import metrics
from mantra.top import top_set


def get_diff(prevrun, newrun, field='state'):
    diff = {}
    old = set(prevrun.get(field, []))
    new = set(newrun[field])
    diff['lost'] = old - new
    diff['new'] = new - old
    return diff


def is_arrived(run):
    return run['target'].issubset(run['state'])


def is_toparrived(run):
    return top_set(run['target']).issubset(top_set(run['state']))


def get_transformed_session(args):
    previous = {}
    newruns = []
    first = True
    info = {}
    session_metrics = dict(arrival=None, noiseatarrival=None)
    moments = {}
    runs = read_session(args.session_dir, skiperrors=True)
    for run in runs:
        if first:
            if run is None:
                return {}
            first = False
            info['target'] = run['target']
            info['par_targetsize'] = run['par_targetsize']

        run['google_diff'] = get_diff(previous, run)
        run['arrived'] = is_arrived(run)
        run['toparrived'] = is_toparrived(run)
        run['accuracy'] = {'abs': metrics.accuracy(run['target'],
            run['state'])}
        run['topaccuracy'] = {'abs': metrics.topaccuracy(run['target'],
            run['state'])}
        run['noise'] = metrics.noise_info(run['target'], run['state'],
                metrics.traditional_noise)
        run['topnoise'] = metrics.noise_info(run['target'], run['state'],
                metrics.top_noise)
        thismoment = {'run': run, 'idx': len(newruns)}
        if run['toparrived']:
            if 'toparrival' not in moments:
                moments['toparrival'] = thismoment
        if run['arrived']:
            if 'arrival' not in moments:
                moments['arrival'] = thismoment
            if session_metrics['arrival'] is None:
                session_metrics['arrival'] = run['time']
                session_metrics['noiseatarrival'] = run['noise']
        del run['target']
        del run['par_targetsize']
        newruns.append(run)
        previous = run

    if not newruns:
        return {}

    if 'arrival' in moments:
        moments['arrivalorlast'] = moments['arrival']
    else:
        moments['arrivalorlast'] = {'run': newruns[-1],
                'idx': len(newruns) - 1}

    if 'toparrival' in moments:
        moments['toparrivalorlast'] = moments['toparrival']
    else:
        moments['toparrivalorlast'] = {'run': newruns[-1],
                'idx': len(newruns) - 1}

    if args.skip_small_nonarrived:
        if 'arrival' not in moments:
                if max((r['time'] for r in newruns)) < args.skip_small_nonarrived \
                            or not newruns:
                    return {}

    return {'info': info, 'runs': newruns, 'metrics': session_metrics,
            'moments': moments}


def main(args):
    session = get_transformed_session(args)
    json.dump(session,
            args.outfile,
            default=lambda x: sorted(tuple(x))
            )


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Produce easier to analyze session files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    parser.add_argument('session_dir',
            help='dir containing one json file per run')
    parser.add_argument('--skip-small-nonarrived', type=int,
            metavar='LENGTH', default=0,
            help="non-top-arrived sessions smaller than this will result in"
            " empty session generation"
            )
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
