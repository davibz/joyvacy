#!/usr/bin/env python2.7
from __future__ import division
import logging
import json
from collections import Counter

from mantra.constants import MOMENTSLIST
from mantra.top import top_set


def get_values(args):
    for session in args.enhanced_sessions:
        with open(session) as buf:
            enh = json.load(buf)
        if not enh:
            continue
        moment = enh['moments'].get(args.arrivalmoment, None)
        if moment is None:
            continue
        yield set(enh['info']['target']), set(moment['run']['state'])


def count(args):
    cats = Counter()
    topcats = Counter()
    tot = 0
    for target, state in get_values(args):
        tot += 1
        diff = state - target
        cats.update(diff)
        topcats.update(top_set(diff))

    def rel_counter(counter, total):
        return {name: counter[name] / total
                for name in counter}
    return {
            'categories': rel_counter(cats, tot),
            'top': rel_counter(topcats, tot)
            }


def main(args):
    result = count(args)
    json.dump(result, args.outfile)


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Calculate average noise at arrival. '
        'Non-arrived sessions are skipped',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')

    metrics = parser.add_argument_group('metric options')
    metrics.add_argument('--rel', dest='absoluteness',
        action='store_const', const='rel',
            default='abs', help='get metric relative to the number of targets')

    parser.add_argument('enhanced_sessions',
            help='dir containing one json file per run', nargs='+')
    metrics.add_argument('--arrivalmoment', default='toparrival',
            choices=MOMENTSLIST,
            help='Which moment should be chosen')
    parser.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    main(args)
