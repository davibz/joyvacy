# Long description {{{3
# This is the data flow:
# doit will create virtualenvs and testall, all.0.gz, learn_pos, learn_neg from $(DATADIR)/*.csv.gz in $(DATADIR)/*/
#   [ adding filters ] {
#     - instead of reading all.gz, reads the greatest all.$n.gz
#       * the creation of the all.0.gz seems to be impossible to be moved out of where it is
#       * we can move the test.gz and learn_*.gz out of it, using the same tree created by make_categorized_samples
#       * binary_category (url.py+95) should be changed to read the "highest" learn gzipped files
#       * testall.%.gz depend on learn_pos.$*.gz learn_neg.$*.gz
#     - all.3.gz just needs to depend on all.2.gz (and maybe something else)
#     - make all.gz depend (and symlink) all.$mynumberofpasses.gz
#   }
# after that, doit will create classifiers: binary, multiclass, hierarchical.tar.bz2
#   the hierarchical is a tarball because I cannot get how to pickle a ready-to-use hierarchical
#   otherwise.
# }}}3
RESDIR=$(CL)/results
DISTDIR=$(CL)/dist

DATASETS:=$(wildcard $(DATADIR)/*.csv.gz)
DATASETS_LEVEL1:=$(wildcard $(DATADIR)/*.1.csv.gz)
DATASETS_LEVEL2:=$(wildcard $(DATADIR)/*.2.csv.gz)
CLASSIFIERS=$(DATASETS:.csv.gz=.binaryall)
CLASSIFIERS_LEVEL1=$(DATASETS_LEVEL1:.csv.gz=.binaryall)
CLASSIFIERS_LEVEL2=$(DATASETS_LEVEL2:.csv.gz=.binaryall)
RESULTS=$(addprefix $(RESDIR)/binaryall_,$(notdir $(DATASETS:.csv.gz=.test)))

.PHONY: all test allresults baseresults hierresults datadirs
# secondary means that no intermediate files will be removed
.SECONDARY:

all: bin allresults
bin: $(DISTDIR)/hierarchical 
datadirs: $(DATASETS:.csv.gz=)
allresults: baseresults hierresults
baseresults: $(RESULTS)
hierresults: $(RESDIR)/binaryall_hier.test
allclassifiers: $(CLASSIFIERS)
test: $(RESDIR)/binaryall_dmoz.test $(RESDIR)/binaryall_google.test


# Data {{{3
$(DATADIR)/content.rdf.u8.gz:
	wget -c "http://rdf.dmoz.org/rdf/content.rdf.u8.gz" -O $@
$(DATADIR)/dmoz.csv.gz: $(CL)/convert.py $(DATADIR)/content.rdf.u8.gz
	python2 $< $@
# }}}3

%.py:
	@touch $@
$(CL)/url.py: $(VENV) $(addprefix $(CL)/,skutils.py utils.py resume.py)
$(CL)/hierarchical.py: $(VENV) $(CL)/utils.py $(CL)/resume.py


$(RESDIR)/binaryall_%.test: $(DATADIR)/%.binaryall $(DATADIR)/%/testall $(CL)/url.py 
	./$(VENV)/bin/python $(CL)/hierarchical_test.py $< $(DATADIR)/$*/testall > $@

$(RESDIR)/binaryall_hier.test: $(CL)/hierarchical.py $(DATADIR)/classifiers.tar.bz2 $(DATADIR)/hier/testall
	./$(VENV)/bin/python $(word 1,$^) --ignore-nochild --mapping base64 --tarball $(word 2,$^) --root-name $(notdir $(CLASSIFIERS_LEVEL1)) --url-file $(word 3,$^) > $@

# note that using --strip will break!
$(DISTDIR)/%: $(CL)/%.py $(VENV) $(CL)/utils.py
	./$(VENV)/bin/pyinstaller --onefile --distpath=$(DISTDIR) --workpath=$(CL)/build -w -y $<

clean:
	rm -rf $(DISTDIR)/resume $(RESULTS) $(CLASSIFIERS)
	rm -rf $(DATASETS:.csv.gz=/)

databackup.tar.gz: $(DATASETS)
	tar czf $@ --dereference $^

# vim: set noet fdm=marker fdl=2:
