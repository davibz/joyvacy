PYTHONPATH := common/src
export PYTHONPATH
ADDONSDK=addon-sdk
CFX:=$(ADDONSDK)/bin/cfx
ADDON:=FireJoyvacy
EVAL_PROFILE:=$(shell mktemp --tmpdir -du firefox.eval.$(shell date +%s).XXXXXXX)


.PHONY: cfx
cfx: $(CFX)
xpi: $(ADDON)/build/mantra.xpi

jetpack-sdk-latest.tar.gz:
	wget -c "https://ftp.mozilla.org/pub/mozilla.org/labs/jetpack/jetpack-sdk-latest.tar.gz"
$(CFX): jetpack-sdk-latest.tar.gz
	mkdir -p $(ADDONSDK)
	tar xaf $< -C $(ADDONSDK) --strip-components=1
	touch $@

$(ADDON): $(ADDON)/data/corpus $(ADDON)/data/classifier $(ADDON)/data/classifiers.tar.bz2
	echo addon ready

$(ADDON)/data/classifier: $(DISTDIR)/hierarchical
	cp -f $< $@

$(ADDON)/data/classifiers.tar.bz2: data/classifiers/hierarchical.tar.bz2
	cp -f $< $@

$(ADDON)/build/mantra.xpi: $(CFX) $(ADDON)
	mkdir -p $(ADDON)/build
	$< --pkgdir=$(ADDON) xpi --output-file $@
fftest: $(CFX) $(ADDON)
	$< --pkgdir=$(ADDON) test -f .

ffrun: $(CFX) $(ADDON)
	$< --pkgdir=$(ADDON) run

ffrun_persistent: $(CFX) $(ADDON)
	$< --pkgdir=$(ADDON) -p firefox_profile run

$(EVAL_PROFILE): $(ADDON)/test.sh $(INTERESTS)/en-US.csv
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -s 1000 -n 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)

ffrun_eval: $(CFX) $(ADDON) $(EVAL_PROFILE)
	test -n "$(TMPDIR)"
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) evalres || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/0_0-3_1000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 3 -s 1000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)


closed_experiments/0_0-3_2000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 3 -s 2000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/1_100-1_1000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 100 -n 1 -s 1000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/2_200-1_1000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 200 -P 2 -n 1 -s 1000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/3_300-1_1000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 300 -P 3 -n 1 -s 1000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/0_0-2_2000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 2 -s 2000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/0_0-10_5000/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 10 -s 5000 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

closed_experiments/0_0-3_1000-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 3 -s 1000 -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

#long, attempting low noise
closed_experiments/0_0-1_3000-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 1 -s 3000 -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be quick!
closed_experiments/0_0-1_soon-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 1 -s 200 -e -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be quick!
closed_experiments/0_0-1_reach-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 1 -s 200 -r -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be quick!
closed_experiments/0_0-2_reach-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 2 -s 500 -r -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be quick!
closed_experiments/0_0-3_reach-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 3 -s 1000 -r -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be (almost) quick!
closed_experiments/0_0-5_reach-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 5 -s 1000 -r -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)

# this should be (almost) quick!
closed_experiments/0_0-7_reach-0click/data: $(CFX) $(ADDON)
	test -n "$(TMPDIR)"
	rm -rf $(EVAL_PROFILE)
	mkdir -p $(EVAL_PROFILE)
	touch $(EVAL_PROFILE)
	$(ADDON)/test.sh -p 0 -n 7 -s 1200 -r -c 1 $(INTERESTS)/en-US.csv $(EVAL_PROFILE)
	test -n "$(CFX_TIMEOUT)"
	timeout $(CFX_TIMEOUT) $< --pkgdir=$(ADDON) -p $(EVAL_PROFILE) run || true
	$(ADDON)/save_test.sh $(EVAL_PROFILE) $@ || echo "Error saving"
	rm -rf $(EVAL_PROFILE)
# vim: set ts=4 sw=4 noet:
