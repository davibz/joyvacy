from utils import randomly_tree_pick as orig_randomly_tree_pick
from nose.tools import eq_

chooser = lambda x: x[-1]


def randomly_tree_pick(*args, **kwargs):
    if 'chooser' not in kwargs:
        kwargs['chooser'] = chooser
    return orig_randomly_tree_pick(*args, **kwargs)


def test_flat():
    l = map(lambda x: (x,), range(5))
    eq_(randomly_tree_pick(l), (4,))


def test_2levels():
    eq_(randomly_tree_pick((
        ('1', 'a'),
        ('1', 'b'),
        ('2', 'c'),
        ('2', 'd'),
    )), ('2', 'd'))


def test_shorter():
    eq_(randomly_tree_pick((
        ('1', 'a'),
        ('1', 'b'),
        ('1', 'c'),
        ('1', 'c', 'A'),
        ('1', 'c', 'B'),
        ('2', 'c'),
        ('2', 'd'),
    )), ('2', 'd'))


def test_deeper():
    eq_(randomly_tree_pick((
        ('1', 'a'),
        ('1', 'b'),
        ('2', 'c'),
        ('2', 'd'),
        ('2', 'e', 'A'),
        ('2', 'e', 'B')
    )), ('2', 'e', 'B'))


def test_pick_longer():
    eq_(randomly_tree_pick((
        ('1',),
        ('1',),
        ('2', 'c'),
        ('2', 'd'),
        ('2', 'e', 'A'),
        ('2', 'e', 'B')
    )), ('2', 'e', 'B'))

