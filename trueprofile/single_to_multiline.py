'''
get_categories_site.py used to have a weird format. Let's convert it

It was oneline per category, with lot of columns
google query was not removed
'''

import sys
import gzip

def degooglize(site):
    from urllib2 import urlparse
    if 'url?q=' in site:
        site = urlparse.parse_qs(
                urlparse.urlparse(site).query)['q'].pop()

def converter(fname):
    with open(fname) as buf:
        for line in buf:
            sites = line.split('\t')
            cat = sites.pop(0)
            for site in sites:
                yield '%s\t%s\n' % (cat, degooglize(site))

with gzip.open(sys.argv[2], 'w') as buf:
    for line in converter(sys.argv[1]):
        buf.write(line)
