from functools import partial

from nose.tools import eq_

from get_categories_site import make_dest
make_dest = partial(make_dest, mkdir=False)


def test_one():
    eq_(make_dest('gdir', ['asd']), 'gdir/asd')


def test_two():
    eq_(make_dest('gdir', ['asd', 'foo']), 'gdir/asd_dir/foo')


def test_three():
    eq_(make_dest('gdir', ['asd', 'foo', 'baz']), 'gdir/asd_dir/foo_dir/baz')
