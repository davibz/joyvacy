from time import sleep
from collections import namedtuple
import cookielib
import os.path
from ConfigParser import SafeConfigParser

from splinter import Browser

ADS_URL = 'https://www.google.com/settings/ads'
DRIVER = 'firefox'
AGENT = 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1'

Cookie = namedtuple('Cookie', ['name', 'value'])
def read_cookies(buf):
    for line in buf:
        name, value = line.split('\t')[5:7]
        yield Cookie(name, value)

def get_ff_profile_dir():
    DEF = 'Default'
    PATH = 'Path'
    basedir = os.path.expanduser('~/.mozilla/firefox/')
    ini = os.path.join(basedir, 'profiles.ini')
    conf = SafeConfigParser()
    conf.readfp(open(ini))
    for sec in conf.sections():
        if conf.has_option(sec, DEF) and conf.getboolean(sec, DEF):
            return os.path.join(basedir, conf.get(sec, PATH))

browser_settings = {}
#browser_settings['user_agent'] = AGENT
browser_settings['profile'] = get_ff_profile_dir()
with Browser(DRIVER, **browser_settings) as br:
    br.visit('https://www.google.com/')
    br.visit(ADS_URL)
    rows = br.find_by_xpath("//div[@role='row']")
    rows.find_by_xpath("//div[@role='tab']")[-1].click()
    interests = rows.find_by_xpath("//tr[@tabindex='0']/td[1]")
    print '\n'.join(map(lambda e: e.text, interests))

def get_script():
    if os.path.exists('googlead.js'):
        return open('googlead.js').read()
    return get_script_from_url()
