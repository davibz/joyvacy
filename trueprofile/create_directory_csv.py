'''
given a directory tree, creates a directory csv.gz file

The first column (category) can be customized
'''
# This could have been a very simple script:
#    find . -type f -print0|xargs -0 cat|uniq|sort -u |gzip -c > $@
# But our goal is to customize the "label" (that is, the first column) so to be
# able to "aggregate" as much as we want

import os
from argparse import ArgumentParser
from copy import copy

from utils import memoize
from mantra.buffers import auto_buf
from mantra.corpus import extract_corpus_line, website_filter


def get_files(basedir):
    '''find $basedir -type f'''
    for dir_in, dirs, files in os.walk(basedir):
        for basename in files:
            yield os.path.join(dir_in, basename)


def change_label(buf, new_label):
    '''
    This will yield one line at a time. Each line is read from buf but
    filtered so that the first column now displays ${new_label}
    '''
    for line in buf:
        label, rest = line.split('\t', 1)
        yield '%s\t%s' % (new_label, rest)


def get_fname(fname, options):
    options = copy(options)
    options.skip = 0
    label = get_label(fname, options)
    return options.dest % \
        {
            'depth': options.depth,
            'split':
            '+'.join(label.split('+')[:options.split])
        }


def get_label(fname, options):
    fname = os.path.relpath(fname, options.basedir)
    path = fname.split(os.path.sep)
    base = path.pop()
    path = map(lambda x: x[:-4] if x.endswith('_dir') else x, path)
    path.append(base)

    if(len(path) < options.depth):
        return None
    joined = '+'.join(path[options.skip:options.depth])
    assert os.path.sep not in joined
    return joined


@memoize
def get_buf(fname, mode='r'):
    return auto_buf(fname, mode)


def main(options):
    for f in get_files(options.basedir):
        new_label = get_label(f, options)
        if new_label is None:
            continue
        with open(f) as src:
            for line in change_label(src, new_label):
                fname = get_fname(f, options)
                parsed = extract_corpus_line(line)
                if parsed is None or not website_filter(parsed[0]):
                    continue
                get_buf(fname, 'w').write(line)


# split means that the main generations is done grouping at that many parent
# categories
def get_parser():
    parser = ArgumentParser(description='Tries to manipulate the trackers')
    parser.add_argument('basedir', metavar='BASEDIR',
                        help='Something like google_directory/')
    parser.add_argument('--dest', metavar='DEST', default='-',
                        help='Output file. Gzip and Bz2 are automatically' +
                        ' supported. Escapes %%(depth)s and %%(split)s' +
                        ' are supported')
    parser.add_argument('--depth', metavar='DEPTH', default=1, type=int,
                        help='Aggregate at this depth')
    parser.add_argument('--skip', metavar='S', default=0, type=int,
                        help='Omit first S level from the label description')
    parser.add_argument('--split', metavar='SPLIT', default=0, type=int,
                        help='Split at this depth')
    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
