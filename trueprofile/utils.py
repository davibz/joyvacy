from __future__ import print_function
import sys
import select
import random
from logging import Handler, log, INFO, WARNING, ERROR
from subprocess import check_call, CalledProcessError
import functools
from gzip import GzipFile
from bz2 import BZ2File
from contextlib import contextmanager
from itertools import izip, chain
from collections import deque

MEAN_SLEEP_TIME = 6


def get_opener(fname):
    if fname.endswith('.gz'):
        return GzipFile
    if fname.endswith('.bz2'):
        return BZ2File
    return open


def auto_buf(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            return sys.stdout
        else:
            return sys.stdin
    else:
        opener = get_opener(fname)
        buf = opener(fname, mode)
        return buf


@contextmanager
def auto_open(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            yield sys.stdout
        else:
            yield sys.stdin
    else:
        opener = get_opener(fname)
        try:
            buf = opener(fname, mode)
            yield buf
        finally:
            buf.close()


def sleep_time(mean=MEAN_SLEEP_TIME):
    seconds = 1/float(mean)
    return random.expovariate(seconds)


def randomly(seq):
    shuffled = list(seq)
    random.shuffle(shuffled)
    return iter(shuffled)


def randomly_tree(seq):
    while True:
        yield randomly_tree_pick(seq)


def randomly_tree_pick(tree_seq, chooser=random.choice, level=0):
    '''
    tree_seq is a sequence of tuples. Each tuple represent a path from the
    root to a node.
    Therefore, tree_seq is actually a DAG.

    randomly_tree_pick will always return a leaf; that is, a node without any
    children

    We want to pick an element so that there is uniform probability for each
    _level_, and not for each node itself. This is especially important for
    unbalanced trees: if say, we have a node A with many children, and a node B
    with few childrens, there is high probability that picking randomly on
    nodes will get a son of A. We want to have same probability of getting a
    son of A and a son of B
    '''
    from collections import OrderedDict

    def level_filter(item, level):
        if len(item) <= level:
            return None
        return item[level]

    # current_level is a ordered list of possible values for this level
    # the ordering is important only for unit-testing reasons
    # current_level has _unique_ values (to prevent nodes with many children to
    # be favored in any way)
    current_level = list(
        OrderedDict.fromkeys(filter(
            lambda x: x is not None,
            map(lambda x: level_filter(x, level), tree_seq))))
    # if there is no node at such depth, we need to stop
    if not current_level:
        assert len(tree_seq) == 1
        return tree_seq[0]
    pick = chooser(current_level)
    resulting = filter(lambda x: len(x) >= level + 1 and x[level] == pick,
                       tree_seq)
    return randomly_tree_pick(resulting, chooser=chooser, level=level+1)


def input_timeout(timeout=60, text=None, lvl=WARNING):
    if text is None:
        text = 'Press any key in %d seconds' % timeout
    log(lvl, text)

    i, o, e = select.select([sys.stdin], [], [], timeout)
    if i:
        return sys.stdin.readline().strip()
    return None


class NotifyHandler(Handler):
    def __init__(self, level):
        Handler.__init__(self, level)

    def emit(self, record):
        urgency = "normal"
        if record.levelno >= ERROR:
            urgency = "critical"
        if record.levelno < INFO:
            urgency = "low"
        try:
            check_call(["notify-send", "-u", urgency, record.msg])
        except CalledProcessError:
            # a logger cannot _create_ problems
            pass
        except OSError:
            # typically, notify-send is not installed
            pass


def memoize(obj):
        cache = obj.cache = {}

        @functools.wraps(obj)
        def memoizer(*args, **kwargs):
            if args not in cache:
                cache[args] = obj(*args, **kwargs)
            return cache[args]
        return memoizer


def coroutine(func):
    def start(*args, **kwargs):
        g = func(*args, **kwargs)
        g.next()
        return g
    return start


def zip_pad(*iterables, **kw):
    if kw:
        assert len(kw) == 1
        pad = kw["pad"]
    else:
        pad = None
    done = [len(iterables) - 1]

    def pad_iter():
        if not done[0]:
            return
        done[0] -= 1
        while 1:
            yield pad
    iterables = [chain(seq, pad_iter()) for seq in iterables]
    return izip(*iterables)


def iter_with_previous(iterable, context=2):
    prev = deque([], maxlen=context)
    for elem in iterable:
        prev.append(elem)
        yield [None] * (context - len(prev)) + [x for x in prev]
