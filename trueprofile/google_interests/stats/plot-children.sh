#!/usr/bin/env bash

if [ $# -ne 2  ]; then
    echo "Usage: $0 SRC DST"
    exit 1
fi
src=$1
dst=$2
shift 2

define(){ IFS='\n' read -r -d '' ${1} || true;  }
define gnuplot_cmd <<EOPLOT
set terminal pngcairo size 350,262 enhanced font 'Verdana,10';
set output '$dst';
plot '-' s f;
EOPLOT

sort "$src"|uniq -c| awk '{ print $2 " " $1  }' | gnuplot -e "$gnuplot_cmd"
