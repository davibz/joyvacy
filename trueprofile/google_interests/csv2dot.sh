#!/usr/bin/env bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 CSV"
    exit 2
fi

echo 'digraph {'
tr -dc '[\n\t[:alpha:]]' < "$1" | sed 's/\t/ -> /g;s/$/;/;s/^/ROOT -> /'
echo '}'
    

