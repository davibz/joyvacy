#!/usr/bin/env bash

csv=$1
subtree() {
    # $1 is the starting level
    # $2 is the end level
    cut -s -f "${1}-${2}" $csv|uniq
}

descendents() {
    cut -f "1-$2" $csv|sort -u|cut -f $1|uniq -c|sort -n
}

echo -e "Top categories:\t$(subtree 1 1|wc -l)"

nlines=$(wc -l $csv | awk '{ print $1 }')
for i in {2..7}; do
    cats=$(subtree 1 $i|wc -l)
    echo -e "Level $i categories:\t$cats"
done

for i in {1..7}; do
    echo -e "Level $i categories:"
    echo -e "   number of children: "
    descendents $i $((i+1))| head -n1
    descendents $i $((i+1))| tail -n1
    echo -e "   number of children+nephews: "
    descendents $i $((i+2))| head -n1
    descendents $i $((i+2))| tail -n1
done
