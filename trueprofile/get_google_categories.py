import sys
from argparse import ArgumentParser

import firefox_better

from mantra import clean


def get_all_interests(browser, lang='en'):
    '''
    this yields all interests, not just yours

    each interest is actually a list, to express hierarchy
    '''
    URL = 'https://support.google.com/ads/answer/2842480?hl=%s' % lang
    PREFIX = 'category::'
    HIER_SEP = '>'

    # browser_settings['profile'] = get_ff_profile_dir()

    br.visit(URL)
    toplevel_interests = br.find_by_css(".zippy-container a.zippy")
    for top in toplevel_interests:
        top.click()
        for content in br.find_by_css('.content-container > div p'):
            for cat in content.text.split('\n'):
                if not cat.startswith(PREFIX):
                    continue
                cat = cat[len(PREFIX):]
                yield tuple(cat.split(HIER_SEP))


def get_parser():
    parser = ArgumentParser(description='Get all google interests')
    parser.add_argument('--outfile', metavar='CSVFILE', default=sys.stdout,
                        help='Where to save the results. Defaults to stdout')
    parser.add_argument('--lang', metavar='LANG', default='en-US',
                        help='Language (expressed in en-US form)')

    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    prefs = {
        'intl.accept_languages': args.lang,
        'general.useragent.locale': args.lang
    }
    with firefox_better.Browser(profile_preferences=prefs) as br:
        if args.outfile != sys.stdout:
            args.outfile = open(args.outfile, 'w')
        for cat in sorted(set(get_all_interests(br, args.lang[:2]))):
            if clean.cat_is_ok(cat):
                continue
            args.outfile.write('%s\n' % '\t'.join(cat).encode('utf-8'))
