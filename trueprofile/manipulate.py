'''
This script tries to change what a tracker thinks of us just visiting some
websites.
'''
from __future__ import print_function
import sys
import random
from collections import defaultdict
from gzip import open as GzipOpen
import json
from itertools import count
from datetime import datetime
from pprint import pprint
from traceback import print_exc
from argparse import ArgumentParser

from firefox_better import Browser

from utils import sleep_time

RUN_DEPTH = 2  # how many link will be clicked in a run


class Directory(object):
    '''represent a *_directory.csv.gz'''
    def __init__(self, filename):
        self.archive = defaultdict(list)  # category: [list, of, urls]
        self.read(filename)

    def read(self, filename):
        with GzipOpen(filename) as buf:
            for line in buf:
                parts = line.split('\t')
                cat = parts[0]
                url = parts[1]
                self.archive[cat].append(url)

    def pick(self, cat):
        '''Get a random site from category "cat"'''
        return random.choice(self.archive[cat])


class Interests(object):
    '''represet a *_directory.csv'''
    def __init__(self, filename):
        self.archive = []
        self.read(filename)

    def read(self, filename):
        with open(filename) as buf:
            for line in buf:
                parts = line.strip().split('\t')
                self.archive.append(parts)

    def pick(self, level=0):
        '''Get a random site from category "cat"'''
        if level == 0:  # pick from any level
            return random.choice(self.archive)
        return random.choice(filter(lambda i: len(i) == level, self.archive))


def get_real_interests(br):
    '''What does google think of us?'''
    ADS_URL = 'https://www.google.com/settings/ads?hl=en'
    br.visit('https://www.google.com/')
    br.visit(ADS_URL)
    rows = br.find_by_xpath("//div[@role='row']")
    rows.find_by_xpath("//div[@role='tab']")[-1].click()
    interests = rows.find_by_xpath("//tr[@tabindex='0']/td[1]")
    return map(lambda e: e.text, interests)


def visit_site(br, url, nclicks):
    '''Visit + click around'''
    if nclicks < 0:
        return
    print(datetime.now(), '-> visit', url, nclicks)
    br.visit(url)
    links = br.find_by_css('a')
    random.shuffle(links)
    sleep_time()
    for el in links:
        try:
            next_url = el['href']
            if not (next_url and next_url.startswith('http')):
                continue
        except Exception as exc:
            print_exc(exc)
            continue
        try:
            visit_site(br, next_url, nclicks-1)
        except:
            print("Error on '%s'" % next_url)
            continue
        break


def cheat(br, directory, category):
    '''
    pick a site, visit, clickclick, what does google think?
    And debug in the middle of this!
    '''
    try:
        start_interest = get_real_interests(br)
    except:
        start_interest = None
    while True:
        seed = directory.pick(category)
        try:
            visit_site(br, seed, RUN_DEPTH)
            break
        except Exception as exc:
            print_exc(exc)
            continue
    try:
        end_interest = get_real_interests(br)
    except:
        end_interest = []
    return {
        'cat': category,
        'site': seed,
        'nclick': RUN_DEPTH,
        'interest_before': start_interest,
        'interest_after': end_interest
    }


def run_series(target, matcher=None):
    '''Launch a series of run, untile the target appears in the profile'''
    if matcher is None:
        matcher = lambda ints: ints and target in ints
    history = []
    lang = 'en-US'
    prefs = {
        'intl.accept_languages': lang,
        'general.useragent.locale': lang
    }
    with Browser(profile_preferences=prefs) as br:
        for i in count(1):
            res = cheat(br, directory, target)
            res['nvisit'] = i
            print("Run %d[%s]: %s" % (i,
                                      target,
                                      ', '.join(res['interest_after'])))
            if matcher(res['interest_after']):
                res['history'] = history
                pprint(res)
                return res
            else:
                history.append(res)


def get_parser():
    parser = ArgumentParser(description='Tries to manipulate the trackers')
    parser.add_argument('directory', metavar='DIRECTORY',
                        help='Something like google.csv.gz')
    parser.add_argument('--target', metavar='CATEGORY', default='News',
                        help='What we want to let the tracker think of us')
    parser.add_argument('--translation', metavar='CATEGORY_TRANSLATION',
                        action='append', default=[],
                        help='Provide localizations for category name. Can be '
                        + 'used more than once')
    parser.add_argument('--logfile', metavar='LOG', default=sys.stdout,
                        help='File that will be appended with summary')
    parser.add_argument('--target-list', metavar='TARGETLIST', default=None,
                        help='A file containing a target (one per line).' +
                        ' At each step a random one will be picked')
    parser.add_argument('--random-target', action='store_true', default=False,
                        help='Choose random targets from the ones available')
    parser.add_argument('--target-level', metavar='TARGETLEVEL', default=0,
                        help='Choose only targets of this level.' +
                        ' The default is 0, which means "any level"')

    return parser

if __name__ == '__main__':
    # We have two main inputs: the directory and the target category
    parser = get_parser()
    args = parser.parse_args()
    directory = Directory(args.directory)
    if args.target_list is None:
        if args.random_target:
            target = lambda: random.choice(directory.archive.keys())
        else:
            target = lambda: args.target
    else:
        targetlist = Interests(args.target_list)
        target = lambda: targetlist.pick(args.target_level)[-1]

    def tr_match(accepted):
        accepted = set(accepted)

        def match(interests):
            if not interests:
                return False
            print('MATCH', accepted, interests)
            for word in accepted:
                if word in interests:
                    return True
            return False
        return match
    while True:
        cat = target()
        res = run_series(cat, tr_match([cat] + args.translation))
        with open(args.logfile, 'a') as out:
            out.write(json.dumps(res))
            out.write('\n')
