import sys

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from splinter.driver.webdriver import BaseWebDriver
from splinter.driver.webdriver.cookie_manager import CookieManager
from splinter.driver.webdriver.firefox import WebDriverElement


class WebDriver(BaseWebDriver):
    driver_name = "Firefox"

    def __init__(self, profile=None, extensions=None, user_agent=None,
                 profile_preferences=None, wait_time=2, driver_args={}):
        firefox_profile = FirefoxProfile(profile)
        firefox_profile.set_preference('extensions.logging.enabled', False)
        firefox_profile.set_preference('network.dns.disableIPv6', False)

        if user_agent is not None:
            firefox_profile.set_preference('general.useragent.override',
                                           user_agent)

        if profile_preferences:
            for key, value in profile_preferences.items():
                firefox_profile.set_preference(key, value)

        if extensions:
            for extension in extensions:
                firefox_profile.add_extension(extension)

        self.driver = Firefox(firefox_profile, **driver_args)

        self.element_class = WebDriverElement

        self._cookie_manager = CookieManager(self.driver)

        super(WebDriver, self).__init__(wait_time)


def Browser(**custom_settings):
    WM_NAME = 'annoying_%s' % sys.argv[0]
    browser_binary = FirefoxBinary()
    # Problem: we need to respawn each time a new browser,
    # but this is annoying. Setting a WM_CLASS allows your WM to be configured
    browser_binary.add_command_line_options('--class', WM_NAME)
    browser_settings = {
        'driver_args': {
            'firefox_binary': browser_binary
        }
    }
    browser_settings.update(**custom_settings)
    return WebDriver(**browser_settings)


class BrowserFactory(object):
    '''
    High level class to get consistent browser, set options etc.

    Using an instance of this as global will make it very simple to create
    consistent instances of browser.
    '''
    def __init__(self, socks=None, lang='en-US'):
        self.socks = socks
        self.lang = lang
        self.cache = None

    def get_new_browser(self):
        browser_settings = {}
        browser_settings['user_agent'] = 'Mozilla/5.0 ' \
            + '(X11; U; Linux i686; en-US; rv:1.9.0.1) ' \
            + 'Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1'
        prefs = {}
        prefs['browser.startup.homepage_override.mstone'] = 'ignore'
        prefs['intl.accept_languages'] = self.lang
        prefs['general.useragent.locale'] = self.lang
        if self.socks is not None:
            host, port = self.socks.split(':')
            # https://groups.google.com/d/msg/selenium-users/UNtNfB6_CIc/NKDjlspphPEJ
            prefs['network.proxy.socks'] = host
            prefs['network.proxy.socks_port'] = int(port)
            prefs['network.proxy.type'] = 1

        browser_settings['profile_preferences'] = prefs
        # browser_settings['profile'] = get_ff_profile_dir()
        b = Browser(**browser_settings)
        return b

    def get_cached_browser(self):
        if not self.cache:
            self.cache = self.get_new_browser()
        return self.cache

