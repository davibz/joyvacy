'''
Given a category, what sites belong to it?
We'll try to discover just searching on google.

This program creates a directory tree resembling the interests tree; files
contain list of urls + title that match with the category
'''

from __future__ import print_function
import os
import time
from argparse import ArgumentParser
import logging

from firefox_better import BrowserFactory

from utils import sleep_time, randomly_tree, input_timeout, NotifyHandler
OUTPREFIX = os.getenv('OUTPREFIX', '')
browser_factory = BrowserFactory()


def get_browser():
    return browser_factory.get_cached_browser()


logging.basicConfig(level=logging.INFO)
root_logger = logging.getLogger()
root_logger.addHandler(NotifyHandler(logging.WARNING))


def degooglize(site):
    from urllib2 import urlparse
    if 'url?q=' in site:
        site = urlparse.parse_qs(
            urlparse.urlparse(site).query)['q'].pop()
    return site


def extracts_urls(browser, query=None):
    def get_urls(br):
        links = browser.find_by_css('h3 a')
        while not links:
            input_timeout(timeout=180, text='CAPTCHA! (results not found)')
            links = browser.find_by_css('h3 a')
        for link in links:
            yield degooglize(link['href']), link.text
    if query is not None:
        browser.visit('https://www.google.com')
        vis = browser.url
        browser.fill('q', '%s\n' % query)
        if browser.url == vis:
            browser.find_by_css('button[@name="btnK"]').first.click()
    else:
        navlinks = browser.find_by_css('#nav a')
        while not navlinks:
            input_timeout(timeout=180, text='CAPTCHA! (navlinks not found)')
            navlinks = browser.find_by_css('#nav a')
        navlinks.last.click()
    return set(get_urls(browser))


def get_sites(category, howmany=50):
    results = set()

    browser = get_browser()
    extracts_urls(browser, category)
    page = 0
    while len(results) < howmany:
        time.sleep(sleep_time())
        results.update(extracts_urls(browser))
        page += 1
        if(page) > 20:
            break
    return results


def get_categories(buf, delim='\t', level=1):
    '''
    given a file-like, extract categories
    '''
    categories = set()
    for line in buf:
        parts = line.strip().split(delim)
        categories.add(tuple(parts[0:level]))
    return categories


def write_urls(cat, results, outbuf):
    for site, title in results:
        line = '\t'.join((cat, site, title))
        outbuf.write('%s\n' % line.encode('utf-8'))


def make_dest(prefix, cat, mkdir=True):
    fname = cat[-1]
    dirs = [prefix] + ['%s_dir' % d for d in cat[:-1]]
    path = os.path.join(*dirs)
    if mkdir:
        mkdir_p(path)
    return os.path.join(path, fname)


def mkdir_p(path):
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def get_parser():
    parser = ArgumentParser(description='Creates a directory of google ' +
                            'results for google interests')
    parser.add_argument('interests', metavar='INTERESTS',
                        help='Something like google_interests.csv.gz')
    parser.add_argument('outprefix', metavar='OUTPREFIX',
                        help='Directory where to put the directory in')
    parser.add_argument('--level', metavar='LEVEL', default=2, type=int,
                        help='The nesting level')
    parser.add_argument('--sites-per-keyword', metavar='NSITES', default=30,
                        type=int,
                        help='How many sites to collect for each keyword')
    parser.add_argument('--proxy-socks', metavar='SOCKS', default=None,
                        help='Set a socks proxy')

    return parser

if __name__ == '__main__':
    args = get_parser().parse_args()
    if args.proxy_socks is not None:
        browser_factory.socks = args.proxy_socks
        print('PROXY', browser_factory.socks)
    with open(args.interests) as input:
        categories = get_categories(input, level=args.level)
    results = {}
    outprefix = args.outprefix

    for cat in randomly_tree(categories):  # remeber: cat is a sequence
        print('CAT', cat)
        sites = get_sites(' '.join(cat), howmany=args.sites_per_keyword)
        dest = make_dest(outprefix, cat)
        with open(dest, 'a') as output:
            print('adding lines to %s' % dest)
            write_urls(cat[0], sites, output)
        print('ENDCAT', cat)
        time.sleep(sleep_time())
