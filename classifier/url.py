'''
URL classifications
'''

import os
import logging
from logging import getLogger
logging.basicConfig(format='[%(asctime)s|%(name)s|%(levelname)s] %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)
getLogger('DataFetcher').setLevel(logging.DEBUG)
getLogger('test_suite.evaluate').setLevel(logging.DEBUG)
getLogger('test_suite').setLevel(logging.DEBUG)
getLogger('expand_url').setLevel(logging.INFO)
from itertools import imap
from collections import Counter

from sklearn.multiclass import OneVsRestClassifier
from sklearn.externals import joblib

from resume import load_predicter, save_predicter
from skutils import EstimatorSVC, estimator_factory_fast
from utils import Aggregator, BASEDIR, common_evaluations, binarize, \
        DataFetcher

assert os.path.exists(BASEDIR) and os.path.isdir(BASEDIR), \
        "%s does not exist" % BASEDIR
binarydir = os.path.join(BASEDIR, os.getenv('DATA_PREFIX', 'ctg'))

# Draft
# To train it, we need to download dmoz and do as in the paper:
# Purely URL-based topic classification [ACM2009]

# All supervised estimators in the scikit-learn implement a fit(X, y) method
# to fit the model and a predict(X) method that, given unlabeled observations
# X, returns the predicted labels y.


# We are in the multiclass case, but not in the multilabel one.

class ToDenseTransformer(object):
    def fit(self, X, y):
        return self

    def transform(self, y):
        return tuple(matrix.toarray() for matrix in y)


def main_simpletrain(args):
    '''train + test + evaluate'''
    assert len(args) == 1
    samplesdir = args[0]
    samples = map(lambda f: os.path.join(samplesdir, f),
                  filter(lambda f: f.endswith('.csv.gz'),
                         os.listdir(samplesdir)))
    samples.sort()

    reader = DataFetcher()
    estimator = EstimatorSVC.pipe
    suite = TestSuite(reader, estimator)
    suite.train([samples.pop(0)])
    common_evaluations(*suite.test(samples))


class TestSuite(object):
    def __init__(self, reader, estimator, logname='test_suite'):
        self.log = getLogger(logname)
        self.reader = reader
        self.estimator = estimator

    def train(self, fnames):
        questions, answers = self.reader.read(fnames)
        self.log.debug('There are %d elements in learning set' % len(answers))
        self.log.debug("Most common answers: %s" %
                       str(Counter(answers).most_common(5)))
        self.estimator.fit(questions, answers)
        self.log.info('fit complete')

    def test(self, fnames):
        questions, truth = self.reader.read(fnames)
        self.log.debug('There are %d elements in test set' % len(questions))
        # prediction = tuple(evaluate(questions))
        prediction = self.estimator.predict(questions)
        self.log.debug("Most common answers: %s" %
                       str(Counter(truth).most_common(5)))
        self.log.debug("Most common predicted answers: %s" %
                       str(Counter(prediction).most_common(5)))
        return truth, prediction


def binary_category(catdir):
    category = os.path.basename(catdir)
    assert os.path.exists(catdir) and os.path.isdir(catdir)
    train_files = map(lambda f: os.path.join(catdir, f),
                      ('learn_pos', 'learn_neg'))

    # reader/estimators/etc
    reader = DataFetcher(preprocessor=lambda a: tuple(binarize(category, a)))
    estimator = estimator_factory_fast(category)
    suite = TestSuite(reader, estimator)
    suite.train(train_files)
    return suite


def main_binarytrain(args):
    '''a binary classifier for a single class'''
    # arguments and filenames
    assert len(args) == 1
    category = args.pop()
    catdir = os.path.join(binarydir, category)
    assert os.path.exists(catdir) and os.path.isdir(catdir)
    test_file = os.path.join(binarydir, 'testall')
    assert os.path.exists(test_file), test_file
    suite = binary_category(catdir)

    # reader/estimators/etc
    truth, prediction = suite.test([test_file])
    print('Binary %s' % category)
    print(common_evaluations.header)
    common_evaluations(truth, prediction)


class BinaryAll(object):
    def __init__(self, basedir):
        self.basedir = basedir

        assert os.path.exists(basedir) and os.path.isdir(basedir), basedir
        assert os.path.exists(self.test_file), self.test_file

    @property
    def test_file(self):
        return os.path.join(self.basedir, 'testall')

    def _train(self, out=None):
        classifiers = {}
        for category in sorted(os.listdir(self.basedir)):
            catdir = os.path.join(self.basedir, category)
            if not os.path.isdir(catdir):
                continue
            classifiers[category] = binary_category(catdir)
        # todo: aggregator
        multiclass_estimator = Aggregator(map(lambda suite: suite.estimator,
                                              classifiers.values()))
        return multiclass_estimator

    def _get_classifier_fname(self, out):
        if out is None:
            out = os.path.join(self.basedir, 'binaryall.pickle')
        return out

    def create(self, classifier_fname=None):
        classifier_fname = self._get_classifier_fname(classifier_fname)
        estimator = self._train()
        save_predicter(estimator, classifier_fname)

    def evaluate(self, classifier_fname=None):
        classifier_fname = self._get_classifier_fname(classifier_fname)
        predicter = load_predicter(classifier_fname)

        reader = DataFetcher()
        questions, truth = reader.read([self.test_file])
        print('Useless TRUTH stats:', Counter(truth).most_common(3))
        prediction = tuple(imap(lambda x: x.name,
                                predicter(questions)))
        print('Useless PREDI stats:', Counter(prediction).most_common(8))
        common_evaluations(truth, prediction)
        for category in set(truth):
            def bin(iterable):
                return tuple(imap(lambda x: x[1],
                                  binarize(category,
                                           ((0, x) for x in iterable)
                                           )))
            common_evaluations(bin(truth), bin(prediction), '[%s]' % category)
            print('### Useless TRUTH stats:',
                  Counter(bin(truth)).most_common(3))


def main_binaryall_test(args):
    b = BinaryAll(binarydir)
    pickle_file = args.pop()
    b.evaluate(pickle_file)


def main_binaryall_learn(args):
    # args = ['something.pickle'] or []
    # arguments and filenames
    b = BinaryAll(binarydir)
    b.create(args.pop())


def main_binaryallone(args):
    # arguments and filenames
    pickle_file = None
    if len(args) > 0:
        pickle_file = args.pop()
    assert len(args) == 0
    assert os.path.exists(binarydir) and os.path.isdir(binarydir)
    test_file = os.path.join(binarydir, 'testall')
    assert os.path.exists(test_file), test_file

    def train():
        train_file = os.path.join(BASEDIR, 'short.600k.csv.gz')
        assert os.path.exists(train_file), train_file

        suite = TestSuite(DataFetcher(),
                          OneVsRestClassifier(estimator_factory_fast(
                              name='single_est')))
        suite.train([train_file])
        return suite

    if pickle_file is None:
        pickle_file = os.path.join(binarydir, 'binaryallone.pickle')
        suite = train()
        joblib.dump(suite, pickle_file, compress=3)
        return
    else:
        suite = joblib.load(pickle_file)

    truth, prediction = suite.test([test_file])

    print 'Useless TRUTH stats:', Counter(truth).most_common(3)
    print 'Useless PREDI stats:', Counter(prediction).most_common(8)
    common_evaluations(truth, prediction)
    for category in set(truth):
        def bin(iterable):
            return tuple(imap(lambda x: x[1],
                              binarize(category,
                                       ((0, x) for x in iterable)
                                       )))
        print '### With respect to %s' % category
        common_evaluations(bin(truth), bin(prediction))

if __name__ == '__main__':
    import sys
    sub = dict((
        (k, v)
        for k, v in globals().items()
        if k.startswith('main_')))
    subcmd = 'main_%s' % sys.argv[1]
    if subcmd not in sub:
        print map(lambda c: c[5:], sub.keys())
        sys.exit(1)
    subcmd = sub[subcmd]
    subcmd(sys.argv[2:])

# vim: set ts=4 sw=4 et:
