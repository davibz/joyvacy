#!/usr/bin/env bash

LEARNSIZE=${LS:-1000}
TESTSIZE=${TS:-1000}

if [ $# -ne 2 ]; then
  exit 2
fi
data=$1
outpref=$2
shift 2
if [ ! -f "$data" ]; then
  exit 2
fi
if [ ! -d "$(dirname "$outpref")" ]; then
  echo "dir non existent"
  exit 2
fi

for i in {1..3}; do
  if [ $i -eq 1 ]; then
    size=$LEARNSIZE
  else
    size=$TESTSIZE
  fi
  zcat "$data" | shuf -n "$size" | gzip -c > "$outpref.$i.csv.gz"
done

