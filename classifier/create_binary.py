'''
This will make a binary classifier for a single category.

You should save it as .binary
'''
from __future__ import print_function

from url import binary_category
import resume


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Create a binary classifier')
    parser.add_argument('directory', metavar='BASEDIR', type=str, nargs=1,
                        help='must contain learn_pos and learn_neg files')
    parser.add_argument('--probabilistic', action='store_true', default=False)
    parser.add_argument('--classifier-out', metavar='FILENAME',
            default=None, help='Write (pickle) classifier to FILENAME')
    return parser

if __name__ == '__main__':
    args = get_parser().parse_args()
    out = binary_category(args.directory[0]).estimator
    print(out.name)

    if args.classifier_out:
        if args.probabilistic:
            resume.save_prob_predicter(out, args.classifier_out[0])
        else:
            resume.save_predicter(out, args.classifier_out)

# vim: set ts=4 sw=4 et:
