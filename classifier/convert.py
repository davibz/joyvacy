'''takes dmoz archive and creates a shorter version of it'''
import sys
import os
from utils import get_opener, BASEDIR
from gzip import GzipFile

from dmoz import DmozParser


class MyHandler(object):
    '''Handler that output a CSV from dmoz xml file'''
    def __init__(self, outbuf):
        self.out = outbuf

    def page(self, page, content):
        '''called for every "entry", outputs a line'''
        if page != None and page != "":
            topic = '/'.join(content['topic'].split('/')[1:2]).strip()
            title = content['d:Title']
            if topic and topic not in ('Regional', 'World'): # the paper said so
                self.out.write('%s\t%s\t%s\n' %
                        (topic.encode('ascii', 'ignore'),
                            page.encode('utf-8'),
                            title.encode('utf-8')
                            ))

    def finish(self):
        return


def read_dmoz(fname, out):
    '''
    read content.rdf.gz and create a much smaller version: tab-delimited csv,
    with just top category and url
    '''
    opener = get_opener(fname)
    buf = opener(fname, 'r')
    parser = DmozParser(buf)
    parser.add_handler(MyHandler(out))
    parser.run()
    buf.close()


def main_convert(args):
    if args:
        assert len(args) == 1
        out = GzipFile(args[0], 'w')
    else:
        out = sys.stdout
    content = os.path.join(BASEDIR, 'content.rdf.u8.gz')
    read_dmoz(content, out)

if __name__ == '__main__':
    main_convert(sys.argv[1:])
