'''
This will make a multiclass classifier, given many probabilistic classifiers

You should save it as .multiclass
'''
from __future__ import print_function

from utils import Aggregator
import resume


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Create a multiclass classifier given many binary ones')
    parser.add_argument('binary', metavar='BINARY_CLASSIFIER', type=str,
            nargs='+',
            help='binary classifiers, as created by create_binary.py')
    parser.add_argument('--classifier-out', metavar='FILENAME',
            default=None, help='Write (pickle) classifier to FILENAME')
    return parser

if __name__ == '__main__':
    args = get_parser().parse_args()

    estimators = [resume.load_raw(fname) for fname in args.binary]
    multiclass = Aggregator(estimators)

    if args.classifier_out:
        resume.save_predicter(multiclass, args.classifier_out)


# vim: set ts=4 sw=4 et:
