#!/usr/bin/env python2.7
'''
This has the purpose of getting the confidence we have on a certain url, and
filtering it out if it is not enough
'''

from __future__ import print_function
import os
import logging
log = logging.getLogger('filter_confidence')
log.setLevel(logging.INFO)
from itertools import chain

from numpy import isnan

from utils import Website, read_csv, YES_LABEL, auto_open
from url import binary_category
import resume


def urls_from_files(filenames):
# TODO: move to utils AND read title, too
    if not filenames:
        return
    for fname in filenames:
        for website, topic in read_csv(fname):
            yield website


def main(args):
    classifiers = list(make_classifier(args))
    log.info(','.join((c.name for c in classifiers)))

    counter_ok = 0
    counter_fail = 0
    with auto_open(args.ok_out[0], 'w') as buf_ok:
        with auto_open(args.fail_out[0], 'w') as buf_fail:
            for url in chain(
                    (Website(u, '') for u in args.urls),
                    urls_from_files(args.url_file)):
                res = sorted(
                        ((cl.name, cl.predict_proba([url])[0][YES_LABEL])
                            for cl in classifiers),
                        reverse=True,
                        key=lambda x: x[1]
                        )[:2]
# diff is "how much we're confident about classification"
                if isnan(res[0][1]):
                    log.debug('cant classify entry')
                    continue
                if isnan(res[1][1]):
                    log.debug("some classifier could not classify")
                    res[1][1] = 0
                diff = res[0][1] - res[1][1]
                print('%s = %.2f [%s - %s]' %
                        (url, diff, res[0][0], res[1][0]))
                ok = abs(diff) > args.minimum_confidence
                if ok:
                    counter_ok += 1
                    buf_ok.write(('%s\n' % url.to_csv()).encode('utf-8'))
                else:
                    counter_fail += 1
                    url.extra = '%s[%.2f]-%s[%.2f]' % \
                            (res[0][0], res[0][1], res[1][0], res[1][1])
                    #url.extra = '-'.join(imap(lambda x: x[0], res))
                    buf_fail.write(('%s\n' % url.to_csv()).encode('utf-8'))


def make_classifier(args):
    print(args.directory, args.classifier_in)
    if args.directory:
        for d in args.directory:
            while d.endswith('/'):
                d = d[:-1]
            if not os.path.exists(d):
                log.warn('non existent classifier directory %s' % d)
            yield binary_category(d).estimator
    if args.classifier_in:
        for cl in args.classifier_in:
            yield resume.load_prob_predicter(cl)


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
            description='Filter out confidence')
    parser.add_argument('--directory', metavar='CLASSIFIER', type=str,
            nargs='*', help='must contain learn_pos.gz learn_neg.gz')
    parser.add_argument('urls', metavar='URL', nargs='*',
            help='the url to classify')
    parser.add_argument('--ok-out', metavar='FILENAME', nargs=1, default='-',
            help='Write ok lines (self-confident) to FILENAME')
    parser.add_argument('--fail-out', metavar='FILENAME', nargs=1, default='-',
            help='Write fail lines (non-self-confident) to FILENAME')
    parser.add_argument('--url-file', metavar='URLFILE', nargs=1, default=None,
            help='Read urls to classify from file')
    parser.add_argument('--classifier-in', metavar='FILENAME', nargs='*',
            default=None, help='Get a classifier from pickle')
    parser.add_argument('--minimum-confidence', metavar='CONFIDENCE', nargs=1,
            default=0.1, help='Remove urls with confidence less than this')
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    main(args)

# vim: set ts=4 sw=4 et:
