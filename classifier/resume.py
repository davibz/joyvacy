from __future__ import print_function
from itertools import imap, izip
# import dill as pickle
import sys
from collections import Counter
import tarfile
import tempfile
from logging import getLogger, DEBUG, basicConfig
log = getLogger('resume')
import os
import shutil
import json

from utils import Aggregator, ngrams, expand_url, get_opener, Website, \
ConcatPipeline, DataFetcher, common_evaluations, binarize, YES_LABEL, \
NO_LABEL, INV_LABELS
from sklearn.externals import joblib


def save_predicter(predicter, fname, additional_imports=[]):
    assert hasattr(predicter, 'predict'), "type %s has no attribute predict" % \
            type(predicter)
    return joblib.dump(predicter, fname, compress=3)


def load_raw(fname):
    return joblib.load(fname)


def load_predicter(fname):
    return load_raw(fname).predict


def save_prob_predicter(predicter, fname, additional_imports=[]):
    assert hasattr(predicter, 'predict_proba'), "type %s has no attribute predict" % \
            type(predicter)
    return joblib.dump(predicter, fname, compress=3)


def load_prob_predicter(fname):
    loaded = load_raw(fname)
    fields = [f for f in dir(loaded) if not f.startswith('_')]
    return loaded.predict_proba


def load_predicter_tarball(fname, loader=load_predicter, member_filter=None):
    '''yields a (name, predicter) couple for each predicter in the tarball'''
    tar = tarfile.open(fname, 'r:*')
    basedir = tempfile.mkdtemp(prefix='tar_resume_')
    try:
        for member in tar.getmembers():
            if member_filter and not member_filter(member):
                continue
            tar.extract(member, basedir)
            extracted_fname = os.path.join(basedir, member.name)
            predicter = loader(extracted_fname)
            os.remove(extracted_fname)
            yield (member.name, predicter)
    except Exception as exc:
        log.exception(exc)
    finally:
        shutil.rmtree(basedir)


def get_prediction_name(prediction):
    l = getLogger('get_prediction_name')
    l.info('getting name of %s' % prediction)
    if hasattr(prediction, 'name'):
        return prediction.name
# is an array of probabilities
    try:
        INV_LABELS.keys().index(prediction)
    except ValueError:
        return 'YES' if prediction[YES_LABEL] > prediction[NO_LABEL] else 'NO'
    else:
        return INV_LABELS[prediction]


def resume_prob_predict(pickle_file, questions):
    predict = load_prob_predicter(pickle_file)
    prediction_raw = predict(questions)
    prediction_raw = tuple(prediction_raw)
    for url, pred_raw in izip(questions, prediction_raw):
        topic = get_prediction_name(pred_raw)
        log.debug(url, pred_raw)
        all_prob = {}
        for i, prob in enumerate(pred_raw):
            all_prob[INV_LABELS[i]] = prob
        yield url, topic, all_prob


def get_topic(val):
    if hasattr(val, 'name'):
        return val.name
    else:
        return INV_LABELS[val]


def resume_predict(pickle_file, questions):
    predict = load_predicter(pickle_file)
    prediction_raw = tuple(predict(questions))
    prediction = map(get_topic, prediction_raw)
    for url, topic in izip(questions, prediction):
        log.info('%s: %s' % (url, prediction))
        yield url, topic


def main_predict(fname, urls, buf=None, probabilistic=False):
    if buf is None:
        buf = sys.stdout
    if probabilistic:
        for url, topic, estimates in resume_prob_predict(fname, urls):
            buf.write('%s\t%s\t%s\n' % (url, topic, json.dumps(estimates)))
    else:
        for url, topic in resume_predict(fname, urls):
            buf.write('%s\t%s\n' % (url, topic))


def results(pickle_file, test_file):
    '''
    Get a pickle file and a file containing a list of urls.

    Does return nothing, only print
    '''
    questions, truth = DataFetcher().read([test_file])
    predicter = load_predicter(pickle_file)
    prediction = tuple(imap(lambda x: x.name,
                            predicter(questions)))
    print('Useless PREDI stats:', Counter(prediction).most_common(8))
    common_evaluations(truth, prediction)
    for category in set(truth):
        def bin(iterable):
            return tuple(imap(lambda x: x[1],
                              binarize(category,
                                       ((0, x) for x in iterable)
                                       )))
        common_evaluations(bin(truth), bin(prediction), '[%s]' % category)
        print('### Useless TRUTH stats:',
              Counter(bin(truth)).most_common(3))



# PyInstaller Hack {{{3
def import_all():
    '''Hack so that pyinstaller discover everything'''
    # read tracing execution
    import zipimport
    import site
    import os
    import errno
    import posix
    import posixpath
    import stat
    import genericpath
    import warnings
    import linecache
    import types
    import UserDict
    import _abcoll
    import abc
    import _weakrefset
    import _weakref
    import copy_reg
    import encodings
    import encodings
    import codecs
    import _codecs
    import encodings.aliases
    import encodings.utf_8
    import itertools
    import cStringIO
    import cPickle
    import sklearn
    import sklearn
    import sklearn.__check_build
    import sklearn.__check_build
    import sklearn.__check_build._check_build
    import sklearn.base
    import copy
    import weakref
    import inspect
    import string
    import re
    import sre_compile
    import _sre
    import sre_parse
    import sre_constants
    import strop
    import dis
    import opcode
    import imp
    import tokenize
    import token
    import operator
    import collections
    import _collections
    import keyword
    import heapq
    import _heapq
    import thread
    import numpy
    import numpy
    import __future__
    import numpy.__config__
    import numpy.version
    import numpy._import_tools
    import numpy.add_newdocs
    import numpy.lib
    import numpy.lib
    import math
    import numpy.lib.info
    import numpy.lib.type_check
    import numpy.core
    import numpy.core
    import numpy.core.info
    import datetime
    import numpy.core.multiarray
    import numpy.core.umath
    import numpy.core._internal
    import numpy.compat
    import numpy.compat
    import numpy.compat._inspect
    import numpy.compat.py3k
    import numpy.core.numerictypes
    import numpy.core.numeric
    import numpy.core.arrayprint
    import functools
    import _functools
    import numpy.core.fromnumeric
    import numpy.core._methods
    import numpy.core.defchararray
    import numpy.core.records
    import numpy.core.memmap
    import numpy.core.scalarmath
    import numpy.core.function_base
    import numpy.core.machar
    import numpy.core.getlimits
    import numpy.core.shape_base
    import numpy.testing
    import numpy.testing
    import unittest
    import unittest
    import unittest.result
    import traceback
    import StringIO
    import unittest.util
    import unittest.case
    import difflib
    import pprint
    import unittest.suite
    import unittest.loader
    import fnmatch
    import unittest.main
    import unittest.runner
    import time
    import unittest.signals
    import numpy.testing.decorators
    import numpy.testing.utils
    import numpy.testing.nosetester
    import numpy.testing.numpytest
    import numpy.lib.ufunclike
    import numpy.lib.index_tricks
    import numpy.lib.function_base
    import numpy.lib.twodim_base
    import numpy.lib._compiled_base
    import numpy.lib.utils
    import numpy.matrixlib
    import numpy.matrixlib
    import numpy.matrixlib.defmatrix
    import numpy.lib.stride_tricks
    import numpy.lib.nanfunctions
    import numpy.lib.shape_base
    import numpy.lib.scimath
    import numpy.lib.polynomial
    import numpy.linalg
    import numpy.linalg
    import numpy.linalg.info
    import numpy.linalg.linalg
    import numpy.linalg.lapack_lite
    import numpy.linalg._umath_linalg
    import numpy.lib.arraysetops
    import numpy.lib.npyio
    import numpy.lib.format
    import io
    import _io
    import numpy.lib._datasource
    import shutil
    import pwd
    import grp
    import numpy.lib._iotools
    import future_builtins
    import numpy.lib.financial
    import numpy.lib.arrayterator
    import numpy.lib.arraypad
    import numpy.fft
    import numpy.fft
    import numpy.fft.info
    import numpy.fft.fftpack
    import numpy.fft.fftpack_lite
    import numpy.fft.helper
    import numpy.polynomial
    import numpy.polynomial
    import numpy.polynomial.polynomial
    import numpy.polynomial.polyutils
    import numpy.polynomial.polytemplate
    import numpy.polynomial.chebyshev
    import numpy.polynomial.legendre
    import numpy.polynomial.hermite
    import numpy.polynomial.hermite_e
    import numpy.polynomial.laguerre
    import numpy.random
    import numpy.random
    import numpy.random.info
    import numpy.random.mtrand
    import numpy.ctypeslib
    import ctypes
    import ctypes
    import _ctypes
    import struct
    import _struct
    import ctypes._endian
    import numpy.ma
    import numpy.ma
    import numpy.ma.core
    import numpy.ma.extras
    import scipy
    import scipy
    import scipy.__config__
    import scipy.version
    import scipy.lib
    import scipy.lib
    import scipy.lib._version
    import scipy.lib.six
    import scipy.sparse
    import scipy.sparse
    import scipy.sparse.base
    import scipy.sparse.sputils
    import scipy.sparse.csr
    import scipy.sparse._sparsetools
    import scipy.sparse.compressed
    import scipy.sparse.data
    import scipy.sparse.lil
    import bisect
    import _bisect
    import scipy.sparse._csparsetools
    import scipy.sparse.dia
    import scipy.sparse.csc
    import scipy.sparse.dok
    import scipy.sparse.coo
    import scipy.sparse.bsr
    import scipy.sparse.construct
    import scipy.sparse.extract
    import scipy.sparse.csgraph
    import scipy.sparse.csgraph
    import scipy.sparse.csgraph._components
    import scipy.sparse.csgraph._laplacian
    import scipy.sparse.csgraph._validation
    import scipy.sparse.csgraph._tools
    import scipy.sparse.csgraph._shortest_path
    import scipy.sparse.csgraph._traversal
    import scipy.sparse.csgraph._min_spanning_tree
    import sklearn.externals
    import sklearn.externals
    import sklearn.externals.six
    import sklearn.cross_validation
    import numbers
    import sklearn.utils
    import sklearn.utils
    import sklearn.utils.murmurhash
    import sklearn.utils.validation
    import sklearn.utils.fixes
    import sklearn.utils.class_weight
    import sklearn.utils.sparsetools
    import sklearn.utils.sparsetools
    import sklearn.utils.sparsetools._graph_validation
    import sklearn.utils.sparsetools._graph_tools
    import sklearn.utils.sparsetools._min_spanning_tree
    import sklearn.utils.sparsetools._traversal
    import sklearn.externals.joblib
    import sklearn.externals.joblib
    import sklearn.externals.joblib.memory
    import pydoc
    import pkgutil
    import repr
    import json
    import json
    import json.decoder
    import json.scanner
    import _json
    import encodings.hex_codec
    import binascii
    import json.encoder
    import sklearn.externals.joblib.hashing
    import pickle
    import marshal
    import hashlib
    import _hashlib
    import sklearn.externals.joblib.func_inspect
    import sklearn.externals.joblib._compat
    import sklearn.externals.joblib.logger
    import logging
    import logging
    import threading
    import atexit
    import sklearn.externals.joblib.disk
    import sklearn.externals.joblib.numpy_pickle
    import zlib
    import sklearn.externals.joblib.parallel
    import multiprocessing
    import multiprocessing
    import multiprocessing.process
    import multiprocessing.util
    import subprocess
    import gc
    import select
    import fcntl
    import _multiprocessing
    import multiprocessing.synchronize
    import multiprocessing.forking
    import sklearn.externals.joblib.format_stack
    import sklearn.externals.joblib.my_exceptions
    import sklearn.metrics
    import sklearn.metrics
    import sklearn.metrics.metrics
    import scipy.spatial
    import scipy.spatial
    import scipy.spatial.kdtree
    import scipy.spatial.ckdtree
    import scipy.spatial.qhull
    import scipy.spatial._plotutils
    import scipy.lib.decorator
    import scipy.spatial.distance
    import scipy.spatial._distance_wrap
    import scipy.linalg
    import scipy.linalg
    import scipy.linalg.linalg_version
    import scipy.linalg.misc
    import scipy.linalg.blas
    import scipy.linalg._fblas
    import scipy.lib._util
    import scipy.linalg.basic
    import scipy.linalg.flinalg
    import scipy.linalg._flinalg
    import scipy.linalg.lapack
    import scipy.linalg._flapack
    import scipy.linalg.calc_lwork
    import scipy.linalg.decomp
    import scipy.linalg.decomp_svd
    import scipy.linalg.decomp_lu
    import scipy.linalg.decomp_cholesky
    import scipy.linalg.decomp_qr
    import scipy.linalg._decomp_qz
    import scipy.linalg.decomp_schur
    import scipy.linalg._decomp_polar
    import scipy.linalg.matfuncs
    import scipy.linalg.special_matrices
    import scipy.linalg._expm_frechet
    import scipy.linalg._matfuncs_sqrtm
    import scipy.linalg._solvers
    import numpy.dual
    import sklearn.preprocessing
    import sklearn.preprocessing
    import sklearn.preprocessing.data
    import sklearn.utils.sparsefuncs
    import sklearn.preprocessing.label
    import sklearn.utils.multiclass
    import sklearn.preprocessing.imputation
    import scipy.stats
    import scipy.stats
    import scipy.stats.stats
    import scipy.special
    import scipy.special
    import scipy.special._ufuncs_cxx
    import scipy.special._ufuncs
    import scipy.special.basic
    import scipy.special.specfun
    import scipy.special.orthogonal
    import scipy.special.spfun_stats
    import scipy.special.lambertw
    import scipy.stats.futil
    import scipy.stats.distributions
    import scipy.stats._distn_infrastructure
    import scipy.misc
    import scipy.misc
    import scipy.misc.doccer
    import scipy.misc.common
    import scipy.misc.pilutil
    import tempfile
    import random
    import _random
    import scipy.stats._distr_params
    import scipy.optimize
    import scipy.optimize
    import scipy.optimize.optimize
    import scipy.optimize.linesearch
    import scipy.optimize.minpack2
    import scipy.optimize._minimize
    import scipy.optimize._trustregion_dogleg
    import scipy.optimize._trustregion
    import scipy.optimize._trustregion_ncg
    import scipy.optimize.anneal
    import scipy.optimize.lbfgsb
    import scipy.optimize._lbfgsb
    import scipy.optimize.tnc
    import scipy.optimize.moduleTNC
    import scipy.optimize.cobyla
    import scipy.optimize._cobyla
    import scipy.optimize.slsqp
    import scipy.optimize._slsqp
    import scipy.optimize._root
    import scipy.optimize.minpack
    import scipy.optimize._minpack
    import scipy.optimize.nonlin
    import scipy.sparse.linalg
    import scipy.sparse.linalg
    import scipy.sparse.linalg.isolve
    import scipy.sparse.linalg.isolve
    import scipy.sparse.linalg.isolve.iterative
    import scipy.sparse.linalg.isolve._iterative
    import scipy.sparse.linalg.interface
    import scipy.sparse.linalg.isolve.utils
    import scipy.sparse.linalg.isolve.minres
    import scipy.sparse.linalg.isolve.lgmres
    import scipy.sparse.linalg.isolve.lsqr
    import scipy.sparse.linalg.isolve.lsmr
    import scipy.sparse.linalg.dsolve
    import scipy.sparse.linalg.dsolve
    import scipy.sparse.linalg.dsolve.linsolve
    import scipy.sparse.linalg.dsolve._superlu
    import scipy.sparse.linalg.dsolve._add_newdocs
    import scipy.sparse.linalg.eigen
    import scipy.sparse.linalg.eigen
    import scipy.sparse.linalg.eigen.arpack
    import scipy.sparse.linalg.eigen.arpack
    import scipy.sparse.linalg.eigen.arpack.arpack
    import scipy.sparse.linalg.eigen.arpack._arpack
    import scipy.sparse.linalg.eigen.lobpcg
    import scipy.sparse.linalg.eigen.lobpcg
    import scipy.sparse.linalg.eigen.lobpcg.lobpcg
    import scipy.sparse.linalg.matfuncs
    import scipy.sparse.linalg._onenormest
    import scipy.sparse.linalg._expm_multiply
    import scipy.optimize.zeros
    import scipy.optimize._zeros
    import scipy.optimize.nnls
    import scipy.optimize._nnls
    import scipy.optimize._basinhopping
    import scipy.integrate
    import scipy.integrate
    import scipy.integrate.quadrature
    import scipy.integrate.odepack
    import scipy.integrate._odepack
    import scipy.integrate.quadpack
    import scipy.integrate._quadpack
    import scipy.integrate._ode
    import scipy.integrate.vode
    import scipy.integrate._dop
    import scipy.integrate.lsoda
    import scipy.stats._constants
    import new
    import scipy.stats._continuous_distns
    import scipy.stats.vonmises_cython
    import scipy.stats._tukeylambda_stats
    import scipy.stats._discrete_distns
    import scipy.stats._rank
    import scipy.stats.rv
    import scipy.stats.morestats
    import scipy.stats.statlib
    import scipy.stats._binned_statistic
    import scipy.stats.kde
    import scipy.stats.mvn
    import scipy.stats.mstats
    import scipy.stats.mstats_basic
    import scipy.stats.mstats_extras
    import scipy.stats.contingency
    import scipy.stats._multivariate
    import sklearn.preprocessing._weights
    import sklearn.metrics.scorer
    import sklearn.metrics.cluster
    import sklearn.metrics.cluster
    import sklearn.metrics.cluster.supervised
    import sklearn.utils.lgamma
    import sklearn.metrics.cluster.expected_mutual_info_fast
    import sklearn.metrics.cluster.unsupervised
    import sklearn.metrics.pairwise
    import sklearn.utils.extmath
    import sklearn.utils._logistic_sigmoid
    import sklearn.metrics.pairwise_fast
    import sklearn.metrics.cluster.bicluster
    import sklearn.metrics.cluster.bicluster
    import sklearn.metrics.cluster.bicluster.bicluster_metrics
    import sklearn.utils.linear_assignment_
    import sklearn.cluster
    import sklearn.cluster
    import sklearn.cluster.spectral
    import sklearn.neighbors
    import sklearn.neighbors
    import sklearn.neighbors.typedefs
    import sklearn.neighbors.dist_metrics
    import sklearn.neighbors.ball_tree
    import sklearn.neighbors.kd_tree
    import sklearn.neighbors.graph
    import sklearn.neighbors.base
    import sklearn.neighbors.unsupervised
    import sklearn.neighbors.classification
    import sklearn.neighbors.regression
    import sklearn.neighbors.nearest_centroid
    import sklearn.neighbors.kde
    import sklearn.manifold
    import sklearn.manifold
    import sklearn.manifold.locally_linear
    import sklearn.utils.arpack
    import distutils
    import distutils
    import _virtualenv_distutils
    import _virtualenv_distutils
    import distutils.dist
    import email
    import email
    import email.mime
    import email.mime
    import distutils.errors
    import distutils.fancy_getopt
    import getopt
    import distutils.util
    import distutils.dep_util
    import distutils.spawn
    import distutils.log
    import distutils.debug
    import distutils.sysconfig
    import distutils.version
    import sklearn.manifold.isomap
    import sklearn.utils.graph
    import sklearn.utils.graph_shortest_path
    import sklearn.decomposition
    import sklearn.decomposition
    import sklearn.decomposition.nmf
    import sklearn.decomposition.pca
    import sklearn.decomposition.kernel_pca
    import sklearn.decomposition.sparse_pca
    import sklearn.linear_model
    import sklearn.linear_model
    import sklearn.linear_model.base
    import sklearn.linear_model.cd_fast
    import sklearn.linear_model.bayes
    import sklearn.linear_model.least_angle
    import scipy.interpolate
    import scipy.interpolate
    import scipy.interpolate.interpolate
    import scipy.interpolate.fitpack
    import scipy.interpolate._fitpack
    import scipy.interpolate.dfitpack
    import scipy.interpolate.polyint
    import scipy.interpolate._ppoly
    import scipy.interpolate.fitpack2
    import scipy.interpolate.interpnd
    import scipy.interpolate.rbf
    import scipy.interpolate._monotone
    import scipy.interpolate.ndgriddata
    import sklearn.utils.arrayfuncs
    import sklearn.linear_model.coordinate_descent
    import sklearn.utils.weight_vector
    import sklearn.utils.seq_dataset
    import sklearn.linear_model.sgd_fast
    import sklearn.linear_model.stochastic_gradient
    import sklearn.feature_selection
    import sklearn.feature_selection
    import sklearn.feature_selection.univariate_selection
    import sklearn.feature_selection.base
    import sklearn.feature_selection.rfe
    import sklearn.feature_selection.from_model
    import sklearn.linear_model.ridge
    import sklearn.grid_search
    import sklearn.linear_model.logistic
    import sklearn.svm
    import sklearn.svm
    import sklearn.svm.classes
    import sklearn.svm.base
    import sklearn.svm.libsvm
    import sklearn.svm.liblinear
    import sklearn.svm.libsvm_sparse
    import sklearn.svm.bounds
    import sklearn.linear_model.omp
    import sklearn.linear_model.passive_aggressive
    import sklearn.linear_model.perceptron
    import sklearn.linear_model.randomized_l1
    import sklearn.decomposition.dict_learning
    import sklearn.decomposition.truncated_svd
    import sklearn.decomposition.fastica_
    import sklearn.decomposition.factor_analysis
    import sklearn.manifold.mds
    import sklearn.isotonic
    import sklearn._isotonic
    import sklearn.manifold.spectral_embedding_
    import sklearn.cluster.k_means_
    import sklearn.cluster._k_means
    import sklearn.cluster.mean_shift_
    import sklearn.cluster.affinity_propagation_
    import sklearn.cluster.hierarchical
    import scipy.cluster
    import scipy.cluster
    import scipy.cluster.vq
    import scipy.cluster.hierarchy
    import scipy.cluster._hierarchy_wrap
    import sklearn.cluster._hierarchical
    import sklearn.cluster._feature_agglomeration
    import sklearn.cluster.dbscan_
    import sklearn.cluster.bicluster
    import sklearn.cluster.bicluster
    import sklearn.cluster.bicluster.spectral
    import sklearn.cluster.bicluster.utils
    import sklearn.covariance
    import sklearn.covariance
    import sklearn.covariance.empirical_covariance_
    import sklearn.covariance.shrunk_covariance_
    import sklearn.covariance.robust_covariance
    import sklearn.covariance.graph_lasso_
    import sklearn.covariance.outlier_detection
    import sklearn.datasets
    import sklearn.datasets
    import sklearn.datasets.base
    import csv
    import _csv
    import sklearn.datasets.covtype
    import gzip
    import urllib2
    import base64
    import httplib
    import array
    import socket
    import _socket
    import _ssl
    import urlparse
    import mimetools
    import rfc822
    import ssl
    import textwrap
    import urllib
    import sklearn.datasets.mlcomp
    import sklearn.datasets.lfw
    import sklearn.datasets.twenty_newsgroups
    import tarfile
    import sklearn.feature_extraction
    import sklearn.feature_extraction
    import sklearn.feature_extraction.dict_vectorizer
    import sklearn.feature_extraction.hashing
    import sklearn.feature_extraction._hashing
    import sklearn.feature_extraction.image
    import sklearn.feature_extraction.text
    import unicodedata
    import sklearn.feature_extraction.stop_words
    import sklearn.datasets.mldata
    import scipy.io
    import scipy.io
    import scipy.io.matlab
    import scipy.io.matlab
    import scipy.io.matlab.mio
    import scipy.io.matlab.miobase
    import scipy.io.matlab.byteordercodes
    import scipy.io.matlab.mio4
    import scipy.io.matlab.mio_utils
    import scipy.io.matlab.mio5
    import scipy.io.matlab.streams
    import scipy.io.matlab.mio5_params
    import encodings.utf_16
    import encodings.utf_32
    import scipy.io.matlab.mio5_utils
    import scipy.io.netcdf
    import mmap
    import scipy.io._fortran
    import scipy.io.mmio
    import scipy.io.idl
    import scipy.io.harwell_boeing
    import scipy.io.harwell_boeing
    import scipy.io.harwell_boeing.hb
    import scipy.io.harwell_boeing._fortran_format_parser
    import sklearn.datasets.samples_generator
    import sklearn.datasets.svmlight_format
    import contextlib
    import sklearn.utils.arraybuilder
    import sklearn.datasets._svmlight_format
    import sklearn.datasets.olivetti_faces
    import sklearn.datasets.species_distributions
    import sklearn.datasets.california_housing
    import zipfile
    import sklearn.semi_supervised
    import sklearn.semi_supervised
    import sklearn.semi_supervised.label_propagation
    import sklearn.gaussian_process
    import sklearn.gaussian_process
    import sklearn.gaussian_process.gaussian_process
    import sklearn.gaussian_process.regression_models
    import sklearn.gaussian_process.correlation_models
    import sklearn.hmm
    import sklearn.mixture
    import sklearn.mixture
    import sklearn.mixture.gmm
    import sklearn.mixture.dpgmm
    import sklearn._hmmc
    import sklearn.lda
    import sklearn.naive_bayes
    import sklearn.pipeline
    import sklearn.qda
    import sklearn.cross_decomposition
    import sklearn.cross_decomposition
    import sklearn.cross_decomposition.pls_
    import sklearn.cross_decomposition.cca_
    import sklearn.pls
    import utils
# }}}3


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
            description='Classify using already-built classifier')
    parser.add_argument('classifier', metavar='CLASSIFIER', type=str, nargs=1,
            help='the classifier, as dumped by url.py')
    parser.add_argument('urls', metavar='URL', nargs='*',
            help='the url to classify')
    parser.add_argument('--output', '--url-out', metavar='FILENAME', nargs=1,
            default='-', help='Write output to FILENAME')
    parser.add_argument('--url-file', metavar='URLFILE', nargs=1, default=None,
            help='Read urls to classify from file')
    parser.add_argument('--probabilistic', default=False, action='store_true',
            help='Treat the classifier as probabilistic')
    return parser


def initLoggers(enable_loggers=[], level=DEBUG):
    from logging import StreamHandler, Formatter
    handler = StreamHandler()
    handler.setLevel(level)
    formatter = Formatter(
        '%(name)-8s:%(levelname)-8s - %(message)s')
    handler.setFormatter(formatter)

    loggers = map(getLogger, enable_loggers)
    for l in loggers:
        l.setLevel(level)
        l.addHandler(handler)


if __name__ == '__main__':
    args = get_parser().parse_args()

    buf = None if args.output == '-' else open(args.output[0], 'w')
    urls = [Website(u, '') for u in args.urls]
    if args.url_file:
        for line in open(args.url_file[0]):
            urls.append(Website(line.strip(), ''))

    log.debug('before init')
    initLoggers(enable_loggers=('aggregator', 'resume'),
            level=DEBUG)
    main_predict(args.classifier[0], urls, buf, args.probabilistic)

# vim: set ts=4 sw=4 et fdm=marker foldlevel=2:
