from nose.tools import eq_ as eq

from utils import expand_url, Website


def eq_(l1, l2):
    '''unsorted eq'''
    eq(sorted(l1), sorted(l2))


def expand(url, ngram):
    return list(expand_url(Website(url, ''), ngram))


def test_url_noexpand():
    '''Url splitting without expansion'''
    eq_(expand('http://fub.it', 4), ['fub', 'it'])
    eq_(expand('http://doma.in/get?par=12&also=true', 4),
            ['doma', 'in', 'get', 'par', 'also'])


def test_url_expand_multiple():
    '''multiple ngram sizes means the union of them'''
    eq_(expand('http://www.espando.com', [3, 4]),
            sorted((
                'www', 'esp', 'spa', 'pan', 'and', 'ndo',
                'espa', 'span', 'pand', 'ando', 'com')))


def test_real():
    '''A real url'''
    eq_(expand('http://google.it', 4), ['goog', 'oogl', 'ogle', 'it'])


def test_noalpha():
    '''real urls, with "strange" chars'''
    eq_(expand('http://kamatoz.com/password-management-software.php', 4),
            ['kama', 'amat', 'mato', 'atoz', 'com', 'pass', 'assw', 'sswo',
                'swor', 'word', 'mana', 'anag', 'nage', 'agem', 'geme', 'emen',
                'ment', 'soft', 'oftw', 'ftwa', 'twar', 'ware', 'php'])
def test_query():
    '''Query string _values_ should be avoided (not keys)'''
    eq_(expand('http://google.it/ciao?zap=notthis&query=onlykeys', 4),
            ['goog', 'oogl', 'ogle', 'it', 'ciao', 'zap', 'quer', 'uery'])
