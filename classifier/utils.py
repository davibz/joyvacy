import sys
from itertools import imap, izip, count
from logging import getLogger
from gzip import GzipFile
from bz2 import BZ2File
from itertools import chain
from contextlib import contextmanager

import numpy
from sklearn.metrics import accuracy_score, recall_score, precision_score, \
        f1_score

# importing Website is NOT an error: it's needed for retrocompatibility with
# stuff importing Website from utils!
from mantra.corpus import read_corpus, Website
from mantra.expand import expand_url

YES_LABEL = 1
NO_LABEL = 0
LABELS = {
        'YES': YES_LABEL,
        'NO': NO_LABEL
        }
INV_LABELS = {}
for k, v in LABELS.items():
    assert v not in INV_LABELS
    INV_LABELS[v] = k


BASEDIR = 'classifier/data/'  # sooo ugly


def binarize(answer_yes, iterable):
    '''
    A kind of preprocessor (see DataFetcher)

    Transforms (question, answer) flattening topic to yes/no
    '''
    for question, answer in iterable:
        yield question, YES_LABEL if answer == answer_yes else NO_LABEL


def get_common_evaluations(truth, prediction):
    '''calculates accuracy, recall, precision, and returns a dict of them'''
    def calc(fun):
        return fun(truth, prediction, pos_label=YES_LABEL)
    return dict(
            accuracy=accuracy_score(truth, prediction),
            microrecall=recall_score(truth, prediction, pos_label=None,
                average='micro'),
            macrorecall=recall_score(truth, prediction, pos_label=None,
                average='macro'),
            microprecision=precision_score(truth, prediction, pos_label=None,
                average='micro'),
            macroprecision=precision_score(truth, prediction, pos_label=None,
                average='macro'),
            microf1=f1_score(truth, prediction, pos_label=None,
                average='micro'),
            macrof1=f1_score(truth, prediction, pos_label=None,
                average='macro'),
            )


def common_evaluations(truth, prediction, desc='', ret=False):
    '''print evaluations as a nice header'''
    e = get_common_evaluations(truth, prediction)
    out = 'A=%.2f\tR=%.2f\tP=%.2f\tF=%.2f %s' % \
          (e['accuracy'], e['recall'],
           e['precision'], e['fmeasure'],
           desc)
    if ret:
        return out
    print(out)
common_evaluations.header = 'Accuracy\tRecall\tPrecision\tFmeasure'


def read_csv(fname):
    '''simple read - yield over CSV'''
    opener = get_opener(fname)
    buf = opener(fname, 'r')
    for obj in read_corpus(buf):
        yield obj
    buf.close()

class DataFetcher(object):
    def __init__(self, preprocessor=None):
        self.log = getLogger('DataFetcher')
        if preprocessor is None:
            self.preprocessor = lambda x: x
        else:
            self.preprocessor = preprocessor

    def read(self, fnames):
        '''get data from multiple files'''
        read_func = read_csv
        if self.preprocessor is not None:
            read_func = lambda names: self.preprocessor(read_csv(names))
        all_csv = imap(read_func, fnames)
        self.log.debug('starting file read: %s' % str(fnames))
        questions, answers = zip(*chain(*all_csv)) or ([], [])
        self.log.debug('done file read: %s' % str(fnames))
        return questions, answers


def get_opener(fname):
    if fname.endswith('.gz'):
        return GzipFile
    if fname.endswith('.bz2'):
        return BZ2File
    return open


def auto_buf(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            return sys.stdout
        else:
            return sys.stdin
    else:
        opener = get_opener(fname)
        buf = opener(fname, mode)
        return buf


@contextmanager
def auto_open(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            yield sys.stdout
        else:
            yield sys.stdin
    else:
        opener = get_opener(fname)
        buf = opener(fname, mode)
        try:
            yield buf
        finally:
            buf.close()


def ngrams(word, ngram):
    '''Returns a tuple with n-grammed tokens'''
    if ngram >= len(word):
        yield word
    else:
        for i in xrange(0, len(word) - ngram + 1):
            yield word[i:i + ngram]


class Aggregator(object):
    def __init__(self, estimators):
        '''estimators need to be independently trained'''
        self.estimators = estimators
        self.log = getLogger('aggregator')

    def predict(self, items):
        return imap(lambda estprob: estprob[0], self.predict_proba(items))

    def predict_proba(self, items):
# re-initializing the logger is harmless, and helps about pickling
        self.log = getLogger('aggregator')
        res = {}
        for est in self.estimators:
            res[est] = tuple(imap(lambda x: x[-1],
                                  est.predict_proba(items)))
            self.log.info('Prediction done for %s' %
                     est.name if hasattr(est, 'name') else str(est))
        self.log.info('ALL Predictions done')
        # res[i][j] is the j-th result for the i-th estimator
        # we need to return the maximum value for every j
        assert len(res[self.estimators[0]]) == len(items)
        for j in xrange(len(items)):
            # debug: how much difference is it between the chosen estimator and
            # the other ones?
            results = {}
            for est in self.estimators:
                estimate = res[est][j]
                if numpy.isnan(estimate):
                    continue
                results[est.name] = estimate
            self.log.debug(sorted(results.items(), key=lambda x: x[1],
                reverse=True))

            max_confidence = None
            best_estimator = None
            for est in self.estimators:
                val = res[est][j]
                if val is numpy.nan or numpy.isnan(val):
                    continue
                assert 0.0 <= val <= 1.0, \
                        "Confidence is %f (%s), out of range" %\
                        (val, type(val))
                if max_confidence is None or max_confidence < val:
                    max_confidence = val
                    best_estimator = est
            yield best_estimator, max_confidence


class ConcatPipeline(object):
    '''Fake pipeline object'''
    def __init__(self, all_classifiers, confidences=None):
        self.classifiers = all_classifiers
        if confidences is not None and \
                len(confidences) != len(all_classifiers):
            raise ValueError(
                    'confidences and classifiers have different length')
        self.confidences = confidences

    @property
    def name(self):
        return '+'.join(map(lambda x: x.name, self.classifiers))

    @property
    def confidence(self):
        if self.confidences is None:
            return 1.0
        return reduce(lambda x, y: x * y, self.confidences)

    def __str__(self):
        return '<%s (%.2f)>' % (self.name, self.confidence)

    def to_keyvalue(self):
        d = {}
        d['name'] = self.name
        d['conf'] = '%.4f' % self.confidence
        confidences = self.confidences
        if confidences is None:
            confidences = []
        for i, cl, conf in izip(count(1), self.classifiers, confidences):
            d['level_%d' % i] = cl.name
            d['conf_%d' % i] = '%.4f' % conf
        return ','.join(imap(lambda kv: '='.join(kv), d.items()))
