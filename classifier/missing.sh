#!/usr/bin/env zsh

datadir=$(dirname $0)/data

for f in $datadir/*.2.csv.gz; do
	b=$(basename $f .2.csv.gz)
	c="$datadir/$b.2.binaryall"
	if [[ ! -e $c ]] ; then
		basename "$(readlink -f $f)" .2.csv.gz
	fi
done

