from utils import ConcatPipeline
from resume import results

def main(pickle_file, test_file):
    return results(pickle_file, test_file)
    


if __name__ == '__main__':
    import sys
    main(sys.argv[1], sys.argv[2])
