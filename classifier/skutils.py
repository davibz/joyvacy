from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, naive_bayes

from utils import expand_url

class EstimatorSVC:
    pipe = Pipeline([
        ('vect', CountVectorizer(analyzer=expand_url)),
        ('estimator', svm.SVC()),
        ])

def estimator_factory_fast(name):
    pipe = Pipeline([
        ('vect', CountVectorizer(analyzer=expand_url)),
        #('todense', ToDenseTransformer()),
        # BernoulliNB > MultinomialNB
        ('estimator',naive_bayes.BernoulliNB()),
        #('estimator', svm.LinearSVC()),
        ])
    pipe.name = name
    return pipe




