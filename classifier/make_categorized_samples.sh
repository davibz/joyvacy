#!/usr/bin/env bash
short=$1
shift
if [ ! -f $short ]; then
  echo "$short Does not exist"
fi
basedir=${DATA_PREFIX:-ctg}
TESTSET_SIZE=${TEST_SIZE:-1000}
declare -a categories
oldifs=$IFS
IFS=$'\n'
declare -a categories=($(zcat "$short" | cut -f1 | uniq | sort -u))

for ctg in "${categories[@]}"; do
  echo "[1.$ctg]"
  ctdir="$basedir/$ctg"
  mkdir -p "$ctdir"
  zcat "$short"|fgrep -w "$ctg" | shuf | gzip -c > "$ctdir/all.gz"
  zcat "$ctdir/all.gz"| head -n $TESTSET_SIZE | gzip -c > "$ctdir/test.gz"
  zcat "$ctdir/all.gz"| tail -n "+$((TESTSET_SIZE + 1))" | gzip -c > "$ctdir/learn_pos.gz"
done

echo '[2] Building negative test set'

for ctg in "${categories[@]}"; do
  ctdir="$basedir/$ctg"
  length=$(zcat "$ctdir/learn_pos.gz"| wc -l)
  ncats="${#categories[*]}"
  catlength=$((length/(ncats-1)))
  echo "[2.$ctg] Length is $length, foreach of $((ncats-1)) cat is $catlength"
  for otherctg in "${categories[@]}"; do
    if [ $otherctg = $ctg ]; then
      continue
    fi
    zcat "$basedir/$otherctg/all.gz" | shuf -n "$catlength"
  done | gzip -c > "$ctdir/learn_neg.gz"
done
