from nose.tools import eq_

from utils import ngrams as ngrams_orig

def ngrams(*args):
    return list(ngrams_orig(*args))

def test_same_length():
    for word in ('a', 'yo', 'baz', 'yeah', 'pippo'):
        eq_(ngrams(word, len(word)), [word])

def test_greater_length():
    for word in ('a', 'yo', 'baz', 'yeah', 'pippo'):
        for l in range(len(word), len(word)+4):
            eq_(ngrams(word, len(word)), [word])

def test_1letter():
    eq_(ngrams('foo', 2), ['fo', 'oo'])
    eq_(ngrams('yeah', 3), ['yea', 'eah'])

def test_2letter():
    eq_(ngrams('foo', 1), ['f', 'o', 'o'])
    eq_(ngrams('yeah', 2), ['ye', 'ea', 'ah'])
