'''takes many predicters, and build a hierarchical one'''
from __future__ import print_function
import argparse
import resume
from base64 import b64decode
import sys
import os
from itertools import imap, izip
from collections import Counter
import logging
import tarfile
import tempfile
import json

from sklearn.metrics import confusion_matrix

from utils import ConcatPipeline, Website, DataFetcher, \
        get_common_evaluations, binarize, auto_open


class CascadeError(Exception):
    tmpl = 'Output of root predicter [%s] not in sub keys'

    def __init__(self, value):
        message = self.tmpl % value
        Exception.__init__(self, message)
        self.value = value


class DirectPredicter(object):
    def __init__(self, root, subs=None, ignore_nochild=False):
        '''compatibility layer to just do nothing'''
        self.root = root

    def predict_one(self, question):
        res = next(self.root([question]))
        return ConcatPipeline((res,))

    def predict(self, questions):
        for q in questions:
            yield self.predict_one(q)


class HierPredicter(object):
    def __init__(self, root, subs, ignore_nochild=False):
        '''
        @param root: a predicter whose possible outputs must be the keys of
                     subs
        @param subs: a mapping whose keys are outputs of root.evaluate, value
                     is a predicter
        '''
        self.root = root
        self.subs = subs
        self.ignore_nochild = ignore_nochild

    def predict_one(self, question):
        '''
        Let root evaluate; in cascade, the right "sub" is called
        '''
        value = next(self.root([question]))
        if value.name not in self.subs:
            if not self.ignore_nochild:
                raise ValueError('Output of root-predicter[%s] not in subkeys'
                                 % value.name)
            subvalue = value
        else:
            subvalue = next(self.subs[value.name]([question]))
        return ConcatPipeline((value, subvalue))

    def predict(self, questions):
        for q in questions:
            yield self.predict_one(q)


class HierProbaPredicter(object):
    def __init__(self, root, subs, ignore_nochild=False):
        '''
        @param root: a predicter whose possible outputs must be the keys of
                     subs
        @param subs: a mapping whose keys are outputs of root.evaluate, value
                     is a predicter
        '''
        self.root = root
        self.subs = subs
        self.ignore_nochild = ignore_nochild

    def predict_proba(self, questions):
        for question, pred in izip(questions, self.root(questions)):
            estimator, confidence = pred
            name = estimator.name
            if name in self.subs:
                sub_est, sub_confidence = next(self.subs[name]([question]))
            else:
                msg = 'Output of root predicter [%s] not in sub keys (%s)' \
                            % (name, ','.join(self.subs.keys()))
                if not self.ignore_nochild:
                    raise ValueError(msg)
                logging.warn(msg)
                logging.debug(self.subs.keys())
                sub_est = estimator
                sub_confidence = 0.1
            yield ConcatPipeline((estimator, sub_est),
                    confidences=(confidence, sub_confidence))

    def predict(self, *args):
        for est in self.predict_proba(*args):
            yield est


def get_classification_results(questions, truth, prediction_raw):
    print(truth[:5])
    print(prediction_raw[:5])
    prediction = tuple(imap(lambda x: x.name, prediction_raw))
    print(prediction[:5])
    results = {'global': {},
            'by_truth_category': {},
            'by_predicted_category': {}}
    results['global']['prediction_common'] = Counter(prediction).most_common(8)
    results['global']['stats'] = get_common_evaluations(truth, prediction)
    results['global']['confusion'] = confusion_matrix(truth, prediction).\
            tolist()
    for category in set(truth):
        r = results['by_truth_category'][category] = {}

        def bin(iterable):
            return tuple(imap(lambda x: x[1],
                              binarize(category,
                                       ((0, x) for x in iterable)
                                       )))

        r['stats'] = get_common_evaluations(bin(truth), bin(prediction))
        r['confusion'] = confusion_matrix(bin(truth), bin(prediction)).tolist()
        r['truth_common'] = Counter(bin(truth)).most_common(3)
        r['pred_common'] = Counter(bin(prediction)).most_common(3)
    for category in set(prediction):
        del bin

        def bin(iterable):
            return tuple(imap(lambda x: x[1],
                              binarize(category,
                                       ((0, x) for x in iterable)
                                       )))
        r = results['by_predicted_category'][category] = {}
        r['stats'] = get_common_evaluations(bin(truth), bin(prediction))
        r['truth_common'] = Counter(bin(truth)).most_common(3)
        r['pred_common'] = Counter(bin(prediction)).most_common(3)
    return results


def save_classification_results(questions, truth, prediction_raw,
        out_file=None):
    if out_file is None:
        out_file = sys.stdout
    classification_results = get_classification_results(questions, truth,
            prediction_raw)
    out_file.write(json.dumps(classification_results))


def save_classification(questions, prediction, out_file=None):
    if out_file is None:
        out_file = sys.stdout
    for website, topic in izip(questions, prediction):
        line = u'%s\t%s\t%s\n' % \
                (topic.name, unicode(website), topic.to_keyvalue())
        out_file.write(line.encode('utf-8'))


def get_mapper(args):
    if not args.mapping:
        return lambda x: x
    if args.mapping[0] == 'base64':
        return lambda x: b64decode(os.path.basename(x).split('.', 1)[0])
    else:
        print('Invalid value for mapper "%s"' % args.mapping, file=sys.stderr)
        sys.exit(1)


def get_loader(args):
    if args.subcommand == 'save':
        return resume.load_raw
    if args.probabilistic:
        return resume.load_prob_predicter
    return resume.load_predicter


def get_subs(args):
    mapper = get_mapper(args)
    loader = get_loader(args)
    for sub_desc in args.subs:
        origin, fname = sub_desc.split(':', 1)
        if not origin:
            origin = mapper(fname)
        if args.skip_nonexisting and not os.path.exists(fname):
            continue
        yield (origin, loader(fname))


def get_predicter(args):
    mapper = get_mapper(args)
    loader = get_loader(args)
    root = None
    if args.root:
        root = loader(args.root)
    subs = dict(get_subs(args))
    if args.tarball is not None:
        for tarball in args.tarball:
            # TODO: add member_filter *.binaryall
            for fname, predicter in resume.load_predicter_tarball(tarball,
                    loader=loader):
                if fname == args.root_name:
                    if root is not None:
                        logging.warning("Found file with root-name in tarball "
                                "but root already set")
                    root = predicter
                else:
                    subs[mapper(fname)] = predicter
    if root is None:
        logging.error("Couldn't find any root classifier")

    if args.single:
        return DirectPredicter(root)

    if not subs:
        logging.error("Couldn't find any sub classifier")
    else:
        print("Sub classifiers: %s" % ','.join(subs.keys()))

    if args.probabilistic:
        Predicter = HierProbaPredicter
    else:
        Predicter = HierPredicter
    if 'ignore_nochild' in args:
        return Predicter(root, subs, args.ignore_nochild)
    else:
        return Predicter(root, subs)


def get_urls(args):
    # urls = [Website(u, '') for u in args.urls]
    if args.url_file:
        for line in open(args.url_file[0]):
            yield Website(line.strip(), '')


def main_save(args):
    new_predicter = get_predicter(args)
    tar = tarfile.open(args.classifier_out, 'w:bz2')

    def add_pred_to_tar(pred, name):
        tmpname = tempfile.mktemp()
        if args.probabilistic:
            resume.save_prob_predicter(pred, tmpname)
        else:
            resume.save_predicter(pred, tmpname)
        tar.add(tmpname, name)
        os.unlink(tmpname)
    add_pred_to_tar(new_predicter.root, 'root')
    for pred_name, pred in new_predicter.subs.items():
        add_pred_to_tar(pred, pred_name)
    tar.close()


def main_classify(args):
    new_predicter = get_predicter(args)
    questions, truth = DataFetcher().read(args.url_file)
    questions = tuple(questions)
    truth = tuple(truth)
    prediction = tuple(new_predicter.predict(questions))
    if args.url_out:
        with auto_open(args.url_out[0], 'w') as outbuf:
            save_classification(questions, prediction, outbuf)
    if args.summary:
        # Output evaluation measure to stdout
        with auto_open(args.summary, 'w') as outbuf:
            save_classification_results(questions, truth, prediction, outbuf)


def get_parser():
    parser = argparse.ArgumentParser(
        description='Classify using already-built predicter')
    in_opt = parser.add_argument_group(title='Read options')
    in_opt.add_argument('--root', metavar='ROOT_PREDICTER', type=str,
                        help='the predicter, as dumped by url.py')
    in_opt.add_argument('--tarball', metavar='PREDICTERS', type=str, nargs=1,
                        help='a tarball containing all predicters')
    in_opt.add_argument('--root-name', metavar='ROOT_NAME', type=str,
                        default='root',
                        help='Recognize root classifier inside the tarball')
    in_opt.add_argument('subs', metavar='SUB_PREDICTER', type=str, nargs='*',
                        help='sub predicter, as dumped by url.py')
    in_opt.add_argument('--skip-nonexisting', action='store_true',
                        default=False,
                        help='Ignore non existing files')
    in_opt.add_argument('--probabilistic', action='store_true',
                        default=False,
                        help='Use probabilistic techniques, '
                        'to give you confidence')
    in_opt.add_argument('--mapping', metavar='MAPPING', nargs=1, default=None,
                        help='From filename to label')
    in_opt.add_argument('--single', action='store_true', default=False,
            help='ignore any subs, consider only root')

    sub = parser.add_subparsers(dest='subcommand')
    save = sub.add_parser('save', description='Save to a tarbz2')
    save.add_argument('--classifier-out', metavar='TARBALL',
            default=None, help='Write classifier to TARBALL')
    save.set_defaults(func=main_save)

    classify = sub.add_parser('classify', description='Classify urls')
    classify.add_argument('--url-file', metavar='URLFILE', nargs=1,
            default=None, help='Read urls to classify from file')
    classify.add_argument('--summary', metavar='FILENAME',
            default=None, help='Write test summary on FILENAME')
    classify.add_argument('--url-out', metavar='FILENAME', nargs=1,
            default=None, help='Write url classifications to FILENAME')
    classify.add_argument('--ignore-nochild', action='store_true',
            default=False, help='Ignore non existing children')
    classify.set_defaults(func=main_classify)
    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    args.func(args)

# vim: set ts=4 sw=4 et:
