#!/usr/bin/env bash
set -e

if [ $# -ne 1 ]; then
    echo "Usage: $0 BASEDIR"
    exit 1
fi
mapper() {
	#sha1sum |awk '{ print $1 }'
	base64
}
basedir="$1"
true="../trueprofile/"
true=$(readlink -f "$true")

mkdir -p "$basedir"
cd "$basedir"
find "$true" -maxdepth 1 -type f  -name '*.2.csv.gz'| while read dest
do
    base=$(basename "$dest" .2.csv.gz)
    mapped=$(echo -n "$base" | mapper)
    ln -sf "$dest" "${mapped}.2.csv.gz"
done

find "$true" -maxdepth 1 -type f  -name '*.1.csv.gz'| while read dest
do
    ln -s "$dest" .
    break
done


