#!/usr/bin/env bash

PACKAGES="
python2.7
python2.7-dev
virtualenvwrapper
realpath
build-essential
libblas-dev
liblapack-dev
gfortran
libgfortran3
libpng-dev
libfreetype6-dev
vnc4server
"
apt-get install -y $PACKAGES

