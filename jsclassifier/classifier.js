var fs = require('fs');

var website = require('./website');
var HierClassifier = require('./hierarchical');
function parse_args() {
	if(process.argv.length !== 4) {
		console.error( "Usage: " + process.argv[0] +
			      " URLS CLASSIFIER");
		process.exit(2);
	}
	return {
		urlfile: process.argv[2],
		classifierfile: process.argv[3]
	}
}

function read_urls(fname, callback) {
    console.time('read_urls');
    require('fs').readFile(fname, "utf-8", function(err, data) {
        if(err) throw err;
        urls = [];
        for(var line of data.split('\n')) {
            fields = line.split('\t');
            if(fields.length !== 3) {
                continue;
            }
            urls.push(new website.ClassifiableWebsite(fields[1], fields[2], fields[0]));
        }
        console.timeEnd('read_urls');
        callback(null, urls);
    });
}

function main() {
    args = parse_args();

    var async = require('async');
    async.parallel({
	    classifier: function(cb) {
		    var content = fs.readFileSync(args.classifierfile);
		    var cl = HierClassifier.fromJson(content);
		    cb(null, cl);
	    },
	    urls: async.apply(read_urls, args.urlfile)
    }, function(err, results) {
	    if(err) throw err;
	    async.map(results.urls, function(url, cb) { //categorize
		    var label = results.classifier.categorize(url);
		    cb(null, {predicted: label, orig: url});
	    }, function(err, data) { //check if it matches
		    async.map(data, function(predorig, cb) {
			    cb(null, predorig.predicted === predorig.orig.label);
		    }, function(err, booleans) { //count matching
			    if(err) throw err;
			    console.log("matches in ", booleans.reduce(function(x,y) {
				    return x+y;
			    }, 0), "over", booleans.length)
		    }
			     );
	    });
    });
}
if(require.main === module) {
	main();
}
