var bayes = require('bayes');
function HierClassifier(topcl, subcls, delim, factory) {
    this.topcl = topcl;
    this.subcls = subcls;
    this.delim = delim;
    this.factory = factory;
}

HierClassifier.prototype.learn = function(item, label) {
    labels = label.split('+');
    toplabel = labels[0];
    sublabel = labels[1];
    this.topcl.learn(item, toplabel);
    if(this.subcls[toplabel] === undefined) {
        this.subcls[toplabel] = this.factory(toplabel);
    }
    this.subcls[toplabel].learn(item, sublabel);
}

HierClassifier.prototype.categorize = function(item) {
    var top_value = this.topcl.categorize(item);
    if(this.subcls[top_value] === undefined) {
        throw "No subclassifier for " + top_value;
    }
    var sub_value = this.subcls[top_value].categorize(item);
    return top_value + this.delim + sub_value;
}

HierClassifier.prototype.toString = function() {
	return "[HierClassifier; " + Object.keys(this.subcls).length + " top]";
}

HierClassifier.prototype.toJson = function() {
	out = {};
	out.top = this.topcl.toJson();
	console.log('top of the opts', this.topcl.options);
	console.log('top of the opts-JSON', JSON.stringify(this.topcl.options));
	out.sub = {};
	for(var label in this.subcls) {
		out.sub[label] = this.subcls[label].toJson();
	}
	out.delim = this.delim;
	return JSON.stringify(out);
}
HierClassifier.fromJson = function(jsontext, factory) {
	var data = JSON.parse(jsontext);
	var bayes = require('bayes');
	var top = bayes.fromJson(data.top);
	top.tokenizer = tokenizer;
	var sub = {};
	for(var label in data.sub) {
		sub[label] = bayes.fromJson(data.sub[label]);
		sub[label].tokenizer = tokenizer;
	}
	return new HierClassifier(top, sub, data.delim)
}

function get_classifier_from_corpus(fname) {
    console.time('readcorpus');
    corpus = website.read_file(fname);
    console.timeEnd('readcorpus');
    return get_classifier_from_data(corpus);
}

function tokenizer(ws) {
	return ws.tokenize();
}
function get_classifier_from_data(corpus) {
    console.time('learn');
    function classifier_factory() {
	return bayes({ tokenizer: tokenizer });
    }
    var classifier = new HierClassifier(classifier_factory(), {}, '+', classifier_factory);
    for(var ws of corpus) {
        classifier.learn(ws, ws.label);
    }
    console.timeEnd('learn');
    return classifier;
}


function main() { //output a json classifier
	if(process.argv.length !== 4) {
		console.error( "Usage: " + process.argv[0] +
			      " CORPUS CLASSIFIER");
		process.exit(2);
	}
	var args = {
		classifierfile: process.argv[3],
		corpusfile: process.argv[2]
	}
	console.log(args);
	var corpus = require('./website').read_file(args.corpusfile);
	var classifier = get_classifier_from_data(corpus);
	require('fs').writeFileSync(args.classifierfile,
				    classifier.toJson());
}

if(require.main === module) {
	main();
}

module.exports = HierClassifier;
