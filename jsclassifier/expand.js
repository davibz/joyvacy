var NGRAM=4;
function ngramize(word, n) {
	if(word.length <= n) {
		return [word];
	}
	return [word.substr(0, n)].concat(  // first 4 chars
		arguments.callee(word.substr(1), n)); //recurse shifted by 1 char
}

function tokenize(word) {
	return word.split(/[^a-z0-9]+/i).
		filter(function(part) { return part.length > 0; });;
}

function improve_search(url) {
	if(url.query !== null) {
		url.query = url.query.substring(1);
	}
	if(url.pathname.indexOf(';') != -1) {
		url.query = url.pathname.substr(url.pathname.indexOf(';') + 1);
		url.pathname = url.pathname.substr(0, url.pathname.indexOf(';'));
		return url;
	}
	return url;
}

function improve_query(url) {
	url = improve_search(url);
	var qs = require('querystringify');
	var ampersand = qs.parse(url.query);
	var semicolon = qs.parse(url.query.replace(/;/g, '&'));
	if(Object.keys(ampersand).length > Object.keys(semicolon).length) {
		url.query = ampersand;
	} else {
		url.query = semicolon;
	}
	return url;
}

function get_things(pageurl, title) {
	var global = {}; //Needed for url-parse not to explode
	pageurl = require('url-parse')(pageurl);
	pageurl = improve_query(pageurl);
	var things = [pageurl.host];
	things.push(pageurl.pathname);
	things.push(title.toLowerCase());
	things = things.concat(Object.keys(pageurl.query));
	return things;
}
function get_tokens(pageurl, title) {
	var parts = [];
	var things = get_things(pageurl, title);
	for(var thing of things) {
		for(var tok of tokenize(thing)) {
			for(var ngram of ngramize(tok, NGRAM)) {
				if(parts.indexOf(ngram) == -1) {
					parts.push(ngram);
				}
			}
		}
	}
	return parts.sort();
}

function check_entry(entry) {
	found_tokens = entry.tokens.sort();
	calc_tokens = get_tokens(entry.url, entry.title);
	if(found_tokens.length == calc_tokens.length)
		return true;
}

function main() {
    fs = require("fs");
    Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    };
    var corpus = JSON.parse(fs.readFileSync("corpus.json"));
    var correct = 0;
    var wrong = 0;
    for(var entry of corpus) {
        if(!check_entry(entry)) {
            wrong++;
        } else {
            correct++;
        }
    }
    console.log(wrong, 'errors on ', correct + wrong);
}

module.exports.get_tokens = get_tokens;
