/* Questo file dovrebbe prendere in input vari file (uno per etichetta
 * tendenzialmente, ma non ce n'e' bisogno, nel formato di interscambio
 * ClassifiableWebsite, e produrre un classificatore baiesiano da
 * salvare in un singolo file di output.
 * */

var bayes = require('bayes');

function read_file(fname) {
    //reads a file and returns a list of ClassifiableWebsite
    console.time('read_file');
    var all = JSON.parse(require('fs').readFileSync(fname));
    var l = [];
    for(var i in all) {
        l.unshift(new ClassifiableWebsite(all[i].url,
                    all[i].title,
                    all[i].label,
                    all[i].tokens));
    }
    console.timeEnd('read_file');
    return l;
}

function ClassifiableWebsite(url, title, label, tokens) {
    this.url = url;
    this.title = title;
    this.label = label;
    this.tokens = tokens;
}

ClassifiableWebsite.prototype.tokenize = function() {
    if(this.tokens !== undefined) {
        return this.tokens;
    }
    return require('./expand').get_tokens(this.url, this.title);
}

ClassifiableWebsite.prototype.labelTop = function() {
    return this.label.split('+')[0];
}

ClassifiableWebsite.prototype.toCSV = function() {
    return [this.label, this.url, this.title].join("\t");
}


module.exports.ClassifiableWebsite = ClassifiableWebsite;
module.exports.read_file = read_file;
