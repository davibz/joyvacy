alldirectories: $(CL)/data/hier/testall.gz $(DIRECTORY).hier.csv.gz $(DIRECTORY).1.csv.gz $(DIRECTORY)_split_2

.PHONY: $(DIRECTORY)_split_2

# Virtualenv {{{3
$(TP_VENV): $(TP_VENV)/bin/activate
$(TP_VENV)/bin/activate: $(TP)/requirements.txt
	test -d $(TP_VENV) || virtualenv -p /usr/bin/python2.7 $(TP_VENV)
	./$(TP_VENV)/bin/pip install -Ur $<
	touch $@
# }}}3

$(INTERESTS)/%.csv: $(TP)/get_google_categories.py $(TP_VENV)
	mkdir -p $(INTERESTS)
	./$(TP_VENV)/bin/python $< --lang $* --outfile $@

$(INTERESTS)/%.dot: $(INTERESTS)/%.csv $(TP_VENV) $(INTERESTS)/csv2dot.py
	mkdir -p $(INTERESTS)
	./$(TP_VENV)/bin/python $(INTERESTS)/csv2dot.py $< $@

$(INTERESTS)/stats/children-%.dat: $(INTERESTS)/en-US.dot $(INTERESTS)/stats.py
	mkdir -p $(INTERESTS)/stats
	python2 $(INTERESTS)/stats.py -c allchildren $<

$(INTERESTS)/stats/children-%.png: $(INTERESTS)/stats/children-%.dat $(INTERESTS)/stats/plot-children.sh
	mkdir -p $(INTERESTS)/stats
	./$(INTERESTS)/stats/plot-children.sh $< $@

$(TP)/%.py: $(TP_VENV)
	@true

$(DIRECTORY): $(TP)/get_categories_site.py $(INTERESTS)/en-US.csv
	./$(TP_VENV)/bin/python $< --level 3 --sites-per-keyword 30 $(INTERESTS)/en-US.csv $@

$(DIRECTORY).%.csv.gz: $(TP)/create_directory_csv.py $(DIRECTORY)
	$(TP_VENV)/bin/python $< $(DIRECTORY) --depth $* --dest '$(DIRECTORY).%(depth)s.csv.gz'

$(DIRECTORY)_split_2: $(TP)/create_directory_csv.py $(DIRECTORY)
	$(TP_VENV)/bin/python $< $(DIRECTORY) --depth 2 --skip 1 --split 1 --dest '$(TP)/%(split)s.%(depth)s.csv.gz'

$(DIRECTORY).hier.csv.gz: $(TP)/create_directory_csv.py $(DIRECTORY)
	$(TP_VENV)/bin/python $< $(DIRECTORY) --depth 2 --dest $@
$(CL)/data/hier/testall.gz: $(TP)/create_directory_csv.py $(DIRECTORY)
	mkdir -p $(CL)/data/hier/
	$(TP_VENV)/bin/python $< $(DIRECTORY) --depth 2 --dest $@
