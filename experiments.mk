experiments: $(EXP)/0_1_100/results $(EXP)/1_10-1_100/results

$(EXP)/%/results/: $(EXP)/%/results/reached.csv $(EXP)/%/results/noise.csv
	@echo results of experiment $* done

$(EXP)/%/results/reached.csv: $(EV)/eval_alltops.sh  $(EV)/subset_top.sh $(EV)/csv2max $(INTERESTS)/en-US.csv $(EV)/average.py $(EV)/subset_top.sh $(EXP)/%/data/
	mkdir -p $(dir $@)
	average_args="--not-wanted-weight 0 --not-reached-weight 100" $< $(INTERESTS)/en-US.csv $(EXP)/$*/data | nl -v0 | (head -n1; tail -n3) | $(EV)/csv2max > $@

$(EXP)/%/results/noise.csv: $(EV)/eval_alltops.sh  $(EV)/subset_top.sh $(EV)/csv2max $(INTERESTS)/en-US.csv $(EV)/average.py $(EV)/subset_top.sh $(EXP)/%/data/
	mkdir -p $(dir $@)
	average_args="--not-wanted-weight 1 --not-reached-weight 0" $< $(INTERESTS)/en-US.csv $(EXP)/$*/data | nl -v0 | (head -n1; tail -n3) | $(EV)/csv2max > $@
