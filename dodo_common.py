import sys
import os
from os.path import join, basename, isdir, abspath
from glob import glob
import errno
import fnmatch
from subprocess import Popen, PIPE

DOIT_CONFIG = {
        'reporter': 'executed-only'
        }
unwanted = ('Reference', 'World Localities')
os.environ['PYTHONOPTIMIZE'] = '2'
os.environ['PYTHONWARNINGS'] = 'default'
os.environ['PYTHONPATH'] = 'common/src/'


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and isdir(path):
            pass
        else:
            raise


def filesin(dirpath):
    '''like os.listdir, but returns fullpath'''
    return [os.path.join(dirpath, fname) for fname in
            sorted(os.listdir(dirpath))]


def find_files(basedir):
    ret = []
    for root, dirnames, filenames in os.walk(basedir):
        ret.extend((join(root, f) for f in filenames))

    ret.sort()
    return ret


def basename_match(filename, pattern):
    return fnmatch.fnmatch(basename(filename), pattern)


def find_files_name(basedir, basename_pattern):
    return filter(lambda f: basename_match(f, basename_pattern),
            find_files(basedir))


def has_bin(prog):
    raw_res = Popen(['which', prog], stdout=PIPE, stderr=PIPE)
    raw_res.communicate()
    return raw_res.returncode == 0


if has_bin('sfood'):
    sys.path.extend(os.environ['PYTHONPATH'].split(os.pathsep))
    from snake.dep_recurse import read_sfood_output, persource_deps, go_rec
    raw_res = Popen(['sfood', '-riuq'] + glob('evaluation/*')
            + os.environ['PYTHONPATH'].split(os.pathsep),
            stdout=PIPE,
            env={'PYTHONPATH': os.environ['PYTHONPATH']})
    dep_graph = persource_deps(read_sfood_output(
        [l for l in raw_res.communicate()[0].split('\n') if l]
        ))
else:
    print('Warning: no python dependency')
    dep_graph = None


def get_deps(pyfname):
    if dep_graph is not None:
        deps = go_rec(dep_graph, abspath(pyfname))
        return deps
    else:
        return pyfname
