import os
import re
from urlparse import urlparse, parse_qs


def expand_url(website, ngram=None, ignore_title=None):
    '''Returns a tuple with n-grammed tokens'''
    if ignore_title is None:
        ignore_title = bool(int(os.environ.get('IGNORE_TITLE', '0')))
    if ngram is None:
        ngram = map(int, os.environ.get('NGRAM', '4').split(':'))

    if type(ngram) in (list, tuple):
        def rec(n):
            return expand_url(website, ngram=n, ignore_title=ignore_title)
        return iter(reduce(
            lambda prev, new: prev.union(new),
            map(rec, ngram),
            set()))

    url = urlparse(website.url)
    components = set()
    urlparts = [url.netloc, url.path] + parse_qs(url.query,
            keep_blank_values=True).keys()
    for part in urlparts:
        for component in expand_url.tokens.split(part):
            if not component:
                continue
            for nword in ngrams(component, ngram):
                components.add(nword)
    if ignore_title is False:
        for word in expand_url.tokens.split(website.title.lower()):
            if not word:
                continue
            for nword in ngrams(word, ngram):
                components.add(nword)
    return iter(components)
expand_url.tokens = re.compile('[^a-zA-Z0-9]+')


def ngrams(word, ngram):
    '''Returns a tuple with n-grammed tokens'''
    if ngram >= len(word):
        yield word
    else:
        for i in xrange(0, len(word) - ngram + 1):
            yield word[i:i + ngram]
