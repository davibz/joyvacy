'''
various stuff to help keep the corpus clean
'''


def cat_is_ok(cat):
    '''here, cat is a list such as [Sports, Football]'''
    if cat[0] in ('World Localities', 'Reference', 'Online Communities'):
        return False
    return True
