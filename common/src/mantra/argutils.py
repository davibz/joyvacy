def is_set_arg(args, parameter):
    if parameter in args and getattr(args, parameter) is not None:
        return True
    return False
