import sys
from gzip import GzipFile
from bz2 import BZ2File
from contextlib import contextmanager


def get_opener(fname):
    if fname.endswith('.gz'):
        return GzipFile
    if fname.endswith('.bz2'):
        return BZ2File
    return open


def auto_buf(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            return sys.stdout
        else:
            return sys.stdin
    else:
        opener = get_opener(fname)
        buf = opener(fname, mode)
        return buf


@contextmanager
def auto_open(fname, mode='r'):
    if fname == '-':
        if 'w' in mode:
            yield sys.stdout
        else:
            yield sys.stdin
    else:
        opener = get_opener(fname)
        buf = opener(fname, mode)
        try:
            yield buf
        finally:
            buf.close()
