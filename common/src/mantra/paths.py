import json
import os
from os.path import exists, join


def get_sessions(expdir):
    if not exists(join(expdir, 'metrics')):
        raise ValueError('not a valid experiment dir')
    for session in os.listdir(join(expdir, 'metrics')):
        yield json.load(open(join(expdir, 'metrics', session,
            'enhanced.json')))
