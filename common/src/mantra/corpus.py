from urlparse import urlparse
from itertools import count
import logging


def website_filter(website):
    parsed_url = urlparse(website.url)
    if parsed_url.netloc in website_filter.banned_domains:
        return False
    return True
website_filter.banned_domains = ('www.youtube.com', 'youtube.com')


def extract_corpus_line(line, context=''):
    log = logging.getLogger('mantra.corpus.extract')
    line = line.decode('utf-8').strip()
    if not line:
        return None
    if line.count('\t') == 1:
        line = '%s\t' % line
    parts = line.split('\t')
    if len(parts) < 3:
        log.error("Too few components in %s '%s'" %
                (context,
                    repr(line)))
    topic, url, title = parts[:3]
    extra = parts[3] if len(parts) > 3 else None
    website = Website(url, title, extra)
    if not website_filter(website):
        return None
    return website, topic


def read_corpus(buf):
    for line_number, line in zip(count(1), buf):
        context = "{} ({})".format(
                    buf.name if hasattr(buf, 'name') else buf,
                    line_number
                    )
        parsed = extract_corpus_line(line, context)
        if parsed is not None:
            yield parsed


class Website(object):
    def __init__(self, url, title, extra=None):
        self.url = url
        self.title = title
        self.extra = extra

    def __str__(self):
        if self.extra:
            return '%s\t%s' % (self.url, self.extra)
        return self.url

    def to_csv(self):
        if self.extra:
            return '\t'.join((self.url, self.title, self.extra))
        else:
            return '\t'.join((self.url, self.title))
