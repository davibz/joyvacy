from __future__ import division
from .top import top_set


def accuracy(target, collected):
    '''accuracy is the intersection divided by the union'''
    return len(target.intersection(collected)) / \
            len(target.union(collected))


def topaccuracy(target, collected):
    return accuracy(top_set(target), top_set(collected))


def traditional_noise(target, collected):
    '''traditional noise: every unwanted category'''
# these asserts mean "is of type set" in duck typing fashio
    assert hasattr(target, 'difference')
    assert hasattr(collected, 'difference')
    unwanted = collected - target
    return len(unwanted)


def top_noise(target, collected):
    '''like traditional noise, but limited to top'''
    return traditional_noise(top_set(target), top_set(collected))


def noise_info(target, collected, metric):
    abs_noise = metric(target, collected)
    return {
            'abs': abs_noise,
            'rel': float(abs_noise) / len(target),
            'target_size': len(target)
            }
