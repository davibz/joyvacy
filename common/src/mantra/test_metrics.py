from nose.tools import eq_

from metrics import accuracy


def test_accuracy_onesame():
    eq_(accuracy({0}, {0}), 1)


def test_accuracy_onedifferent():
    eq_(accuracy({0}, {1}), 0)


def test_accuracy_oneontwo():
    eq_(accuracy({0, 1}, {1}), 0.5)


def test_accuracy_oneonfour():
    eq_(accuracy({0, 1}, {1, 2, 3}), 0.25)
