def top_cat(cat_name):
    return cat_name.split('+')[0]


def top_set(s):
    return set([top_cat(cat) for cat in s])
