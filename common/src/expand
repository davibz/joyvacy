#!/usr/bin/env python2
import logging
import json

from mantra.corpus import Website
from mantra.expand import expand_url


def expand_many(websites_iter):
    s = set()
    for website in websites_iter:
        s.update(expand_url(website))
    logging.debug('corpus read')
    return s


def history_reader(infile):
    next(infile)  # first line is header
    for line in infile:
        parts = line.split('\t')
        yield Website(parts[2], parts[3])


def corpus_reader(infile):
    for line in infile:
        parts = line.split('\t')
        yield Website(parts[1], parts[2])


def main_readfile(args):
    features = expand_many(args.reader(args.infile))
    args.outfile.write('\n'.join(features))


def main_single(args):
    for feature in expand_url(Website(args.url, args.title)):
        args.outfile.write(feature)
        args.outfile.write('\n')


def main_compare(args):
    corpus = expand_many(corpus_reader(args.corpus))
    args.corpus.close()
    for web in history_reader(args.history):
        web_features = set(expand_url(web))
        data = dict(total=len(web_features), miss=len(web_features -
            corpus), hit=len(web_features.intersection(corpus)))
        data['hitrate'] = int(data['hit'] * 100 / float(data['total']))
        data['missrate'] = 100 - data['hitrate']
        args.outfile.write(json.dumps(data))
        args.outfile.write('\n')


def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
        description='Get corpus features')
    generic = parser.add_argument_group('generic options')
    generic.add_argument('--loglevel', metavar='LEVEL', type=str,
            default='INFO')
    sub = parser.add_subparsers(dest='subcomannd')
    readfile = sub.add_parser('readfile', description='Read history from file '
            'and get URLs from there')

    readfile.add_argument('--infile', metavar='CSV',
            type=argparse.FileType('r'),
            default='-')
    readfile.add_argument('--history', dest='reader', action='store_const',
            const=history_reader, default=corpus_reader)
    readfile.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    readfile.set_defaults(func=main_readfile)

    single = sub.add_parser('single')
    single.add_argument('url')
    single.add_argument('title')
    single.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    single.set_defaults(func=main_single)

    compare = sub.add_parser('compare')
    compare.add_argument('history',
            type=argparse.FileType('r'), default='-')
    compare.add_argument('--corpus', metavar='CSV',
            type=argparse.FileType('r'), default='-')
    compare.add_argument('--outfile', metavar='OUTFILE', default='-',
            type=argparse.FileType('w'))
    compare.set_defaults(func=main_compare)
    return parser


if __name__ == '__main__':
    args = get_parser().parse_args()
    logging.basicConfig(level=args.loglevel)
    args.func(args)
