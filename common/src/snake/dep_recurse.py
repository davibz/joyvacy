#!/usr/bin/env python2.7
from collections import defaultdict
from os.path import join
import json


def read_sfood_output(buf):
    for line in buf:
        # so insecure :(
        yield eval(line)


def persource_deps(sfoodout):
    perfile = defaultdict(list)
    for source, dep in sfoodout:
        if dep == (None, None):
            continue
        perfile[join(*source)].append(join(*dep))
    return perfile


def go_rec(perfile, start):
    out = set()
    out.add(start)
    outlen = len(out)
    while True:
        new = set()
        for f in out:
            for val in perfile[f]:
                new.add(val)
        out.update(new)
        if len(out) == outlen:
            break
        outlen = len(out)
    return list(out)

if __name__ == '__main__':
    import sys
    out = persource_deps(read_sfood_output(sys.stdin))
    print json.dumps(go_rec(out, sys.argv[1]))
