include defaults.mk
-include config.mk

include classifier.mk
include firefox.mk
include trueprofile.mk
include evaluation.mk
include experiments.mk
