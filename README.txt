Components:
* Classifier: takes an URL, return a category
** Create a classifier and save it in a file (serialized)
** Use it to test urls: read the classifier, use it to classify urls
* TrueProfile: python script that returns a list of categories that google
  thinks we are interested in; superseeded by FireJoyvacy, that does the same
  in a single line of code
* InterestSimulator: does what synthoid does: run queries and visit sites
* UI

By now, we have Classifier and TrueProfile, written in python.
We need to create a FF extension and reimplement (or integrate) them inside
the extension.

TODO:
* trueprofile: port to js, which should simplify it
* do something useful with history classification
* speedup classification (persistent process whose input is appended to?)
