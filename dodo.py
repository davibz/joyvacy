from __future__ import print_function
import os
import gzip
from pprint import pprint
import json

from doit import create_after
from doit.tools import check_timestamp_unchanged

from dodo_common import mkdir_p, DOIT_CONFIG, unwanted

# "toolchain" setup {{{
pathdirs = os.environ.get('PATH', '').split(':')
for d in ('common/src',):
    real_d = os.path.realpath(d)
    if not real_d in pathdirs:
        pathdirs.append(real_d)
os.environ['PATH'] = os.pathsep.join(pathdirs)
os.environ['PYTHONPATH'] = 'common/src'
os.environ['TEST_SIZE'] = '50'
defaults = {
        'NGRAM': '4',
        'IGNORE_TITLE': '0',
        }
for k in defaults:
    os.environ.setdefault(k, defaults[k])
# }}}

# conf globals {{{
relevant_env = {k: os.environ[k] for k in defaults}
conf_string = ','.join(('{}={}'.format(k, v)
    for k, v in sorted(relevant_env.items())))
corpus_dir = 'trueprofile'
root_cat = 'google_directory'
if relevant_env == defaults:  # following defaults
    data = 'data'
    results = 'results/'
else:
    data = os.path.join('data', conf_string)
    results = os.path.join('results', conf_string)
midcorpus = os.path.join(data, 'corpus_intermediate')
classifiers = os.path.join(data, 'classifiers')
# }}}


# utilities {{{


def _get_cat_path(cat, prefix=None):
    '''here, cat is expressed as a list of cat and subcat'''
    if len(cat) == 0:
        raise ValueError('cat cannot be empty')
    while len(cat) >= 2 and cat[0] == root_cat and cat[1] == root_cat:
        cat.pop(0)
    if prefix is None:
        return os.path.join(*cat)
    return os.path.join(prefix, *cat)


def _get_subcats(fname):
    cats = set()
    with gzip.GzipFile(fname) as buf:
        for line in buf:
            sub = line.split('\t')[0]
            if sub not in unwanted:
                cats.add(sub)
    return cats


def _concatenate(fnames, outfile):
    with open(outfile, 'w') as outbuf:
        for fname in fnames:
            with open(fname) as inbuf:
                outbuf.write(inbuf.read())


def _virtualenv(basedir):
    return ['virtualenv', '-p', '/usr/bin/python2', basedir]


def _virtualenv_install(basedir, req):
    return [os.path.join(basedir, 'bin', 'pip'), 'install', '-r', req]

# utilities }}}


def do_catinfo(cat, info):
    pprint(cat)
    pprint(info)


def task_catinfo():
    for cat, info in cats_top.items():
        yield {
                'name': cat,
                'actions': [
                    (do_catinfo, (cat, info))
                    ],
                'uptodate': [True],
                }

# corpus data {{{
# this could be moved to a task, according to
# http://pydoit.org/dependencies.html#calculated-dependencies
cats_top = {}
for fname in filter(
        lambda f: f.endswith('.2.csv.gz') or f.endswith('.1.csv.gz'),
        os.listdir(corpus_dir)
        ):
    cat_name = fname.split('.')[0]
    info = {}
    info['csv'] = os.path.join(corpus_dir, fname)
    if cat_name == root_cat:
        info['middir'] = os.path.join(midcorpus, cat_name)
    else:
        info['middir'] = _get_cat_path([cat_name], midcorpus)
    info['sub'] = _get_subcats(info['csv'])
    cats_top[cat_name] = info
    del info
# corpus data }}}

for un in unwanted:
    if un in cats_top.keys():
        del cats_top[un]

assert len(cats_top) == len(set(cats_top))


def task_data_environ():
    def dump(filename):
        mkdir_p(os.path.dirname(filename))
        with open(filename, 'w') as buf:
            json.dump(relevant_env, buf)
    outs = [os.path.join(outdir, 'environ.json') for outdir in (data, results)]
    yield {
            'name': 'environ',
            'actions': [(dump, (out,)) for out in outs],
            'targets': outs
            }


def task_corpusdir():
    '''creates all.0 files from csv'''
    yield dict(name=None, uptodate=[True])
    exe = 'corpusfilter/src/make_samples_tree'
    for cat, info in cats_top.items():
        yield {
                'name': cat,
                'actions': [[
                    exe,
                    '-d', 'all.0',
                    info['csv'],
                    info['middir']
                        ]],
                'targets': [os.path.join(midcorpus, cat, sub, 'all.0')
                    for sub in info['sub']],
                'file_dep': [exe,
                    'common/src/corpus_prefilter',
                    'common/src/mantra/corpus.py', info['csv']],
                'doc': 'creates all.0'
                }


def task_filterconfidence():
    '''creates all.1 applying filter_confidence.py to all.0'''
# ./classifier/venv/bin/python classifier/filter_confidence.py  --url-file
    # trueprofile/Sports.2.csv.gz --directory
    # data/corpus_intermediate/google_directory/*/  --ok-out
    # data/corpus_intermediate/google_directory/Sports/all.1 --fail-out
    # /tmp/fail.txt
    yield dict(name=None, uptodate=[True])

    FILTER_POS = 1
    exe = 'classifier/filter_confidence.py'

    fname = 'all.%d' % FILTER_POS
    prev_fname = 'all.%d' % (FILTER_POS - 1)
    for cat, info in cats_top.items():
        if cat != root_cat:
            continue
        alldirs = [os.path.join(s) for s in info['sub']]
        alldirs = map(lambda s: os.path.join(info['middir'], s), alldirs)
        for subcat, subinfo in map(lambda s: (s, cats_top[s]), info['sub']):
            out = os.path.join(midcorpus, cat, subcat, fname)
            filter_confidence = [
                    'classifier/venv/bin/python',
                    exe,
                    '--url-file', subinfo['csv'],
                    '--directory'
                    ]
            filter_confidence.extend(alldirs)
            filter_confidence.extend(['--ok-out', out,
                '--fail-out',
                os.path.join(midcorpus, cat, subcat, 'filter_confidence.fail')
                ])
            deps = [os.path.join(midcorpus, cat, s, 'learn_pos') for s in
            info['sub']]
            deps.append(os.path.join(midcorpus, cat, subcat, prev_fname))
            deps.append('classifier/venv/bin/python')
            deps.append(exe)
            yield {
                    'name': '%s+%s' % (cat, subcat),
                    'actions': [filter_confidence],
                    'targets': [out],
                    'file_dep': deps,
                    'doc': 'creates %s' % out
                    }


def task_corpus_learningset():
    '''creates learn_pos and learn_neg from all.0'''
    yield dict(name=None, uptodate=[True])
    exe = 'corpusfilter/src/make_learning'
    for cat, info in cats_top.items():
        for subcat in info['sub']:
            base = os.path.join(midcorpus, cat, subcat)
            yield {
                    'name': '%s+%s' % (cat, subcat),
                    'actions': [[exe,
                        '-a', 'all.0',
                        info['csv'],
                        _get_cat_path([cat], midcorpus)
                        ]],
                    'file_dep': [exe, os.path.join(base, 'all.0')],
                    'targets': [os.path.join(base, f) for f in
                        ('learn_pos', 'learn_neg', 'test')],
                    'doc': 'creates learn_pos and learn_neg'
                    }


def task_makeffcorpus():
    o = {}
    o['exe'] = 'trueprofile/create_directory_csv.py'
    o['target'] = 'FireJoyvacy/data/corpus'
    o['input'] = 'trueprofile/google_directory/'

    cmd = ["python2.7", o['exe'], o['input'],
           '--dest', '%s/%%(split)s.%%(depth)s.csv' % o['target'],
           '--depth', '2', '--split', '2']
    return {
            'actions': [['mkdir', '-p', o['target']],
                        cmd],
            'targets': [o['target']],
            'file_dep': [o['exe']]
            }


def task_corpus2json():
    '''create a json (single file) corpus to be used by a classifier'''
    o = {}
    o['base'] = 'FireJoyvacy/data/corpus'
    o['dest'] = o['base'] + '.json'
    o['exe'] = './common/src/jsoncorpus'
    return {
            'actions': ['cat %(base)s/*.csv | %(exe)s --outfile %(dest)s' % o],
            'file_dep': ['common/src/jsoncorpus',
                'common/src/mantra/expand.py'],
            'task_dep': ['makeffcorpus'],
            'uptodate': [check_timestamp_unchanged(o['base'])],
            'targets': [o['dest']]
            }


def task_jsonclassifier():
    '''prepare JSON classifier'''
    corpus = 'FireJoyvacy/data/corpus.json'
    out = 'FireJoyvacy/data/classifier.json'
    base = 'jsclassifier'
    return {
        'actions': [[
            'npm', '--prefix', base, 'run',
            'learn', os.path.realpath(corpus), os.path.realpath(out)
        ]],
        'file_dep': [corpus],
        'task_dep': ['npm:' + base],
        'targets': [out]
    }


def task_xpi():
    '''Create mantra xpi'''
    classifier = 'FireJoyvacy/data/classifier.json'
    return {
        'actions': [
            'cd FireJoyvacy; jpm xpi'
        ],
        'file_dep': [classifier],
        # FIXME: setting version number inside the expected target is horrible
        'targets': ['FireJoyvacy/firejoyvacy@fub.it-0.2.xpi']
    }


def task_testall():
    '''creates testall in each category'''
    yield dict(name=None, uptodate=[True])
    for cat, info in cats_top.items():
        sub_test_files = [os.path.join(midcorpus, cat, sub, 'test')
                          for sub in info['sub']]
        out = os.path.join(midcorpus, cat, 'testall')
        yield {
                'name': cat,
                'actions': [(_concatenate, [sub_test_files, out])],
                'file_dep': sub_test_files,
                'targets': [out]
                }


def task_testallhier():
    '''creates a "main" testall'''
    exe = 'trueprofile/create_directory_csv.py'
    out = os.path.join(midcorpus, 'testall')
    return {
            'actions': [
                [
                    'trueprofile/venv/bin/python',
                    exe,
                    '--depth', '2',
                    os.path.join(corpus_dir, root_cat),
                    '--dest', out
                    ],
                ],
            'targets': [out],
            'file_dep': ['trueprofile/venv/bin/python', exe]
            }


@create_after(executed='testallhier')
def task_classbin():
    exe = 'classifier/create_binary.py'
    yield {
            'name': None,
            'task_dep': ['testall'],
            'uptodate': [True]
            }
    for cat, info in cats_top.items():
        for subcat in info['sub']:
            cl_dir = _get_cat_path((cat, subcat), classifiers)
            corpus_dir = _get_cat_path((cat, subcat), midcorpus)
            if not os.path.exists(corpus_dir):
                continue
            try:
                all_file = sorted(filter(lambda f: 'all.' in f,
                    os.listdir(corpus_dir)),
                    reverse=True)[0]
            except IndexError:
                continue
            cl_out = os.path.join(cl_dir, 'binary')
            yield {
                    'name': '%s+%s' % (cat, subcat),
                    'actions': [
                        (mkdir_p, (cl_dir, )),
                        [
                            'classifier/venv/bin/python',
                            exe,
                            '--classifier-out', cl_out,
                            corpus_dir
                            ]
                        ],
                    'targets': [cl_out],
                    'file_dep': [
                        info['csv'],
                        os.path.join(corpus_dir, all_file)
                        ],
                    }


def task_classmulti():
    exe = 'classifier/create_multiclass.py'
    yield {
            'name': None,
            'doc': "Creates 'multiclass' classifiers from 'binary' ones"
            }

    for cat, info in cats_top.items():
        cl_dir = os.path.join(classifiers, cat)
        multiclass_out = os.path.join(cl_dir, 'multiclass')
        binaries = [os.path.join(cl_dir, subcat, 'binary')
                for subcat in info['sub']]
        yield {
                'name': cat,
                'actions': [
                    [
                        'classifier/venv/bin/python',
                        exe,
                        '--classifier-out', multiclass_out
                        ] + binaries
                    ],
                'targets': [multiclass_out],
                'file_dep': [exe],
                # this is toomuch; classbin:$name* would be better
                'task_dep': ['classbin'],
                }


def task_hierarchicaltarball():
    exe = 'classifier/hierarchical.py'
    multiclass_raw = {cat: os.path.join(classifiers, cat, 'multiclass')
            for cat in cats_top.keys() if cat != root_cat}
    multiclass = ['%s:%s' % (cat, path)
            for cat, path in multiclass_raw.items()]
    hier_cl = os.path.join(classifiers, 'hierarchical.tar.bz2')
    return {
            'actions': [
                [
                    'classifier/venv/bin/python',
                    exe,
                    '--skip-nonexisting',
                    '--root',
                    os.path.join(classifiers, root_cat,
                    'multiclass')
                    ] + multiclass +
                [
                    'save',
                    '--classifier-out', hier_cl
                    ]
                ],
            'targets': [hier_cl],
            'file_dep': ['classifier/venv/bin/python', exe] +
            multiclass_raw.values(),
            'task_dep': ['classmulti']
            }


# TODO: pyinstaller hierarchical.py

def task_toplevelresults():
    exe = 'classifier/hierarchical.py'
    hier_cl = os.path.join(classifiers, root_cat, 'multiclass')
    testfile = os.path.join(midcorpus, root_cat, 'testall')
    resfile = os.path.join(results, 'toplevel', 'classification.txt')
    sumfile = os.path.join(results, 'toplevel', 'summary.json')
    return {
            'actions': [
                ['mkdir', '-p', os.path.join(results, 'toplevel')],
                [
                    'classifier/venv/bin/python',
                    exe,
                    '--root', hier_cl,
                    '--single',
                    #'--probabilistic',
                    'classify',
                    '--url-file', testfile,
                    '--url-out', resfile,
                    '--summary', sumfile
                    ]
                ],
            'targets': [resfile, sumfile],
            'file_dep': [exe, hier_cl, testfile],
            'task_dep': ['hierarchicaltarball']
            }


def task_hierarchicalresults():
    exe = 'classifier/hierarchical.py'
    hier_cl = os.path.join(classifiers, 'hierarchical.tar.bz2')
    testfile = os.path.join(midcorpus, 'testall')
    resfile = os.path.join(results, 'hierarchical', 'classification.txt')
    sumfile = os.path.join(results, 'hierarchical', 'summary.json')
    return {
            'actions': [
                ['mkdir', '-p', os.path.join(results, 'hierarchical')],
                [
                    'classifier/venv/bin/python',
                    exe,
                    '--tarball', hier_cl,
                    '--probabilistic',
                    'classify',
                    '--ignore-nochild',
                    '--url-file', testfile,  # hier/testall
                    '--url-out', resfile,
                    '--summary', sumfile
                    ]
                ],
            'targets': [resfile, sumfile],
            'file_dep': [exe, hier_cl, testfile],
            'task_dep': ['hierarchicaltarball']
            }


def task_venv():
    for base in ('classifier', 'trueprofile'):
        d = os.path.join(base, 'venv')
        req = os.path.join(base, 'requirements.txt')
        yield {
                'name': base,
                'actions': [_virtualenv(d),
                    _virtualenv_install(d, req)
                    ],
                'targets': [os.path.join(d, 'bin', exe)
                    for exe in ('pip', 'python')],
                'file_dep': [req]
                }


def task_npm():
    base = 'jsclassifier'
    req = os.path.join(base, 'package.json')
    yield {
            'name': base,
            'actions': [['npm', '--prefix', base, 'install']],
            'targets': [os.path.join(base, 'node_modules')],
            'file_dep': [req]
    }

# vim: set ft=python:
